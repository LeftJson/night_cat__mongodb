package com.fh.util.aliyun;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.fh.util.PageData;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.WriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

public class MongoUtil {
	public static String host1  = "dds-uf651490bb013f441318-pub.mongodb.rds.aliyuncs.com";
	public static String host2  = "dds-uf651490bb013f442650-pub.mongodb.rds.aliyuncs.com";
	public static int port1 = 3717;
	public static int port2 = 3717;
    public static ServerAddress seed1 = new ServerAddress(host1, port1);
    public static ServerAddress seed2 = new ServerAddress(host2, port2);
    public static String username = "root";
    public static String password = "Yx20171024";
    public static String ReplSetName = "mgset-4959079";
    public static String DEFAULT_DB = "admin";
    public static String DEMO_DB = "yemao";    //数据库
    public static String DEMO_COLL = "users";  //集合
    
   	/******************************公网连接****************************/
    private MongoClient mongoClient = null;
    private static final MongoUtil mongo = new MongoUtil();// 饿汉式单例模式
    private MongoUtil()
    {
        if (mongoClient == null)
        {
            try
            {
                System.out.println("*************************************");
         		System.out.println("getMongoDBClient: ");
         	    // 构建Seed列表
                List<ServerAddress> seedList = new ArrayList<ServerAddress>();
                seedList.add(seed1);
                seedList.add(seed2);
                // 构建鉴权信息
                List<MongoCredential> credentials = new ArrayList<MongoCredential>();
                credentials.add(MongoCredential.createScramSha1Credential(username,
                      DEFAULT_DB, password.toCharArray()));
                //===================================================//
                MongoClientOptions.Builder builder = MongoClientOptions.builder();
                // 与目标数据库能够建立的最大connection数量为50 
                builder.connectionsPerHost(50); 
                // 如果当前所有的connection都在使用中，则每个connection上可以有50个线程排队等待 
                builder.threadsAllowedToBlockForConnectionMultiplier(50); 
                // 一个线程访问数据库的时候，在成功获取到一个可用数据库连接之前的最长等待时间为2分钟 
                // 这里比较危险，如果超过maxWaitTime都没有获取到这个连接的话，该线程就会抛出Exception 
                // 故这里设置的maxWaitTime应该足够大，以免由于排队线程过多造成的数据库访问失败 
                builder.maxWaitTime(1000*60*2); 
                // 与数据库建立连接的timeout设置为1分钟 
                builder.connectTimeout(1000*60*1);  
                //===================================================//
                MongoClientOptions mco = builder.build(); 
                mongoClient = new MongoClient(seedList, credentials, mco);
            } catch (Exception e) {
            	e.printStackTrace();
            }
        }
    }
    public static MongoUtil getMongoUtil()
    {
        return mongo;
    }
    public MongoDatabase getDb(String dbName)
    {
        return mongoClient.getDatabase(dbName);
    }

    public MongoCollection<Document> getCollection(String dbName, String collectionName)
    {
    	MongoDatabase database = mongoClient.getDatabase(dbName);
        return database.getCollection(collectionName);
    }
    // 初始化1
//    public static MongoClient createMongoDBClient() {
//    	System.out.println("*************************************");
//   		System.out.println("getMongoDBClient: ");
//   	    // 构建Seed列表
//        List<ServerAddress> seedList = new ArrayList<ServerAddress>();
//        seedList.add(seed1);
//        seedList.add(seed2);
//        // 构建鉴权信息
//        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
//        credentials.add(MongoCredential.createScramSha1Credential(username,
//                DEFAULT_DB, password.toCharArray()));
//        // 构建操作选项，requiredReplicaSetName属性外的选项根据自己的实际需求配置，默认参数满足大多数场景
//        MongoClientOptions options = MongoClientOptions.builder()
//                .requiredReplicaSetName(ReplSetName).socketTimeout(2000)
//                .connectionsPerHost(1).build();
//        return new MongoClient(seedList, credentials, options);
//    }
//    // 初始化2
//    public static MongoClient createMongoDBClientWithURI() {
//    	System.out.println("*************************************");
//   		System.out.println("createMongoDBClientWithURI: ");
//    	MongoClientURI connectionString = new MongoClientURI("mongodb://" + username + ":" + password + "@" +  seed1 + "," + seed2 + "/" + DEFAULT_DB + "?replicaSet=" + ReplSetName);
//        return new MongoClient(connectionString);
//    }
    
//    /**
//     * @return
//     * @throws Exception
//     */
//    public static MongoClient getMongoClient()throws Exception{
//      try {
//	    System.out.println("*************************************");
// 		System.out.println("getMongoDBClient: ");
// 	    // 构建Seed列表
//        List<ServerAddress> seedList = new ArrayList<ServerAddress>();
//        seedList.add(seed1);
//        seedList.add(seed2);
//        // 构建鉴权信息
//        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
//        credentials.add(MongoCredential.createScramSha1Credential(username,
//              DEFAULT_DB, password.toCharArray()));
//        //===================================================//
//        MongoClientOptions.Builder builder = MongoClientOptions.builder();
//        // 与目标数据库能够建立的最大connection数量为50 
//        builder.connectionsPerHost(50); 
//        // 如果当前所有的connection都在使用中，则每个connection上可以有50个线程排队等待 
//        builder.threadsAllowedToBlockForConnectionMultiplier(50); 
//        // 一个线程访问数据库的时候，在成功获取到一个可用数据库连接之前的最长等待时间为2分钟 
//        // 这里比较危险，如果超过maxWaitTime都没有获取到这个连接的话，该线程就会抛出Exception 
//        // 故这里设置的maxWaitTime应该足够大，以免由于排队线程过多造成的数据库访问失败 
//        builder.maxWaitTime(1000*60*2); 
//        // 与数据库建立连接的timeout设置为1分钟 
//        builder.connectTimeout(1000*60*1);  
//        //===================================================//
//        MongoClientOptions mco = builder.build(); 
//        return new MongoClient(seedList, credentials, mco);
//      } catch (Exception e) {
//        throw e;
//      }
//    }
    
    // 查询记录总数
   	public long queryCount(String coll,PageData where) throws Exception {
   		if( coll==null ){
   			return 0;
   		}
   		if( where==null ){
   			where = new PageData();
   		} 
   		if( where.get("ID")!=null ){
   			String id=where.get("ID").toString();
   			if(id.length()>=24){
   				where.put("_id", new ObjectId(id));
   			}else {
   				where.put("_id", id);
			} 
   			where.remove("ID");
		}
   		try {
             // 取得Collecton句柄
             MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
             MongoCollection<Document> collection = database.getCollection(coll);
             // 读取数据
             return collection.count(new BasicDBObject(where));
         }catch(Exception e){
             System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
   		 return 0;
   	}
   	
    // 查询一条记录
   	public PageData queryOne(String coll,PageData where) throws Exception {
   		if( coll==null || where==null ){
   			return null;
   		}
   		if( where.get("ID")!=null ){
   			String id=where.get("ID").toString();
   			if(id.length()>=24){
   				where.put("_id", new ObjectId(id));
   			}else {
   				where.put("_id", id);
			} 
   			where.remove("ID");
		}
   		PageData pd = new PageData();
   		try {
             // 取得Collecton句柄
             MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
             MongoCollection<Document> collection = database.getCollection(coll);
             // 读取数据
             MongoCursor<Document> cursor = collection.find(new BasicDBObject(where)).iterator();
             while (cursor.hasNext()) { 
            	 pd.putAll(cursor.next());
             }
         }catch(Exception e){
             System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
   		 return pd;
   	}
    
    // 查询所有记录
   	public List<PageData> queryAll(String coll,PageData param) throws Exception {
   		if( coll==null ){
   			return null;
   		}
   		List<PageData> list = new ArrayList<PageData>();
   		PageData where = new PageData();
   		PageData sort = new PageData();
   		if( param.get("where")!=null ){
   			where = (PageData) param.get("where");
   		}
   		if( param.get("sort")!=null ){
   			sort = (PageData) param.get("sort");
   		}	
   		try {
             // 取得Collecton句柄
             MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
             MongoCollection<Document> collection = database.getCollection(coll);
             // 读取数据
             MongoCursor<Document> cursor = collection.find(new BasicDBObject(where)).sort(new BasicDBObject(sort)).iterator();
             while (cursor.hasNext()) {
            	 PageData pd = new PageData();
            	 pd.putAll(cursor.next());
            	 list.add(pd);
             }
         }catch(Exception e){
             System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
   		 return list;
   	}
   	
   	/** 
     * 分页查询 
     * @param page 
     * @param pageSize 
     * @param db 
     * @param coll 
     * @return List<PageData>
   	 * @throws Exception 
     */  
    public List<PageData> pageList(String coll,PageData param) throws Exception{  
   		if( coll==null ){
   			return null;
   		}
   		List<PageData> list = new ArrayList<PageData>();
   		int page = 1,pageSize=10;
   		PageData where = new PageData();
   		BasicDBObject sort = new BasicDBObject();
   		PageData singleSort = new PageData();
   		Map multiSort=new LinkedHashMap();
   		if( param.get("page")!=null ){
   			page = (Integer) param.get("page");
   		}
   		if( param.get("pageSize")!=null ){
   			pageSize = (Integer) param.get("pageSize");
   		}
   		if( param.get("where")!=null ){
   			where = (PageData) param.get("where");
   		}
   		if( param.get("sort")!=null ){ 
   			singleSort = (PageData) param.get("sort");
   			sort = new BasicDBObject(singleSort);
   		}
   		if( param.get("multiSort")!=null ){ 
   			multiSort = (LinkedHashMap) param.get("multiSort");
   			sort = new BasicDBObject(multiSort);
   		}
   		try {
             // 取得Collecton句柄
             MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
             MongoCollection<Document> collection = database.getCollection(coll);
             // 读取数据
             MongoCursor<Document> cursor = collection.find(new BasicDBObject(where))
            		  .skip(page)    // 跳过前面指定数量的数据  
            		  .sort(sort)  // 数据集按age字段进行正序排序
            		  .limit(pageSize)                // 限制返回条数. 
            		  .iterator();                    // 迭代
             while (cursor.hasNext()) {
            	 PageData pd = new PageData();
            	 pd.putAll(cursor.next());
            	 Object _id = pd.get("_id"); 
            	 pd.put("_id", _id.toString());
            	 list.add(pd);
             }
         }catch(Exception e){
             System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
   		 return list;
    }  
   	
    // 插入一条
   	public String insertOne(String coll,PageData data) throws Exception {
   		if( coll!=null && data!=null ){
   			try {
   	   		    // 取得Collecton句柄
   	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
   	            MongoCollection<Document> collection = database.getCollection(coll);
   	            // 插入数据
   	            Document doc = new Document(data);
   	            collection.insertOne(doc);
   	            Object _id = doc.get("_id");
   	            if ( _id!=null ){
   	                return _id.toString();
   	            }
   	        }catch(Exception e){
   	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
   	        }
   		}
   		return null;
   	}
   	
    // 插入多条
   	public int insertMany(String coll,List<PageData> data) throws Exception {
   		if( coll!=null && data!=null ){
   			try {
   		   		// 取得Collecton句柄
	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
	            MongoCollection<Document> collection = database.getCollection(coll);
	            long num = collection.count();
	            //插入文档  
	            /** 
	            * 1. 创建文档 org.bson.Document 参数为key-value的格式 
	            * 2. 创建文档集合List<Document> 
	            * 3. 将文档集合插入数据库集合中 mongoCollection.insertMany(List<Document>) 插入单个文档可以用 mongoCollection.insertOne(Document) 
	            * */
	            List<Document> documents = new ArrayList<Document>(); 
	            for( PageData pd : data ){
	            	documents.add(new Document(pd));  
	            }
	            collection.insertMany(documents);  
	            if (collection.count()-num > 0){
   	                System.out.println("添加数据成功！！！");
   	                return 1;
   	            }
	        }catch(Exception e){
	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	        }
   		}
   		return 0;
   	}

    // 修改
   	public int updateOne(String coll,PageData param) throws Exception {
   		PageData where = (PageData) param.get("where");
   		if( where.get("ID")!=null ){
   			String id=where.get("ID").toString();
   			if(id.length()>=24){
   				where.put("_id", new ObjectId(id));
   			}else {
   				where.put("_id", id);
			} 
   			where.remove("ID");
		}
   		PageData data = (PageData) param.get("data");
   		if( coll!=null && where!=null && data!=null ){
   			try {
   	   		    // 取得Collecton句柄
   	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
   	            MongoCollection<Document> collection = database.getCollection(coll);
   	            UpdateResult updateResult = collection.updateMany(new BasicDBObject(where),new BasicDBObject("$set",new BasicDBObject(data)));
   	            if (updateResult.getModifiedCount() > 0)
	   	        {
	   	            return 1;
	   	        }
   	        }catch(Exception e){
   	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
   	        }
   		}
   		return 0;
   	}
   	
    // 批量修改
   	public int updateMany(String coll,PageData param) throws Exception {
   		PageData where = (PageData) param.get("where");
   		PageData data = (PageData) param.get("data");
   		if( coll!=null && where!=null && data!=null ){
   			try {
   	   		    // 取得Collecton句柄
   	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
   	            MongoCollection<Document> collection = database.getCollection(coll);
   	            //更新文档   将文档中likes=100的文档修改为likes=200   
   	            // collection.updateMany(Filters.eq("likes", 100), new Document("$set",new Document(data)));
   	            UpdateResult updateResult = collection.updateMany(new BasicDBObject(where),new BasicDBObject("$set",new BasicDBObject(data)));
   	            if (updateResult.getModifiedCount() > 0)
   	   	        {
   	   	            return 1;
   	   	        } 
   	   		}catch(Exception e){
   	             System.err.println( e.getClass().getName() + ": " + e.getMessage() );
   	        }
   		}
   		return 0;
   	}
   	
    // 删除一条
   	public int deleteOne(String coll,PageData where) throws Exception {
   		if( coll!=null && where!=null ){
   			if( where.get("ID")!=null ){
   	   			String id=where.get("ID").toString();
   	   			if(id.length()>=24){
   	   				where.put("_id", new ObjectId(id));
   	   			}else {
   	   				where.put("_id", id);
   				} 
   	   			where.remove("ID");
   			}
   			try {
	   		// 取得Collecton句柄
	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
	            MongoCollection<Document> collection = database.getCollection(coll);
	            //删除符合条件的第一个文档  
	            DeleteResult deleteResult = collection.deleteOne(new BasicDBObject(where));  
	            if (deleteResult.getDeletedCount() > 0)
	   	        {
	   	            return 1;
	   	        } 
	        }catch(Exception e){
	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	        }
   		}
   		return 0;
   	}
   	
    // 删除多条
   	public int deleteMany(String coll,PageData where) throws Exception {
   		if( coll!=null && where!=null ){
   			if( where.get("ID")!=null ){
   	   			String id=where.get("ID").toString();
   	   			if(id.length()>=24){
   	   				where.put("_id", new ObjectId(id));
   	   			}else {
   	   				where.put("_id", id);
   				} 
   	   			where.remove("ID");
   			}
   			try {
   		   		// 取得Collecton句柄
	            MongoDatabase database = mongoClient.getDatabase(DEMO_DB);
	            MongoCollection<Document> collection = database.getCollection(coll);
	            //删除所有符合条件的文档 
	            DeleteResult deleteResult = collection.deleteMany(new BasicDBObject(where));
	            if (deleteResult.getDeletedCount() > 0)
	   	        {
	   	            return 1;
	   	        } 
	        }catch(Exception e){
	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	        }
   		}
   		return 0;
   	}
   	
   	public static void main(String[] args) {  
   		PageData pd = new PageData();
   		MongoUtil mongo=MongoUtil.getMongoUtil();
   		try {
   			long curr = System.currentTimeMillis();
   			System.out.println("curr:"+curr);
   			
   			// 查询记录总数
   			//long l = mongo.queryCount(DEMO_COLL,null);
   			//System.out.println("l:"+l);
   			
   			// 查询单条
   			//pd.put("name", "zwt");
   			//pd.put("count", 0);
   			//mongo.queryOne(DEMO_COLL, pd);
   			
   			// 查询所有
   			//PageData where = new PageData();
   			//where.put("count", new BasicDBObject("$gt",0)); // 大于0
   			//where.put("name", "zwt");                       // name等于‘zwt’
   			//pd.put("where", where);
   			//List<PageData> list = mongo.queryAll(DEMO_COLL, pd);
   			//System.out.println("list:"+list.size());
   			
   			// 分页查询
   			//pd.put("page", 1);
   			//pd.put("pageSize", 10);
   			//PageData where = new PageData();
   			//where.put("count", new BasicDBObject("$lt",1)); // 小于1
			//where.put("name", "zwt"); 
   			//pd.put("where", where); 
   			//PageData sort = new PageData(); // 排序：1、正序；-1倒序。
   			//sort.put("count", -1); 
   			//pd.put("sort", sort); 
   			//List<PageData> list = mongo.pageList(DEMO_COLL,pd);
   			//for( PageData p : list ){
   			//	System.out.println("p:"+p);
   			//}
   			
   			// 单条插入
   			//pd.put("name", "zwt006");
   			//pd.put("count", 6);
   			//String id = mongo.insertOne(DEMO_COLL, pd);
   			//System.out.println("id:"+id);
   			
   		    // 多条插入
   			//List<PageData> list = new ArrayList<PageData>();
   			//pd.put("name", "zwt007");
   			//pd.put("count", 7);
   			//list.add(pd);
   			//PageData pd1 = new PageData();
   			//pd1.put("name", "zwt008");
   			//pd1.put("count", 8);
   			//list.add(pd1);
   			//int i = mongo.insertMany(DEMO_COLL, list);
   			//System.out.println("i:"+i);
   			
   			// 单条修改
   			//PageData where = new PageData();
   			//where.put("name", "zwt");
   			//pd.put("where", where);
   			//PageData data = new PageData();
   			//data.put("text", "测试123456");
   			//data.put("count", 1);
   			//pd.put("data", data);
   			//int i = mongo.updateOne(DEMO_COLL, pd);
   			//System.out.println("i:"+i);
   			
   			// 多条修改
   			//PageData where = new PageData();
   			//where.put("count", 0);
   			//pd.put("where", where);
   			//PageData data = new PageData();
   			//data.put("text", "123456");
   			//pd.put("data", data);
   			//int i = mongo.updateMany(DEMO_COLL, pd);
   			//System.out.println("i:"+i);
   			
   			// 单条删除
   			//pd.put("count", 5);
   			//int i = mongo.deleteOne(DEMO_COLL, pd);
   			//System.out.println("i:"+i);
   			
   		    // 多条删除
   			//pd.put("count", new BasicDBObject("$gte",2)); // 大于等于
   			//int i = mongo.deleteMany(DEMO_COLL, pd);
   			//System.out.println("i:"+i);
			
   			
   			// 查询模糊所有
//   			PageData where = new PageData();
//   			String treeIndex="jack";
//   			Pattern pattern = Pattern.compile("^" + treeIndex + ".*$", Pattern.CASE_INSENSITIVE);
//   			where.put("user_name", new 	BasicDBObject("$regex",pattern));   
//   			pd.put("where", where);
//   			List<PageData> list = mongo.queryAll(DEMO_COLL, pd);
//   			System.out.println("list:"+list.size());
   			
//   			Object[] s = new Object[10];//字符串数组
//   			s[0]="";
//   			PageData pds = new PageData();
//   			PageData wheres = new PageData();
//   			wheres.put("_id", new BasicDBObject("$in",s));
//   			pds.put("where", wheres);
//   			List<PageData> lists = mongo.queryAll(USER_COLL, pds);
//   			System.out.println("lists:"+lists.size());
//   			Object  total= mongo.queryCount(DEMO_COLL,where);
   			
   			long curr2 = System.currentTimeMillis();
   			System.out.println("curr2:"+curr2);
   			System.out.println("curr2-curr:"+(curr2-curr));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    } 
}
