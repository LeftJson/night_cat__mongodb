package com.fh.util.wangyi;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fh.controller.app.AppController;
import com.fh.util.MD5;
import com.fh.util.PageData;

/**
 * 发送模板短信
 *
 */
@Controller
@RequestMapping({ "/appSend" })
public class SendTemplate extends AppController{
	private static final String SERVER_URL="https://api.netease.im/sms/sendcode.action"; // 发送验证码的请求路径URL
    private static final String VERIFY_URL="https://api.netease.im/sms/verifycode.action"; // 校验验证码
	private static final String APP_KEY="2958171ede0551ca697dd37373e52808";//网易云信分配的账号
    private static final String APP_SECRET="d315f1b9145b";//网易云信分配的密钥
    private static final String NONCE="123456";//随机数
    private static final String CODELEN="6";//验证码长度，范围4～10，默认为4
	
	@RequestMapping({ "/sendmessage" })
	@ResponseBody
    public String sendMsg(HttpServletRequest request) throws IOException {
		PageData pd=new PageData();
		pd = getPageData();
		String mobile = pd.getString("mobile");                    // 目标手机号码
	    String templateid = "4032380";                             // 夜猫验证码短信模板
		
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost(SERVER_URL);

        String curTime=String.valueOf((new Date().getTime()/1000L));
        String checkSum=CheckSumBuilder.getCheckSum(APP_SECRET,NONCE,curTime);//得到 发送验证码必须的参数checkSum

        //设置请求的header	
        post.addHeader("AppKey",APP_KEY);
        post.addHeader("Nonce",NONCE);
        post.addHeader("CurTime",curTime);
        post.addHeader("CheckSum",checkSum);
        post.addHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");

        //设置请求参数
        List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();  
        nameValuePairs.add(new BasicNameValuePair("mobile",mobile));
        nameValuePairs.add(new BasicNameValuePair("templateid", templateid));
        nameValuePairs.add(new BasicNameValuePair("codeLen", CODELEN));

        post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"utf-8"));

        //执行请求
        HttpResponse response=httpclient.execute(post);
        String responseEntity= EntityUtils.toString(response.getEntity(),"utf-8");
        System.out.println("验证码："+responseEntity);
        //判断是否发送成功，发送成功返回true
        JSONObject jsonObj= JSON.parseObject(responseEntity);
        int code = jsonObj.getInteger("code");
        if( code == 200  ){
        	String obj = jsonObj.getString("obj");
        	jsonObj.put("obj", MD5.md5(obj));
        }
        return jsonObj.toString();
    }
	
	@RequestMapping({ "/sendMsg" })
	@ResponseBody
    public String sendMsgById(HttpServletRequest request) throws IOException {
		PageData pd=new PageData();
		pd = getPageData();
		String mobile = pd.getString("mobile");                    // 目标手机号码
	    String templateid = pd.getString("template");              // 短信模板
		
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost(SERVER_URL);

        String curTime=String.valueOf((new Date().getTime()/1000L));
        String checkSum=CheckSumBuilder.getCheckSum(APP_SECRET,NONCE,curTime);//得到 发送验证码必须的参数checkSum

        //设置请求的header	
        post.addHeader("AppKey",APP_KEY);
        post.addHeader("Nonce",NONCE);
        post.addHeader("CurTime",curTime);
        post.addHeader("CheckSum",checkSum);
        post.addHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");

        //设置请求参数
        List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();  
        nameValuePairs.add(new BasicNameValuePair("mobile",mobile));
        nameValuePairs.add(new BasicNameValuePair("templateid", templateid));
        nameValuePairs.add(new BasicNameValuePair("codeLen", CODELEN));

        post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"utf-8"));

        //执行请求
        HttpResponse response=httpclient.execute(post);
        String responseEntity= EntityUtils.toString(response.getEntity(),"utf-8");
        System.out.println("验证码："+responseEntity);
        //判断是否发送成功，发送成功返回true
        JSONObject jsonObj= JSON.parseObject(responseEntity);
        int code = jsonObj.getInteger("code");
        if( code == 200  ){
        	String obj = jsonObj.getString("obj");
        	jsonObj.put("obj", MD5.md5(obj));
        }
        return jsonObj.toString();
    }
	
	// 校验验证码
	@RequestMapping({ "/verifyCode" })
	@ResponseBody
    public String verifyCode(HttpServletRequest request) throws IOException {
		PageData pd=new PageData();
		pd = getPageData();
		String mobile = pd.getString("mobile");               // 手机号码
	    String code = pd.getString("code");                   // 验证码
		
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost(VERIFY_URL);

        String curTime=String.valueOf((new Date().getTime()/1000L));
        String checkSum=CheckSumBuilder.getCheckSum(APP_SECRET,NONCE,curTime);//得到 发送验证码必须的参数checkSum

        //设置请求的header	
        post.addHeader("AppKey",APP_KEY);
        post.addHeader("Nonce",NONCE);
        post.addHeader("CurTime",curTime);
        post.addHeader("CheckSum",checkSum);
        post.addHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");

        //设置请求参数
        List<NameValuePair> nameValuePairs =new ArrayList<NameValuePair>();  
        nameValuePairs.add(new BasicNameValuePair("mobile",mobile));
        nameValuePairs.add(new BasicNameValuePair("code", code));

        post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"utf-8"));

        //执行请求
        HttpResponse response=httpclient.execute(post);
        String responseEntity= EntityUtils.toString(response.getEntity(),"utf-8");
        System.out.println("校验结果："+responseEntity);
        JSONObject jsonObj= JSON.parseObject(responseEntity);
        return jsonObj.toString();
    }
}

