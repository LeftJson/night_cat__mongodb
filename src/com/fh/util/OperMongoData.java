package com.fh.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

public class OperMongoData {
	
	
	public static Map getDatatabe(List<Map> list) {
		Map<String, String> data = new HashMap();
        if( list.size() <= 0 ){
        	return data; 
        }
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        for (Map map : list) {
    		String key=(String) map.get("key");
    		String value=(String) map.get("value");
    		if( key != null && value != null ){
    			params.add(new BasicNameValuePair(key, value));
    		}
		}
        try {
            String url = "http://47.100.34.193:3000/webApi";
            System.out.println("-------------华丽的分割线--------------");
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            String dataStr = EntityUtils.toString(httpEntity, "utf-8");
            Gson token_gson = new Gson();
            System.out.println("-------------华丽的分割线--------------");
            // System.out.println("dataStr："+dataStr);
            data = token_gson.fromJson(dataStr, Map.class); 
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return data;
    } 
	
}
