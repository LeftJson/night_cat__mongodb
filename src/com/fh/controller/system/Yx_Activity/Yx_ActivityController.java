package com.fh.controller.system.Yx_Activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.service.system.Yx_Roel.Yx_RoelService;
import com.fh.service.system.authenti.AuthentiService;
import com.fh.util.FileUpload;
import com.fh.util.PageData;
import com.fh.util.SystemLog;
import com.fh.util.fileConfig;
import com.fh.util.aliyun.MongoUtil;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_activity" })
public class Yx_ActivityController extends BaseController{
	String menuUrl = "yx_activity.do";
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String USER_COLL = "users";  //用户集合
	public static String DEMO_COLL = "activitys"; //活动集合
	public static String ACTUSER_COLL = "activity_users"; //活动参与人集合
	
	@Resource(name = "yxroel")
	private Yx_RoelService yxroel;
	
	//上传文件存放路径
	public static String serverBasePath = fileConfig.getString("sierverBasePath");	
	//public static String serverBasePath = "http://101.132.96.90:8080/upload/img/";
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
	  ModelAndView mv = getModelAndView(); 
	  mv.setViewName("system/activity/activity-list");
	  return mv;
	}
	
	 @RequestMapping({ "/queryList" })
	  @ResponseBody
	  public String queryList(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("ACT_NAME"); 
	      
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	    PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("title", new BasicDBObject("$regex",pattern)); 
		where.put("is_del", 0); 
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序
		sort.put("start_date", -1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(DEMO_COLL,pdd);
		PageData actdata=new PageData(); 
		actdata.put("USER_ID", getUser().getID());
		PageData rudata= this.yxroel.queryById(actdata);
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			auddata.put("apd", rudata.get("DEL_QX"));
			
			if(!"".equals(auddata.get("end_date"))){
				long actdate=Long.parseLong(auddata.get("end_date").toString()); 
				Date actdate2=new Date(actdate);
				long ser_date=new Date().getTime();
				if(actdate - ser_date < 0){ 
					PageData wheres = new PageData();
		   			wheres.put("ID", auddata.get("_id"));
		   			pd.put("where", wheres);
		   			PageData data = new PageData();
		   			data.put("state", 3); 
		   			pd.put("data", data);
		   			mongo.updateOne(DEMO_COLL, pd);    
					auddata.put("actstate", "已结束");
					
				}else {
					auddata.put("actstate", "报名中");
				}
			}else {
				auddata.put("actstate", "已结束");
			}
			
		}
//		Object[] s = new Object[list.size()];//字符串数组 
//		for (int i = 0; i < list.size(); i++) {
//			PageData auddata=list.get(i); 
//			String userid=auddata.get("user_id").toString();  
//			if (userid.length() >= 24) {
//				s[i]=new ObjectId(userid);
//			}else { 
//				s[i]=userid;
//			} 
//		} 
//		
//		PageData pds = new PageData();
//		PageData wheres = new PageData();
//		wheres.put("_id", new BasicDBObject("$in",s));
//		pds.put("where", wheres);
//		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
//		for (int i = 0; i < list.size(); i++) {
//			PageData pa=list.get(i); 
//			for (PageData pag : lists) {
//				if(pa.get("user_id").equals(pag.get("_id").toString())){
//					 pa.put("user_name", pag.get("user_name"));
//				}  
//			} 
//		}   
		Object  total= mongo.queryCount(DEMO_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
	
	 //进入添加活动
	@RequestMapping({ "/queryById" })
    public ModelAndView queryById()throws Exception{
      ModelAndView mv = getModelAndView(); 
      mv.setViewName("system/activity/add-activity");
      return mv;
    }
	
	
	 @RequestMapping({ "/addActImg" })
    @ResponseBody 
    public JSONObject addActImg(MultipartHttpServletRequest request) throws Exception {
      JSONObject obj = new JSONObject();
      PageData pd = new PageData();  
      String title=request.getParameter("title");
      String start_date=request.getParameter("start_date");
      String end_date=request.getParameter("end_date");
      String due_date=request.getParameter("due_date");
      String state=request.getParameter("state");
      String enrollment=request.getParameter("enrollment");
      String description=request.getParameter("description");
      pd.put("title",title);
      pd.put("start_date",Long.parseLong(start_date));
      pd.put("end_date",Long.parseLong(end_date));
      pd.put("due_date",Long.parseLong(due_date));
      pd.put("state",state);
      pd.put("enrollment",enrollment);
      pd.put("description",description);
      String[] a =null;
      pd.put("participants",a);
      pd.put("is_del", 0);
      MultipartFile cover=request.getFile("cover");
      
      if ((cover != null) && (!cover.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(cover,filePath,get32UUID());
        pd.put("cover", fileName); 
      }  
      
      MultipartFile poster_img=request.getFile("poster_img");
      
      if ((poster_img != null) && (!poster_img.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(poster_img,filePath,get32UUID());
        pd.put("poster_img", fileName); 
      } 
      
      MultipartFile details_img=request.getFile("details_img");
      
      if ((details_img != null) && (!details_img.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(details_img,filePath,get32UUID());
        pd.put("details_img", fileName); 
      }  
		String id = mongo.insertOne(DEMO_COLL, pd);
		System.out.println("id:"+id);
      obj.put("statusCode", 200);  
      return obj;
    }
	 
	 @RequestMapping({ "/toEdit" })
    public ModelAndView toEdit()throws Exception{
      ModelAndView mv = getModelAndView();
      PageData pd = new PageData();
      pd = getPageData();  
      pd= mongo.queryOne(DEMO_COLL, pd);
      mv.setViewName("system/activity/edit-activity");
      mv.addObject("pd",pd);
      return mv;
    }
	 
	 
	 
	 @RequestMapping({ "/editActImg" })
    @ResponseBody 
    public JSONObject editActImg(MultipartHttpServletRequest request) throws Exception {
      JSONObject obj = new JSONObject();
      PageData pd = new PageData();  
      String ID=request.getParameter("ID");
      String title=request.getParameter("title");
      
     
      
      String start_date=request.getParameter("start_date");
      String end_date=request.getParameter("end_date");
      String due_date=request.getParameter("due_date");
     
      String state=request.getParameter("state");
      String enrollment=request.getParameter("enrollment");
      String description=request.getParameter("description");
      
      //pd.put("is_del", 0);
      MultipartFile cover=request.getFile("cover");
      PageData data = new PageData(); 
      if ((cover != null) && (!cover.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(cover,filePath,get32UUID());
        data.put("cover", fileName); 
      }  
      
      MultipartFile poster_img=request.getFile("poster_img");
      
      if ((poster_img != null) && (!poster_img.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(poster_img,filePath,get32UUID());
        data.put("poster_img", fileName); 
      } 
      
      MultipartFile details_img=request.getFile("details_img");
      
      if ((details_img != null) && (!details_img.isEmpty())) {
        String filePath =serverBasePath; 
        String fileName = FileUpload.fileUp(details_img,filePath,get32UUID());
        data.put("details_img", fileName); 
      }  
      	PageData where = new PageData();
		where.put("ID", ID);
		pd.put("where", where);
		
		data.put("title",title);
		data.put("start_date",start_date);
		data.put("end_date",end_date);
		data.put("due_date",due_date);
		data.put("state",state);
		data.put("enrollment",enrollment);
		data.put("description",description);
		pd.put("data", data);
		int i = mongo.updateOne(DEMO_COLL, pd);
		 if(i >=1){
			 obj.put("statusCode", 200);
		 }else{
			 obj.put("statusCode", 400);
		 } 
		 
      return obj;
    }
	 
	 
	 @RequestMapping({ "/delactivitybyid" })
	@SystemLog(methods="活动管理",type="删除")
	@ResponseBody
	public JSONObject delactivitybyid() {
		PageData pd = new PageData();
		JSONObject obj = new JSONObject();
		try {
			pd = getPageData();  
			PageData where = new PageData();
		      where.put("ID", pd.get("ID").toString());
		      pd.put("where", where);
		      PageData data = new PageData();
		      data.put("is_del", "1"); 
		      pd.put("data", data);
		      int i = mongo.updateOne(DEMO_COLL, pd); 
		      if(i >=1){
		    	  obj.put("statusCode", 200);
		      }else{
		    	  obj.put("statusCode", 400);
		      } 
		    	
			} catch (Exception e) {
				this.logger.error(e.toString(), e);
			}
		return obj;
	}
	 
	 
	 @RequestMapping(value={"/toBidder"})
	public ModelAndView list() throws Exception {
	  ModelAndView mv = getModelAndView();
	  PageData pd = new PageData();
	  pd = getPageData();  
	  mv.setViewName("system/activity/activity-Userlist"); 
	  mv.addObject("pd",pd);
	  return mv;
	}
	 
	 
	 @RequestMapping({ "/queryActUserList" })
	  @ResponseBody
	  public String queryActUserList(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("user_name"); 
	    String ACTID = pd.getString("ID"); 
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	    PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
//		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
//		where.put("title", new BasicDBObject("$regex",pattern)); 
		where.put("act_id", ACTID);
		where.put("is_del", 0); 
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序
		sort.put("create_date", -1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(ACTUSER_COLL,pdd);  
		Object[] s = new Object[list.size()];//字符串数组 
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			String userid=auddata.get("user_id").toString();  
			if (userid.length() >= 24) {
				s[i]=new ObjectId(userid);
			}else { 
				s[i]=userid;
			} 
		} 
		
		PageData pds = new PageData();
		PageData wheres = new PageData();
		wheres.put("_id", new BasicDBObject("$in",s));
		pds.put("where", wheres);
		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
		for (int i = 0; i < list.size(); i++) {
			PageData pa=list.get(i); 
			for (PageData pag : lists) {
				if(pa.get("user_id").equals(pag.get("_id").toString())){
					 pa.put("user_name", pag.get("user_name"));
					 pa.put("real_name", pag.get("real_name"));
					 pa.put("city", pag.get("city"));
					 pa.put("phone", pag.get("phone"));
				}  
			} 
		}   
		Object  total= mongo.queryCount(ACTUSER_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
	 
	 @RequestMapping({ "/delete" }) 
	@ResponseBody
	public String delete() {
		PageData pd = new PageData(); 
		try {
			pd = getPageData(); 
			PageData where = new PageData();
   			where.put("ID", pd.get("ID"));
   			pd.put("where", where);
   			PageData data = new PageData();
   			data.put("state", 1); 
   			pd.put("data", data);
   			mongo.updateOne(ACTUSER_COLL, pd); 
		} catch (Exception e) {
			this.logger.error(e.toString(), e);
		}
		JSONObject getObj = new JSONObject();
		getObj.put("statusCode", 200);//
		return getObj.toString(); 
	}
}
