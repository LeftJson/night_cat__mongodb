package com.fh.controller.system.yx_Member;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.app.AppController;
import com.fh.entity.Page;
import com.fh.util.FileUpload;
import com.fh.util.OperMongoData;
import com.fh.util.PageData;
import com.fh.util.PushUtil;
import com.fh.util.fileConfig;
import com.fh.util.aliyun.MongoUtil;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_auditRecords" })
public class Yx_AuditRecordsController extends AppController{
	
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String DEMO_COLL = "auditRecords";  //审核记录集合
	public static String USER_COLL = "users";  //用户集合
	public static String CERTI_COLL = "userCertificates";  //用户集合
	public static String PUSH_COLL = "systemNotices";  //用户集合
	
	String menuUrl = "yx_auditRecords.do";
	
	 //上传文件存放路径
	  public static String sierverBasePath = fileConfig.getString("sierverBasePath");
	
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
		ModelAndView mv = getModelAndView();
		mv.setViewName("system/ymAuthenti/Authentiindex");
		return mv;
	}
	
//	 @RequestMapping({ "/querytbAuthenti" })
//	  @ResponseBody
//	  public String querytbpay(Page page)throws Exception{
//	    PageData pd = new PageData();
//	    pd = getPageData();
//
//	    JSONObject getObj = new JSONObject();
//	    String sEcho = "0";// 记录操作的次数  每次加1  
//	    String iDisplayStart = "0";// 起始  
//	    String iDisplayLength = "10";// size  
//	    String mDataProp = "mDataProp_";
//	    String sortName = "";//排序字段
//	    String iSortCol_0 = "";//排序索引
//	    String sSortDir_0 = "";//排序方式
//	    //获取jquery datatable当前配置参数  
//	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
//	    for (int i = 0; i < jsonArray.size(); i++)  
//	    {  
//	        try  
//	        {  
//	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
//	            if (jsonObject.get("name").equals("sEcho")){ 
//	                sEcho = jsonObject.get("value").toString();  
//	            }
//	            else if (jsonObject.get("name").equals("iDisplayStart")){
//	                iDisplayStart = jsonObject.get("value").toString(); 
//	            }
//	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
//	                iDisplayLength = jsonObject.get("value").toString(); 
//	            }
//	            else if (jsonObject.get("name").equals("sSortDir_0")){
//	              sSortDir_0 = jsonObject.get("value").toString(); 
//	            }
//	            else if (jsonObject.get("name").equals("iSortCol_0")){
//	              iSortCol_0 = jsonObject.get("value").toString();
//	              mDataProp = mDataProp+""+iSortCol_0;
//	              for (int j = 0; j < jsonArray.size(); j++)  {
//	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
//	                if(jsonObject1.get("name").equals(mDataProp)){
//	                  sortName = jsonObject1.get("value").toString(); 
//	                }
//	              }
//	            }
//	            
//	        }  
//	        catch (Exception e) { 
//	          e.printStackTrace();
//	            break;  
//	        }  
//	    }   
//	     
//	    String contractName = pd.getString("user_name"); 
//	      
//	    page.setPd(pd);
//	    page.setOrderDirection(sSortDir_0);
//	    page.setOrderField(sortName);
//	    page.setPageSize(Integer.valueOf(iDisplayLength));
//	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
//	    int initEcho = Integer.parseInt(sEcho) + 1;  
//	     
//	   PageData pdd = new PageData();
//	    
//	    // 分页查询
//	    pdd.put("page", page.getPageCurrent());
//	    pdd.put("pageSize", page.getPageSize());
//		PageData where = new PageData();
////		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
////		where.put("real_name", new BasicDBObject("$regex",pattern)); 
//		where.put("is_del", 0); 
//		pdd.put("where", where); 
//		Map multiSort=new LinkedHashMap();// 排序：1、正序；-1倒序。  
//		multiSort.put("audit_state", 1);  
//		multiSort.put("audit_date", -1); 
//		System.out.println(multiSort);
//		pdd.put("multiSort", multiSort); 
//		List<PageData> list = mongo.pageList(DEMO_COLL,pdd);
//		Object[] s = new Object[list.size()];//字符串数组
//		for (int i = 0; i < list.size(); i++) {
//			PageData auddata=list.get(i); 
//			String userid=auddata.get("user_id").toString(); 
//			if (userid.length() >= 24) {
//				s[i]=new ObjectId(userid);
//			}else { 
//				s[i]=userid;
//			}
//			
//		} 
//		
//		PageData pds = new PageData();
//		PageData wheres = new PageData();
//		wheres.put("_id", new BasicDBObject("$in",s));
//		pds.put("where", wheres);
//		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
//		for (int i = 0; i < list.size(); i++) {
//			PageData pa=list.get(i); 
//			for (PageData pag : lists) {
//				if(pa.get("user_id").equals(pag.get("_id").toString())){
//					 pa.put("user_name", pag.get("user_name"));
//				}  
//			} 
//		}  
//		Object  total= mongo.queryCount(DEMO_COLL,where);
//	    
//	    
//	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
//	    getObj.put("iTotalRecords", total);//实际的行数
//	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
//	     
//	    getObj.put("aaData", list);//要以JSON格式返回
//	    return getObj.toString();
//	  }
	 
	 
	 @RequestMapping({ "/querytbAuthenti" })
	  @ResponseBody
	  public String querytbpay(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	            
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("user_name"); 
	      
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	   
	    PageData pds=new PageData();
	    if(!"".equals(contractName) && !"null".equals(contractName)){
	    	PageData where = new PageData();
	 		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
	 		where.put("user_name", new BasicDBObject("$regex",pattern));  
	 		pds.put("where", where);
	    }
	   
		List<PageData> list = mongo.queryAll(USER_COLL, pds);
	    
		Object[] s = new Object[list.size()];//字符串数组
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			String userid=auddata.get("_id").toString(); 
//			if (userid.length() >= 24) {
//				s[i]=new ObjectId(userid);
//			}else { 
				s[i]=userid;
//			}
			
		} 
		
		PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData wheres = new PageData(); 
		wheres.put("user_id", new BasicDBObject("$in",s));
		wheres.put("is_del", 0); 
		pdd.put("where", wheres); 
		Map multiSort=new LinkedHashMap();// 排序：1、正序；-1倒序。  
		multiSort.put("audit_state", 1);  
		multiSort.put("audit_date", -1);  
		pdd.put("multiSort", multiSort); 
		List<PageData> lists = mongo.pageList(DEMO_COLL,pdd); 
		for (int i = 0; i < lists.size(); i++) {
			PageData pa=lists.get(i); 
			for (PageData pag : list) {
				if(pa.get("user_id").equals(pag.get("_id").toString())){
					 pa.put("user_name", pag.get("user_name"));
				}  
			} 
		}   
		Object  total= mongo.queryCount(DEMO_COLL,wheres);
		
		
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", lists);//要以JSON格式返回
	    return getObj.toString();
	  }
	 
	 
	 
	 
	@RequestMapping({ "/toexamine" })
	public ModelAndView toexamine() throws Exception {
		ModelAndView mv = getModelAndView();    
		PageData pd= getPageData(); 
		// 查询单条    
		pd= mongo.queryOne(DEMO_COLL, pd); 
		
		
		PageData pds=new PageData();
		PageData where = new PageData();
		String user_id=pd.get("user_id").toString();
		where.put("ID", user_id);
		pds = mongo.queryOne(USER_COLL, where); 
		 
		PageData pdsd=new PageData();
		PageData wheres = new PageData();
		wheres.put("user_id",pd.get("user_id").toString()); 
		pdsd.put("where", wheres);
		List<PageData> list = mongo.queryAll(CERTI_COLL, pdsd);  
		mv.addObject("pd",pd);
		mv.addObject("pds",pds);
		mv.addObject("pdlist",list);
	    mv.setViewName("system/ymAuthenti/authenti_examine");  
		return mv;
	}
	
	@RequestMapping(value={"/upexamine"})
	 @ResponseBody
	 public JSONObject upexamine(MultipartHttpServletRequest request)throws Exception{ 
	     PageData pd = new PageData();
	     JSONObject getObj = new JSONObject();
	     Object ob=0;
	     try {  
	    	 pd=getPageData();   
	    	 String ID=request.getParameter("ID"); 
	    	 if(ID==null||"".equals(ID)){ 
	    		 getObj.put("statusCode", 400); 
	    		 return getObj;
	    	 }else{ 
	    		 String audit_state=request.getParameter("audit_state");
	    		 
	    		// 单条修改
	    		 PageData pad=new PageData();
    			PageData where = new PageData();
    			where.put("ID", ID);
    			pad.put("where", where);
    			PageData data = new PageData();
    			String audit_final_state=request.getParameter("audstate");
    			data.put("audit_state", request.getParameter("audit_state"));
				data.put("audit_failure_reason", request.getParameter("audit_failure_reason")); 
				PageData userdata=new PageData();
    			if("2".equals(audit_state)){ 
    				userdata.put("authenticating_state", Integer.parseInt(audit_final_state));  
    				if ("6".equals(audit_final_state)) {
						data.put("audit_state", 2); 
	    				MultipartFile file=request.getFile("document");  
	      		      if ((file != null) && (!file.isEmpty())) {
	      		        String filePath =sierverBasePath; 
	      		        String fileName = FileUpload.fileUp(file,filePath,get32UUID());
	      		        data.put("audit_failure_reason", fileName); 
	      		      } 
					}  
    			}else if ("1".equals(audit_state)) {
    				if ("2".equals(audit_final_state)) {
    					userdata.put("authenticating_state", 1.1);  
    				}else if ("4".equals(audit_final_state)) {
    					userdata.put("authenticating_state", 3.1);  
					}else if ("6".equals(audit_final_state)) {
    					userdata.put("authenticating_state", 5.1);  
					}
				}
    			
    			  
    			
    			data.put("audit_date", new Date().getTime());
    			
    			pad.put("data", data);
    			int i = mongo.updateOne(DEMO_COLL, pad);
    			//修改用户状态
    			PageData wherea = new PageData();
       			wherea.put("ID", request.getParameter("user_id"));
       			pd.put("where", wherea); 
       			pd.put("data", userdata);
       			int ia = mongo.updateOne(USER_COLL, pd);
       			
       			
       			System.out.println("i:"+i);
    			
    			
    			 String bt="建联夜猫";
  				 
 				 String alias = request.getParameter("user_id"); 
	 	 		 PageData notif=new PageData();
	 	 		 notif.put("user_id", request.getParameter("user_id"));
	 	 		 notif.put("object_id", request.getParameter("ID"));
	 	 		 notif.put("object_type", 2);
	 	 		 notif.put("title", bt); 
	 	 		 notif.put("type", 0);
	 	 		 notif.put("state", 0);
	 	 		 notif.put("read_number", 0);
	 	 		 notif.put("create_date", System.currentTimeMillis());
	 	 		 notif.put("is_del", 0);
    			 if(i >=1){
    				 String nr="";
    				 if("2".equals(request.getParameter("audit_state"))){
    					 nr="您的证件审核成功！";
	   	 				 
    				 }else if ("1".equals(request.getParameter("audit_state"))) {
    					 nr="您的证件审核失败！";
					} 
    				 
    				 String jsonStr = "{'type':2,'ID':'"+request.getParameter("ID")+"','title':'"+bt+"','content':'"+nr+"'}";//透传内容
  	 				  TransmissionTemplate tmTemplates = PushUtil.transmissionTemplateDemo(bt,nr,jsonStr);
  					  PushUtil pushApp=new PushUtil();
      	   	 		 if(notif.getString("user_id")!=null&&!"".equals(notif.getString("user_id"))){
      	   	 			notif.put("content", nr);
      	   	 			 mongo.insertOne(PUSH_COLL, notif);
      	   	 			 
      	   	 		     //推送
      					 String[] id = {notif.getString("user_id")};
      					 pushApp.pushToSingle(tmTemplates,alias); 
      	   	 		 }
    				 getObj.put("statusCode", 200);
    			 }else{
    				 
    				 getObj.put("statusCode", 400);
    			 } 
	    		 
	    	 }  
		 } catch (Exception e) {
		        this.logger.error(e.toString(), e); 
		 } 
		 return getObj; 
	 }  
	
	//查看详情
	@RequestMapping({ "/todetails" })
	public ModelAndView todetails() throws Exception {
		ModelAndView mv = getModelAndView();    
		PageData pd= getPageData(); 
		// 查询单条    
		pd= mongo.queryOne(DEMO_COLL, pd); 
		
		
		PageData pds=new PageData();
		PageData where = new PageData();
		String user_id=pd.get("user_id").toString();
		where.put("ID", user_id);
		pds = mongo.queryOne(USER_COLL, where); 
		 
		PageData pdsd=new PageData();
		PageData wheres = new PageData();
		wheres.put("user_id",pd.get("user_id").toString()); 
		pdsd.put("where", wheres);
		List<PageData> list = mongo.queryAll(CERTI_COLL, pdsd);  
		mv.addObject("pd",pd);
		mv.addObject("pds",pds);
		mv.addObject("pdlist",list);
	    mv.setViewName("system/ymAuthenti/authenti_details");  
		return mv;
	}
}
