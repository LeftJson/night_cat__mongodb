package com.fh.controller.system.yx_Member;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.app.AppController;
import com.fh.entity.Page;
import com.fh.entity.system.Role;
import com.fh.service.system.role.RoleService;
import com.fh.service.system.user.UserService;
import com.fh.util.PageData;
import com.fh.util.ResultUtil;
import com.fh.util.RightsHelper;
import com.fh.util.SystemLog;
import com.fh.util.Tools;
import com.fh.util.aliyun.MongoUtil;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_user" })
public class Yx_UserController extends AppController{
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String DEMO_COLL = "users";  //集合
	
	String menuUrl = "yx_user/listUsers.do";
	
	@Resource(name="roleService")
	private RoleService roleService;
	
	@Resource(name = "userService")
	private UserService userService;
	
	//跳转到用户的查询页面
	@RequestMapping 
	public ModelAndView list() throws Exception {
		ModelAndView mv = getModelAndView(); 
		mv.setViewName("system/user/list");  
		return mv;
	}
	
	 @RequestMapping({ "/queryByParam" })
	  @ResponseBody
	  public String querytbpay(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	            
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("USERNAME"); 
	    String Phone = pd.getString("Phone"); 
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	   PageData pdd = new PageData();
	    
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("user_name", new BasicDBObject("$regex",pattern));  
		if(!"".equals(Phone) && !"null".equals(Phone)){
		Pattern patterns = Pattern.compile("^.*"+Phone+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("phone", new BasicDBObject("$regex",patterns));  
		}
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序。
		sort.put("create_date", -1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(DEMO_COLL,pdd);
		for( PageData p : list ){
			System.out.println("p:"+p);
		}
		Object  total= mongo.queryCount(DEMO_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
	 
	 //查看详情
	 @RequestMapping({ "/goEditU" })
	public ModelAndView goEditU() throws Exception { 
		ModelAndView mv = getModelAndView();
		PageData pd = new PageData();
		pd = getPageData();  
		// 查询单条    
		pd= mongo.queryOne(DEMO_COLL, pd); 
		System.out.println("查看详情"+pd);
		mv.setViewName("system/user/edit"); 
		mv.addObject("pd", pd); 
		return mv;
	}
	 
	 
	 
	//添加管理员 
	   @RequestMapping({"/SysUsersave"})
	   @SystemLog(methods="添加管理员",type="添加管理员")
	   @ResponseBody
	   public JSONObject SysUsersave() throws Exception {
		 PageData pd = getPageData();
	     JSONObject obj = new JSONObject();
	     String parent_id = get32UUID(); 
		 //角色
	     PageData pds = new PageData(); 
	     
	     pds.put("ROLE_ID", parent_id); 
	     pds.put("ROLE_NAME", "建联后台账号");  
	     pd.put("RIGHTS", ""); 
	     pds.put("QX_ID", "");  
	     pds.put("ADD_QX", "0");
	     pds.put("DEL_QX", "0");
	     pds.put("EDIT_QX", "0");
	     pds.put("CHA_QX", "0");
	     pds.put("SH_QX", "0");  
	     this.roleService.add(pds);
	     /*权限*/
	     PageData pdsd = new PageData(); 
	     String ROLE_ID=parent_id;
	     String menuIds=pd.getString("menuIds");
	     
	     if ((menuIds != null) && (!"".equals(menuIds.trim()))) {
	       BigInteger rights = RightsHelper.sumRights(Tools.str2StrArray(menuIds));
	       Role role = this.roleService.getRoleById(ROLE_ID);
	       role.setRIGHTS(rights.toString());
	       this.roleService.updateRoleRights(role);
	       pdsd.put("rights", rights.toString());
	     } else {
	       Role role = new Role();
	       role.setRIGHTS("");
	       role.setROLE_ID(ROLE_ID);
	       this.roleService.updateRoleRights(role);
	       pdsd.put("rights", "");
	     } 
	     pdsd.put("roleId", ROLE_ID);
	     this.roleService.setAllRights(pdsd); 
	     obj.put("tabid", "role");
	     obj.put("status", "1");
	     obj.put("msg", "success"); 
	      
	     /*账号*/ 
	     Role role = this.roleService.getRoleById(ROLE_ID); 
			pd.put("RIGHTS",role.getRIGHTS());
			pd.put("ID", get32UUID());
			pd.put("STATUS", "0"); 
			pd.put("ROLE_ID", ROLE_ID);
			pd.put("PASSWORD",new SimpleHash("SHA-1",pd.getString("USERNAME"),"123456").toString()); 
			//pd.put("PASSWORD",new SimpleHash("SHA-1", pd.getString("USERNAME"), pd.getString("PASSWORD")).toString());
			pd.put("CREATE_BY",getUser().getID());
			pd.put("CREATE_DATE",new Date()); 
			if (this.userService.findByUId(pd) == null) { 
				this.userService.saveU(pd);
				obj.put("tabid", "user");
			    obj.put("statusCode", "200");
			} else { 
			    obj.put("statusCode", "300");
			}
	     return obj;
	   }
		
		
	   @RequestMapping({"/SysUseredit"})
	   @SystemLog(methods="编辑管理员",type="编辑管理员")
	   @ResponseBody
	   public JSONObject SysUseredit() throws Exception {
		   PageData pd = getPageData();
		   JSONObject obj = new JSONObject(); 
		   try {
		     /*权限*/
		     PageData pdsd = new PageData(); 
		     String ROLE_ID=pd.getString("ROLE_ID");
		     String menuIds=pd.getString("menuIds");
		     
		     if ((menuIds != null) && (!"".equals(menuIds.trim()))) {
		       BigInteger rights = RightsHelper.sumRights(Tools.str2StrArray(menuIds));
		       Role role = this.roleService.getRoleById(ROLE_ID);
		       role.setRIGHTS(rights.toString());
		       this.roleService.updateRoleRights(role);
		       pdsd.put("rights", rights.toString());
		     } else {
		       Role role = new Role();
		       role.setRIGHTS("");
		       role.setROLE_ID(ROLE_ID);
		       this.roleService.updateRoleRights(role);
		       pdsd.put("rights", "");
		     } 
		     pdsd.put("roleId", ROLE_ID);
		     this.roleService.setAllRights(pdsd); 
		     obj.put("tabid", "role");
		     obj.put("status", "1");
		     obj.put("msg", "success"); 
		      
		     /*账号*/ 
		     Role role = this.roleService.getRoleById(pd.getString("ROLE_ID"));
			pd.put("RIGHTS",role.getRIGHTS()); 
			pd.put("ROLE_ID", ROLE_ID);
			pd.put("MODIFY_BY",getUser().getID());
			pd.put("MODIFY_DATE",new Date()); 
			//pd.put("PASSWORD",new SimpleHash("SHA-1",pd.getString("USERNAME"),"123456").toString()); 
			//pd.put("PASSWORD",new SimpleHash("SHA-1", pd.getString("USERNAME"), pd.getString("PASSWORD")).toString());
			
			this.userService.editU(pd);
			obj.put("tabid", "user");
		    obj.put("statusCode", "200");
		    return obj;
		   } catch (Exception e) {
			   obj.put("statusCode", "400");
			    return obj;
			}
	   }
}
