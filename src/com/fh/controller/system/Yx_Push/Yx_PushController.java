package com.fh.controller.system.Yx_Push;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.util.PageData;
import com.fh.util.PushUtil;
import com.fh.util.aliyun.MongoUtil;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_push" })
public class Yx_PushController extends BaseController{
	
	String menuUrl = "yx_push.do";
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String USER_COLL = "users";  //用户集合
	public static String DEMO_COLL = "systemNotices"; //系统通知集合
	
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
		ModelAndView mv = getModelAndView();
		mv.setViewName("system/push/push");
		return mv;
	} 
	
	 
	@RequestMapping({ "/querypushuser" })
	  @ResponseBody
	  public String querypushuser(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();
	
	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	            
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("username"); 
	      
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	   PageData pdd = new PageData();
	    
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("user_name", new BasicDBObject("$regex",pattern)); 
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序。
		sort.put("create_date", -1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(USER_COLL,pdd);
		for( PageData p : list ){
			System.out.println("p:"+p);
		}
		Object  total= mongo.queryCount(USER_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
	
	//推送
	@RequestMapping({ "/pushToApp" })
	@ResponseBody
	public String pushToApp() throws Exception {
		PageData pa=new PageData();
		pa = getPageData();
		JSONObject obj = new JSONObject();
		
		String title = pa.getString("Title");
		String text = pa.getString("Text");
		String jsonStr = "{'type':3,'title':'"+title+"','content':'"+text+"'}";//透传内容
		TransmissionTemplate tmTemplates = PushUtil.transmissionTemplateDemo(title,text,jsonStr);
		NotificationTemplate template = PushUtil.notificationTemplateDemo();
		template.setTitle(title);
		template.setText(text);
		String flag=pa.getString("IS_FREE");
		String result="";
		
		//存储消息内容
		PageData notif=new PageData();
 		notif.put("create_date", new Date().getTime()); 
 		notif.put("object_id","");
 		notif.put("object_type","3"); 
 		notif.put("content",pa.getString("Text"));
 		notif.put("is_del",0); 
 		notif.put("state",0);
 		notif.put("read_number",0); 
 		notif.put("title","建联夜猫");
		
		PushUtil pushApp=new PushUtil();
		//Integer val = 0;
		String pshid="";
		//全部推送  （只存一条通知消息）
		if( "1".equals(flag) ){
			//notif.put("ID", get32UUID());
			notif.put("type", 1);
		    notif.put("object_id", "jianlianyemao");//默认为系统通知消息 
		    pshid=mongo.insertOne(DEMO_COLL, notif);
		    //val = (Integer)sysNotifService.save(notif);
		    result=pushApp.pushToAll(tmTemplates);
		    System.out.println("向全部用户推送："+result.toString());
		}else{
			//adroid推送
			String id =pa.getString("ID");//用户ID（即 别名）
			if( id !=null && !"".equals(id) ){
				String[] ids = id.split(",");
				for (String USER_ID : ids) { 
					notif.put("user_id", USER_ID);
					notif.put("type", 0);
					pshid=mongo.insertOne(DEMO_COLL, notif);
					//val += (Integer)sysNotifService.save(notif);
				}
				
				if(ids.length>9){//批量推送
					result=pushApp.pushToMore(tmTemplates,ids); 
				}else{//少量推送
					result=pushApp.pushToMany(tmTemplates,ids); 
				}
			}
			
			//ios推送
			String ios =pa.getString("IOSID");//用户ID（即 别名）
			if( ios != null && !"".equals(ios) ){
				String[] ids = ios.split(",");
				for (String USER_ID : ids) {
					notif.put("ID", get32UUID());
					notif.put("USER_ID", USER_ID);
					pshid=mongo.insertOne(DEMO_COLL, notif);
					//val += (Integer)sysNotifService.save(notif);
				}
				
				if(ids.length>9){//批量推送
					result=pushApp.pushToIosMore(tmTemplates,ids); 
				}else{//少量推送
					result=pushApp.pushToIosMany(tmTemplates,ids); 
				}
			}
		}
		
		if( "ok".equals(result) || pshid !=null ||!"".equals(pshid)){//发送成功
			obj.put("status", 200); 
		}else{//发送失败
			obj.put("status", 400);
		}  
		return obj.toString();
	} 
}
