package com.fh.controller.system.Yx_Case;

import java.util.List;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.util.PageData;
import com.fh.util.aliyun.MongoUtil;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_case" })
public class Yx_CaseController extends BaseController{
	String menuUrl = "yx_case.do";
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String USER_COLL = "users";  //用户集合
	public static String DEMO_COLL = "personalChw"; //订单集合
	
	@RequestMapping({ "/tocase" })
	  public ModelAndView tocase(Page page) throws Exception {
	    ModelAndView mv = getModelAndView();
	    PageData pd=getPageData();
	    mv.setViewName("system/designer/case");
	    mv.addObject("pd",pd);
	    return mv;
	  }
	
	 @RequestMapping
	  public ModelAndView list(Page page) throws Exception {
	    ModelAndView mv = getModelAndView();
	    PageData pd=getPageData();
	    mv.setViewName("system/designer/case");
	    mv.addObject("pd",pd);
	    return mv;
	  }
	
	 @RequestMapping({ "/querytbcase" })
	  @ResponseBody
	  public String querytbpay(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("name"); 
	    String UID = pd.getString("ID"); 
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	    PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("title", new BasicDBObject("$regex",pattern)); 
		where.put("user_id", UID);
		where.put("type", 0);
		where.put("is_del", 0); 
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序。
		sort.put("create_date", 1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(DEMO_COLL,pdd);
		Object[] s = new Object[list.size()];//字符串数组 
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			String userid=auddata.get("user_id").toString();  
			if (userid.length() >= 24) {
				s[i]=new ObjectId(userid);
			}else { 
				s[i]=userid;
			} 
		} 
		
		PageData pds = new PageData();
		PageData wheres = new PageData();
		wheres.put("_id", new BasicDBObject("$in",s));
		pds.put("where", wheres);
		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
		for (int i = 0; i < list.size(); i++) {
			PageData pa=list.get(i); 
			for (PageData pag : lists) {
				if(pa.get("user_id").equals(pag.get("_id").toString())){
					 pa.put("user_name", pag.get("user_name"));
				}  
			} 
		}   
		Object  total= mongo.queryCount(DEMO_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
}
