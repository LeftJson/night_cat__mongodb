package com.fh.controller.system.school;

import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.system.User;
import com.fh.service.system.school.CommitteeImgService;
import com.fh.service.system.school.SchoolCommitteeService;
import com.fh.util.FileUpload;
import com.fh.util.ImgUploadUtil;
import com.fh.util.PageData;
import com.fh.util.fileConfig;

import net.sf.json.JSONObject;

@Controller
@RequestMapping({"/CommmitteeImg"})
public class CommmitteeImgController extends BaseController{
  @Resource(name = "committeeImgService")
  private CommitteeImgService committeeImgService; 
  
  @Resource(name = "schoolCommitteeService")
  private SchoolCommitteeService schoolCommitteeService;
  

  
  //上传文件存放路径
  public static String videosBasePath = fileConfig.getString("videosBasePath");
  public static String serverBasePath = fileConfig.getString("serverBasePath");
  public static String serverImgPath = fileConfig.getString("serverImgPath");
     
  ImgUploadUtil imgUpload=new ImgUploadUtil();
  @RequestMapping({ "/toLookImgs" })
  public ModelAndView toLookImgs() throws Exception {
    ModelAndView mv = getModelAndView();
    PageData pd = new PageData();  
    try {
      pd = getPageData();  
      PageData pro=schoolCommitteeService.findById(pd); 
      pd.put("RELATED_ID",pd.getString("ID"));
      List<PageData> xmtp =committeeImgService.queryByPid(pd);  
      mv.addObject("pro", pro); 
      mv.addObject("xmtp", xmtp); 
    } catch (Exception e) { 
      e.printStackTrace();
    }    
    mv.setViewName("system/school/committee_img");  
    return mv;
  }

  
  
  @RequestMapping({"/uploadImgs"}) 
  @ResponseBody
  public PageData uploadImgs(){ 
    PageData result=new PageData();
    result = getPageData();  
    try{  
      PageData pa=new PageData(getRequest()); 
      //PageData param=pa.getObject("param"); 
      //List<PageData> imgs=(List<PageData>)data.get("imgs"); 
      PageData data=imgUpload.imgBase64(result); 
      result.put("code","200");
      return  result; 
    }catch(Exception e){
     e.printStackTrace();
     logger.error("图片上传错误：",e); 
      result.put("code","400");
     return  result;
    }  
 }  
  
  //获取当前登录用户
  public User getUser(){
    Subject currentUser = SecurityUtils.getSubject();
    Session session = currentUser.getSession(); 
    User user = (User) session.getAttribute("sessionUser"); 
    return user;
 }

}
