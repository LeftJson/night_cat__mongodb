package com.fh.controller.system.Personnel;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.UPDATE.getCheck;
import com.fh.controller.base.BaseController;
import com.fh.entity.Page; 
import com.fh.service.system.Personnel.ProsService;
import com.fh.service.system.dictionaries.DictionariesService;
import com.fh.util.DateUtil;
import com.fh.util.FileUpload;
import com.fh.util.PageData;
import com.fh.util.ResultUtil;
import com.fh.util.SystemLog;
import com.fh.util.excelexception.Exceldworup;

@Controller
@RequestMapping({ "/perSer" })
//人才管理处理器
public class ProsController extends BaseController{

	String menuUrl = "perSer.do";
	
	@Resource(name = "prosService")
	private ProsService prosService;
	
	@Resource(name = "dictionariesService")
	private DictionariesService dicService;
	
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
		ModelAndView mv = getModelAndView();
		mv.setViewName("system/Personnel/persindex");
		return mv;
	}
	
	@RequestMapping({ "/toAdd" })
	public ModelAndView toAdd() throws Exception {
		ModelAndView mv = getModelAndView();    
		List<PageData> gcflList=this.dicService.queryByPBM("Design_type");
	    mv.setViewName("system/Personnel/pers_add");  
	    mv.addObject("gcflList", gcflList); 
		return mv;
	}
	
	@RequestMapping({ "/querytbuser" })
	@ResponseBody
	public String querytbuser(Page page)throws Exception{
		PageData pd = new PageData();
		pd = getPageData();

		JSONObject getObj = new JSONObject();
		String sEcho = "0";// 记录操作的次数  每次加1  
		String iDisplayStart = "0";// 起始  
		String iDisplayLength = "10";// size  
		String mDataProp = "mDataProp_";
		String sortName = "";//排序字段
		String iSortCol_0 = "";//排序索引
		String sSortDir_0 = "";//排序方式
		//获取jquery datatable当前配置参数  
		JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
		for (int i = 0; i < jsonArray.size(); i++)  
		{  
		    try  
		    {  
		        JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
		        if (jsonObject.get("name").equals("sEcho")){ 
		            sEcho = jsonObject.get("value").toString();  
		        }
		        else if (jsonObject.get("name").equals("iDisplayStart")){
		            iDisplayStart = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iDisplayLength")){ 
		            iDisplayLength = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("sSortDir_0")){
		        	sSortDir_0 = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iSortCol_0")){
		        	iSortCol_0 = jsonObject.get("value").toString();
		        	mDataProp = mDataProp+""+iSortCol_0;
		        	for (int j = 0; j < jsonArray.size(); j++)  {
		        		JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
		        		if(jsonObject1.get("name").equals(mDataProp)){
		        			sortName = jsonObject1.get("value").toString(); 
		        		}
		        	}
		        }
		        
		    }  
		    catch (Exception e) { 
		    	e.printStackTrace();
		        break;  
		    }  
		}   
		 
		String contractName = pd.getString("REAL_NAME"); 
		if ((contractName != null) && (!"".equals(contractName))) { 
			//String param=new String(contractName.getBytes("ISO-8859-1"),"UTF-8").trim();
			pd.put("REAL_NAME",contractName);
		}  
		page.setPd(pd);
		page.setOrderDirection(sSortDir_0);
		page.setOrderField(sortName);
		page.setPageSize(Integer.valueOf(iDisplayLength));
		page.setPageCurrent(Integer.valueOf(iDisplayStart));
		int initEcho = Integer.parseInt(sEcho) + 1;  
		
		List<PageData> data = this.prosService.listByParam(page);
		
		
		 //List list = getCheck.getcapable();
		
		Object  total = this.prosService.findCount(page).get("counts");//查询记录的总行数
		getObj.put("sEcho", initEcho);// 不知道这个值有什么用
		getObj.put("iTotalRecords", total);//实际的行数
		getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
		/*for(PageData datas:data){ 
			datas.put("birthday",datas.get("birthday")==null?"":datas.get("birthday").toString()); 
		} */
		getObj.put("aaData", data);//要以JSON格式返回
		return getObj.toString();
	}
	
	
	
	
	@RequestMapping({ "/delete" })
	@SystemLog(methods="人才管理",type="删除")
	@ResponseBody
	public String delete() {
		PageData pd = new PageData(); 
		JSONObject getObj = new JSONObject();
		Object ob=0;
		try {
			pd = getPageData(); 
			ob=this.prosService.delById(pd);  
			if(Integer.parseInt(ob.toString())>=1){
				getObj.put("statusCode", 200);
			}else{
				getObj.put("statusCode", 400);
			} 
		} catch (Exception e) {
			this.logger.error(e.toString(), e);
		}
		return getObj.toString(); 
	} 
	 
	
	 @RequestMapping(value={"/save"})
	 @SystemLog(methods="人才管理",type="编辑")
	 @ResponseBody
	 public String save()throws Exception{ 
	     PageData pd = new PageData();
	     JSONObject getObj = new JSONObject();
	     Object ob=0;
	     try {  
	    	 pd=getPageData();   
	    	 String ID=pd.getString("ID"); 
	    	 if(ID==null||"".equals(ID)){ 
	 			pd.put("ID", get32UUID());
	    		 //保存
	    		 ob=this.prosService.adduser(pd);  
	    	 }else{//修改 
	    		 pd.put("ID",ID); 
	    		 ob=this.prosService.edit(pd);  
	    	 }  
		 } catch (Exception e) {
		        this.logger.error(e.toString(), e); 
		 }   
	     if(Integer.parseInt(ob.toString())>=1){
	    	 getObj.put("statusCode", 200);
	     }else{
	    	 getObj.put("statusCode", 400);
	     } 
		 return getObj.toString(); 
	 }  
	 
	 
    @RequestMapping({ "/adduserid" })
	@ResponseBody
	public ModelAndView adduserid()throws Exception{
		ModelAndView mv = getModelAndView();
		PageData data = new PageData();
		data = getPageData();
		PageData udata=this.prosService.queryById(data); 
		List<PageData> gcflList=this.dicService.queryByPBM("Design_type");
		mv.setViewName("system/Personnel/pers_edit");
		mv.addObject("data", udata); 
		mv.addObject("gcflList", gcflList); 
		return mv;
	}
     
}
