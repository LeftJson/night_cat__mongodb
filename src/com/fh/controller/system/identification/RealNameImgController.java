package com.fh.controller.system.identification;

import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.system.User;
import com.fh.service.system.identification.IdentificationImgService;
import com.fh.service.system.identification.RealNameService;
import com.fh.util.FileUpload;
import com.fh.util.PageData;
import com.fh.util.fileConfig;

import net.sf.json.JSONObject;

@Controller
@RequestMapping({"/RealNameImg"})
public class RealNameImgController extends BaseController{
  @Resource(name = "identificationImgService")
  private IdentificationImgService identificationImgService; 
  
  @Resource(name = "realNameService")
  private RealNameService realNameService;
  
  //上传文件存放路径
  public static String videosBasePath = fileConfig.getString("videosBasePath");
  public static String serverBasePath = fileConfig.getString("serverBasePath");
  public static String serverImgPath = fileConfig.getString("serverImgPath");
     
  @RequestMapping({ "/toLookImgs" })
  public ModelAndView toLookImgs() throws Exception {
    ModelAndView mv = getModelAndView();
    PageData pd = new PageData();  
    try {
      pd = getPageData();  
      PageData pro=realNameService.findById(pd); 
      pd.put("RELATED_ID",pd.getString("ID"));
      List<PageData> xmtp =identificationImgService.queryByPid(pd);  
      mv.addObject("pro", pro); 
      mv.addObject("xmtp", xmtp); 
    } catch (Exception e) { 
      e.printStackTrace();
    }    
    mv.setViewName("system/identification/identification_img");  
    return mv;
  }
  
  @RequestMapping({ "/uploadImgs" }) 
  @ResponseBody
  public JSONObject uploadImgs(MultipartHttpServletRequest request)throws Exception{
    JSONObject obj = new JSONObject(); 
    PageData pd = new PageData();  
    String RELATED_ID=request.getParameter("RELATED_ID");
    pd.put("RELATED_ID",RELATED_ID); 
    PageData data=identificationImgService.queryMaxOrderby(pd);
    if(data.get("ORDER_BY")!=null){
      pd.put("ORDER_BY", data.get("ORDER_BY").toString());
    }else{
      pd.put("ORDER_BY","1");
    } 
    MultipartFile img=request.getFile("IMG_PATH");  
    if ((img != null) && (!img.isEmpty())) {
      String filePath =serverBasePath; 
      String fileName = FileUpload.fileUp(img,filePath,get32UUID());  
      if(fileName!=null&&!"".equals(fileName)){
        pd.put("IMG_PATH",serverImgPath+fileName.trim()); 
      } 
    }  
    pd.put("ID", get32UUID()); 
    //pd.put("DATE", new Date()); 
    //Object ob=caseImgService.save(pd);
//    if(Integer.parseInt(ob.toString())>0){
//      obj.put("statusCode", 200);   
//    }else{
//      obj.put("statusCode", 400);   
//    } 
    return obj;
  } 
  
  //获取当前登录用户
  public User getUser(){
    Subject currentUser = SecurityUtils.getSubject();
    Session session = currentUser.getSession(); 
    User user = (User) session.getAttribute("sessionUser"); 
    return user;
 }

}
