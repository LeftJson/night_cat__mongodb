package com.fh.controller.system.designer;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.service.system.designer.DesingerHonorService;
import com.fh.util.PageData;
import com.fh.util.ResultUtil;
import com.fh.util.SystemLog;
import com.fh.util.excelexception.Exceldworup;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping({ "/DesignerHonor" })
public class DesignerHonorController extends BaseController{
String menuUrl = "Honor.do";
  
  @Resource(name = "designerhonorService")
  private DesingerHonorService designerhonorService;
  
  @RequestMapping
  public ModelAndView list(Page page) throws Exception {
    ModelAndView mv = getModelAndView();
    mv.setViewName("system/designer/honor");
    return mv;
  }
  

  
  @RequestMapping({ "/querytbhonor" })
  @ResponseBody
  public String querytbhonor(Page page)throws Exception{
    PageData pd = new PageData();
    pd = getPageData();

    JSONObject getObj = new JSONObject();
    String sEcho = "0";// 记录操作的次数  每次加1  
    String iDisplayStart = "0";// 起始  
    String iDisplayLength = "10";// size  
    String mDataProp = "mDataProp_";
    String sortName = "";//排序字段
    String iSortCol_0 = "";//排序索引
    String sSortDir_0 = "";//排序方式
    //获取jquery datatable当前配置参数  
    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
    for (int i = 0; i < jsonArray.size(); i++)  
    {  
        try  
        {  
            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
            if (jsonObject.get("name").equals("sEcho")){ 
                sEcho = jsonObject.get("value").toString();  
            }
            else if (jsonObject.get("name").equals("iDisplayStart")){
                iDisplayStart = jsonObject.get("value").toString(); 
            }
            else if (jsonObject.get("name").equals("iDisplayLength")){ 
                iDisplayLength = jsonObject.get("value").toString(); 
            }
            else if (jsonObject.get("name").equals("sSortDir_0")){
              sSortDir_0 = jsonObject.get("value").toString(); 
            }
            else if (jsonObject.get("name").equals("iSortCol_0")){
              iSortCol_0 = jsonObject.get("value").toString();
              mDataProp = mDataProp+""+iSortCol_0;
              for (int j = 0; j < jsonArray.size(); j++)  {
                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
                if(jsonObject1.get("name").equals(mDataProp)){
                  sortName = jsonObject1.get("value").toString(); 
                }
              }
            }
            
        }  
        catch (Exception e) { 
          e.printStackTrace();
            break;  
        }  
    }   
     
    String contractName = pd.getString("REAL_NAME"); 
    if ((contractName != null) && (!"".equals(contractName))) { 
      //String param=new String(contractName.getBytes("ISO-8859-1"),"UTF-8").trim();
      pd.put("REAL_NAME",contractName);
    }  
    page.setPd(pd);
    page.setOrderDirection(sSortDir_0);
    page.setOrderField(sortName);
    page.setPageSize(Integer.valueOf(iDisplayLength));
    page.setPageCurrent(Integer.valueOf(iDisplayStart));
    int initEcho = Integer.parseInt(sEcho) + 1;  
    
    List<PageData> data = this.designerhonorService.listByParam(page);
    Object  total = this.designerhonorService.findCount(page).get("counts");//查询记录的总行数
    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
    getObj.put("iTotalRecords", total);//实际的行数
    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
    for(PageData pro:data){ 
      ResultUtil.resetRes(pro,new String[]{"create_time"});
    } 
    getObj.put("aaData", data);//要以JSON格式返回
    return getObj.toString();
  }
  
  
//导出Excel
  @RequestMapping(value = "exportfeedback")
    @ResponseBody
    public String exportFeedBack(HttpServletResponse response) throws Exception{
      JSONObject getObj = new JSONObject();
        String fileName = "订单信息"+System.currentTimeMillis()+".xls"; //文件名 
        String sheetName = "订单信息";//sheet名  
        String []title = new String[]{"名称","创建时间","简介","用户编号","类别","查看数"};//标题
        PageData pd= getPageData();
        List<PageData> list = this.designerhonorService.doexlelist(pd);//内容list
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        String [][]values = new String[list.size()][];
        for(int i=0;i<list.size();i++){
            values[i] = new String[title.length];
            //将对象内容转换成string
            PageData obj = list.get(i);
            values[i][0] = obj.getString("name");
            values[i][1] = sdf.format(DateFormat.getDateInstance().parse(obj.getString("create_time")));
            values[i][2] = obj.getString("detail");
            values[i][3] = obj.getString("user_id");
            values[i][4] = obj.getString("type");
            values[i][5] = obj.getString("watch_num");
        }
        
        HSSFWorkbook wb = Exceldworup.getHSSFWorkbook(sheetName, title, values, null);
        
        //将文件存到指定位置  
        try {  
             this.setResponseHeader(response, fileName);  
             OutputStream os = response.getOutputStream();  
             wb.write(os);  
             os.flush();  
             os.close();  
        } catch (Exception e) {  
             e.printStackTrace();  
        }  
        return getObj.toString();
    }
  public void setResponseHeader(HttpServletResponse response, String fileName) {  
    try {  
         try {  
              fileName = new String(fileName.getBytes(),"ISO8859-1");  
         } catch (UnsupportedEncodingException e) {  
              // TODO Auto-generated catch block  
              e.printStackTrace();  
         }  
         response.setContentType("application/octet-stream;charset=ISO8859-1");  
         response.setHeader("Content-Disposition", "attachment;filename="+ fileName);  
         response.addHeader("Pargam", "no-cache");  
         response.addHeader("Cache-Control", "no-cache");  
    } catch (Exception ex) {  
         ex.printStackTrace();  
    }  
} 
  
  
  @RequestMapping({ "/delete" })
  @SystemLog(methods="会员管理",type="删除")
  @ResponseBody
  public String delete() {
    PageData pd = new PageData(); 
    JSONObject getObj = new JSONObject();
    Object ob=0;
    try {
      pd = getPageData(); 
      ob=this.designerhonorService.delById(pd);  
      if(Integer.parseInt(ob.toString())>=1){
        getObj.put("statusCode", 200);
      }else{
        getObj.put("statusCode", 400);
      } 
    } catch (Exception e) {
      this.logger.error(e.toString(), e);
    }
    return getObj.toString(); 
  } 
}
