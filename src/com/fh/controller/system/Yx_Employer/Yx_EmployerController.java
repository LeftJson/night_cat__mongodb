package com.fh.controller.system.Yx_Employer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.util.PageData;
import com.fh.util.SystemLog;
import com.fh.util.aliyun.MongoUtil;
import com.mongodb.BasicDBObject;

@Controller
@RequestMapping({ "/yx_employer" })
public class Yx_EmployerController extends BaseController{
	
	String menuUrl = "yx_employer.do";
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String USER_COLL = "users";  //用户集合
	public static String DEMO_COLL = "orderList"; //订单集合
	public static String TYPE_COLL = "projectTypes"; //字典表集合
	public static String PARTS_COLL = "orderParts"; //参与集合
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
	  ModelAndView mv = getModelAndView(); 
	  PageData pd=new PageData();
	  List<PageData> typelist = mongo.queryAll(TYPE_COLL, pd);
	  mv.setViewName("system/empSer/employer");
	  mv.addObject("typelist",typelist);
	  return mv;
	}
	
	@RequestMapping({ "/querytbemp" })
	  @ResponseBody
	  public String querytbpay(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String contractName = pd.getString("title"); 
	    String projecttype = pd.getString("project_type"); 
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	    PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		Pattern pattern = Pattern.compile("^.*"+contractName+".*$", Pattern.CASE_INSENSITIVE); 
		where.put("project_title", new BasicDBObject("$regex",pattern)); 
		if(!"".equals(projecttype)){
			where.put("project_type", projecttype); 
		}
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序。
		sort.put("create_date", -1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(DEMO_COLL,pdd);
		Object[] s = new Object[list.size()];//字符串数组
		Object[] f = new Object[list.size()];//字符串数组
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			String userid=auddata.get("user_id").toString(); 
			String project_type=auddata.get("project_type").toString(); 
			if (userid.length() >= 24) {
				s[i]=new ObjectId(userid);
			}else { 
				s[i]=userid;
			}
			if (project_type.length() >= 24) {
				f[i]=new ObjectId(project_type);
			}else { 
				f[i]=project_type;
			}
		} 
		
		PageData pds = new PageData();
		PageData wheres = new PageData();
		wheres.put("_id", new BasicDBObject("$in",s));
		pds.put("where", wheres);
		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
		for (int i = 0; i < list.size(); i++) {
			PageData pa=list.get(i); 
			for (PageData pag : lists) {
				if(pa.get("user_id").equals(pag.get("_id").toString())){
					 pa.put("user_name", pag.get("user_name"));
				}  
			} 
		}  
		
		PageData pdtype = new PageData();
		PageData wheress = new PageData();
		wheress.put("_id", new BasicDBObject("$in",f));
		pdtype.put("where", wheress);
		List<PageData> lists1 = mongo.queryAll(TYPE_COLL, pdtype);
		for (int i = 0; i < list.size(); i++) {
			PageData pa=list.get(i); 
			for (PageData pag : lists1) {
				if(pa.get("project_type").equals(pag.get("_id").toString())){
					 pa.put("type_name", pag.get("type_name"));
				}  
			} 
		}  
		Object  total= mongo.queryCount(DEMO_COLL,where); 
		
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回 
	    return getObj.toString();
	  }
	
	@RequestMapping({ "/delete" })
	  @SystemLog(methods="会员管理",type="删除")
	  @ResponseBody
	  public String delete() {
	    PageData pd = new PageData(); 
	    JSONObject getObj = new JSONObject();
	    Object ob=0;
	    try {
	      pd = getPageData();  
	      PageData where = new PageData();
	      where.put("ID", pd.get("ID").toString());
	      pd.put("where", where);
	      PageData data = new PageData();
	      data.put("project_state", "9"); 
	      pd.put("data", data);
	      int i = mongo.updateOne(DEMO_COLL, pd); 
	      if(i >=1){
	        getObj.put("statusCode", 200);
	      }else{
	        getObj.put("statusCode", 400);
	      } 
	    } catch (Exception e) {
	      this.logger.error(e.toString(), e);
	    }
	    return getObj.toString(); 
	  }
	
	@RequestMapping(value={"/toBidder"})
	public ModelAndView list() throws Exception {
	  ModelAndView mv = getModelAndView();
	  PageData pd = new PageData();
	  pd = getPageData();   
	  mv.addObject("pd", pd);
	  mv.setViewName("system/empSer/orderJoinPerson"); 
	  return mv;
	}
	
	
	@RequestMapping({ "/querytborderJoinPerson" })
	  @ResponseBody
	  public String querytborderJoinPerson(Page page)throws Exception{
	    PageData pd = new PageData();
	    pd = getPageData();

	    JSONObject getObj = new JSONObject();
	    String sEcho = "0";// 记录操作的次数  每次加1  
	    String iDisplayStart = "0";// 起始  
	    String iDisplayLength = "10";// size  
	    String mDataProp = "mDataProp_";
	    String sortName = "";//排序字段
	    String iSortCol_0 = "";//排序索引
	    String sSortDir_0 = "";//排序方式
	    //获取jquery datatable当前配置参数  
	    JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
	    for (int i = 0; i < jsonArray.size(); i++)  
	    {  
	        try  
	        {  
	            JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
	            if (jsonObject.get("name").equals("sEcho")){ 
	                sEcho = jsonObject.get("value").toString();  
	            }
	            else if (jsonObject.get("name").equals("iDisplayStart")){
	                iDisplayStart = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iDisplayLength")){ 
	                iDisplayLength = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("sSortDir_0")){
	              sSortDir_0 = jsonObject.get("value").toString(); 
	            }
	            else if (jsonObject.get("name").equals("iSortCol_0")){
	              iSortCol_0 = jsonObject.get("value").toString();
	              mDataProp = mDataProp+""+iSortCol_0;
	              for (int j = 0; j < jsonArray.size(); j++)  {
	                JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
	                if(jsonObject1.get("name").equals(mDataProp)){
	                  sortName = jsonObject1.get("value").toString(); 
	                }
	              }
	            }
	        }  
	        catch (Exception e) { 
	          e.printStackTrace();
	            break;  
	        }  
	    }   
	     
	    String projectID = pd.getString("ID"); 
	    page.setPd(pd);
	    page.setOrderDirection(sSortDir_0);
	    page.setOrderField(sortName);
	    page.setPageSize(Integer.valueOf(iDisplayLength));
	    page.setPageCurrent(Integer.valueOf(iDisplayStart));
	    int initEcho = Integer.parseInt(sEcho) + 1;  
	     
	    PageData pdd = new PageData();
	    // 分页查询
	    pdd.put("page", page.getPageCurrent());
	    pdd.put("pageSize", page.getPageSize());
		PageData where = new PageData();
		where.put("order_id", projectID); 
		pdd.put("where", where); 
		PageData sort = new PageData(); // 排序：1、正序；-1倒序。
		sort.put("create_date", 1); 
		pdd.put("sort", sort); 
		List<PageData> list = mongo.pageList(PARTS_COLL,pdd);
		Object[] s = new Object[list.size()];//字符串数组
		for (int i = 0; i < list.size(); i++) {
			PageData auddata=list.get(i); 
			String userid=auddata.get("user_id").toString();  
			if (userid.length() >= 24) {
				s[i]=new ObjectId(userid);
			}else { 
				s[i]=userid;
			}
		} 
		
		PageData pds = new PageData();
		PageData wheres = new PageData();
		wheres.put("_id", new BasicDBObject("$in",s));
		pds.put("where", wheres);
		List<PageData> lists = mongo.queryAll(USER_COLL, pds);
		for (int i = 0; i < list.size(); i++) {
			PageData pa=list.get(i); 
			for (PageData pag : lists) {
				if(pa.get("user_id").equals(pag.get("_id").toString())){
					 pa.put("user_name", pag.get("user_name"));
					 pa.put("user_phone", pag.get("phone"));
				}  
			} 
		}   
		Object  total= mongo.queryCount(PARTS_COLL,where);
	    
	    
	    getObj.put("sEcho", initEcho);// 不知道这个值有什么用
	    getObj.put("iTotalRecords", total);//实际的行数
	    getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
	     
	    getObj.put("aaData", list);//要以JSON格式返回
	    return getObj.toString();
	  }
	
	
	@RequestMapping(value={"/queryemp"})
	public ModelAndView queryemp() throws Exception {
	  ModelAndView mv = getModelAndView();
	  PageData pd = new PageData();
	  pd = getPageData();   
	  pd=mongo.queryOne(DEMO_COLL, pd);
	  pd.put("project_startTime", Math.round((Double) pd.get("project_startTime")));
	  pd.put("project_endTime", Math.round((Double) pd.get("project_endTime")));
	  pd.put("project_deadLine", Math.round((Double) pd.get("project_deadLine")));
	  PageData pds = new PageData();
	  List<PageData> lists = mongo.queryAll(USER_COLL, pds);
	  for (PageData pag : lists) {
			if(pd.get("user_id").equals(pag.get("_id").toString())){
				pd.put("user_name", pag.get("user_name")); 
			}  
		} 
	  
	  List<PageData> lists1 = mongo.queryAll(TYPE_COLL, pds);  
		for (PageData pag : lists1) {
			if(pd.get("project_type").equals(pag.get("_id").toString())){
				 pd.put("type_name", pag.get("type_name"));
			}  
		}  
	  mv.addObject("pd", pd);
	  mv.setViewName("system/empSer/orderJoin_details"); 
	  return mv;
	}
}
