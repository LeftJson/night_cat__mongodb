package com.fh.controller.system.user;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.UPDATE.getCheck;
import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.entity.system.Authenti;
import com.fh.entity.system.Ym_user;
import com.fh.service.system.authenti.AuthentiService;
import com.fh.service.system.user.TbUserService;
import com.fh.service.system.user.YmUserService;
import com.fh.util.DateUtil;
import com.fh.util.FileUpload;
import com.fh.util.OperMongoData;
import com.fh.util.PageData;
import com.fh.util.ResultUtil;
import com.fh.util.SystemLog;
import com.fh.util.excelexception.Exceldworup;
import com.google.gson.Gson;

@Controller
@RequestMapping({ "/authenti" })
//认证管理处理器
public class AuthentiController extends BaseController{

	
	String menuUrl = "authenti.do";
	
	@Resource(name = "authentiService")
	private AuthentiService authentiService;
	
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
		ModelAndView mv = getModelAndView();
		mv.setViewName("system/ymAuthenti/Authentiindex");
		return mv;
	}
	
	@RequestMapping({ "/toAdd" })
	public ModelAndView toAdd() throws Exception {
		ModelAndView mv = getModelAndView();    
	    mv.setViewName("system/ymAuthenti/authenti_add");  
		return mv;
	}
	
	
	@RequestMapping({ "/toexamine" })
	public ModelAndView toexamine() throws Exception {
		ModelAndView mv = getModelAndView();    
		PageData todata= getPageData();
		todata=this.authentiService.queryById(todata);
		mv.addObject("pd",todata);
	    mv.setViewName("system/ymAuthenti/authenti_examine");  
		return mv;
	}
	
	@RequestMapping({ "/querytbAuthenti" })
	@ResponseBody
	public String querytbAuthenti(Page page)throws Exception{
		PageData pd = new PageData();
		pd = getPageData();

		JSONObject getObj = new JSONObject();
		String sEcho = "0";// 记录操作的次数  每次加1  
		String iDisplayStart = "0";// 起始  
		String iDisplayLength = "10";// size  
		String mDataProp = "mDataProp_";
		String sortName = "";//排序字段
		String iSortCol_0 = "";//排序索引
		String sSortDir_0 = "";//排序方式
		//获取jquery datatable当前配置参数  
		JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
		for (int i = 0; i < jsonArray.size(); i++)  
		{  
		    try  
		    {  
		        JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
		        if (jsonObject.get("name").equals("sEcho")){ 
		            sEcho = jsonObject.get("value").toString();  
		        }
		        else if (jsonObject.get("name").equals("iDisplayStart")){
		            iDisplayStart = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iDisplayLength")){ 
		            iDisplayLength = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("sSortDir_0")){
		        	sSortDir_0 = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iSortCol_0")){
		        	iSortCol_0 = jsonObject.get("value").toString();
		        	mDataProp = mDataProp+""+iSortCol_0;
		        	for (int j = 0; j < jsonArray.size(); j++)  {
		        		JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
		        		if(jsonObject1.get("name").equals(mDataProp)){
		        			sortName = jsonObject1.get("value").toString(); 
		        		}
		        	}
		        }
		        
		    }  
		    catch (Exception e) { 
		    	e.printStackTrace();
		        break;  
		    }  
		}   
		page.setPd(pd);
		page.setOrderDirection(sSortDir_0);
		page.setOrderField(sortName);
		page.setPageSize(Integer.valueOf(iDisplayLength));
		page.setPageCurrent(Integer.valueOf(iDisplayStart));
		int initEcho = Integer.parseInt(sEcho) + 1;  
		
		List<PageData> data = this.authentiService.listByParam(page);
		//List data = getCheck.getStatus();
		
		
		Object  total = this.authentiService.findCount(page).get("counts");//查询记录的总行数
		getObj.put("sEcho", initEcho);// 不知道这个值有什么用
		getObj.put("iTotalRecords", total);//实际的行数
		getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
//		for(PageData pro:data){ 
//			ResultUtil.resetRes(pro,new String[]{"BIRTHDAY","CREATE_DATE"});
//		} 
		getObj.put("aaData", data);//要以JSON格式返回
		return getObj.toString();
	}
	
	//审核
	 @RequestMapping(value={"/upexamine"})
	 @ResponseBody
	 public String upexamine()throws Exception{ 
	     PageData pd = new PageData();
	     JSONObject getObj = new JSONObject();
	     Object ob=0;
	     try {  
	    	 pd=getPageData();   
	    	 String ID=pd.getString("ID"); 
	    	 if(ID==null||"".equals(ID)){ 
	    		 getObj.put("statusCode", 400); 
	    		 return getObj.toString();
	    	 }else{//修改 
	    		 pd.put("ID",ID); 
	    		 pd.put("AUDITOR_ID", getUser().getID());
	    		 pd.put("AUDIT_DATE", new Date());
	    		 ob=this.authentiService.upexamine(pd);
	    		 if(Integer.parseInt(ob.toString())>=1){
	    	           OperMongoData modata=new OperMongoData();
	    	           List<Map> list=new ArrayList<Map>();
	    	           Map map=new HashMap();
	    	           map.put("key", "interfaceId");
	    	           map.put("value", "modifyAuditRecord");
	    	           Map map1=new HashMap();
	    	           map1.put("key", "_id");
	    	           map1.put("value",pd.get("ID"));
	    	           Map map2=new HashMap();
	    	           map2.put("key", "audit_state");
	    	           map2.put("value", pd.get("AUDIT_STATE"));
	    	           Map map3=new HashMap();
	    	           map3.put("key", "auditor_id");
	    	           map3.put("value",pd.get("AUDITOR_ID"));
	    	           Map map4=new HashMap();
	    	           map4.put("key", "audit_failure_reason");
	    	           map4.put("value", pd.get("AUDIT_FAILURE_REASON"));
	    	           list.add(map);
	    	           list.add(map1);
	    	           list.add(map2);
	    	           list.add(map3);
	    	           list.add(map4); 
	    	           Map mongodata = modata.getDatatabe(list);
	    	           System.out.println(mongodata);
	    	           getObj.put("statusCode", 200);
	 			}else{
	 				getObj.put("statusCode", 400);
	 			} 
	    	 }  
		 } catch (Exception e) {
		        this.logger.error(e.toString(), e); 
		 }   
	     if(Integer.parseInt(ob.toString())>=1){
	    	 getObj.put("statusCode", 200);
	     }else{
	    	 getObj.put("statusCode", 400);
	     } 
		 return getObj.toString(); 
	 }  
	 
	
	
	
	  //同步数据
    @RequestMapping({ "/getAuthentilist" })
    @ResponseBody
    public Map<String, String> getAuthentilist() throws Exception{
        Map<String, String> data = new HashMap();
        try {
        	this.authentiService.tbdelete(null);
        	JSONObject getObj = new JSONObject();
           OperMongoData modata=new OperMongoData();
           List<Map> list=new ArrayList<Map>();
           Map map=new HashMap();
           map.put("key", "interfaceId");
           map.put("value", "getAuditRecords");
           list.add(map);
           
           Map mongodata = modata.getDatatabe(list);
           
           
           if( mongodata != null ){
        	   Map result = (Map) mongodata.get("data");
        	   if( result != null ){  
        		   List<Authenti> ymList = new ArrayList<Authenti>();
        		   List<Map> users = (List<Map>) result.get("records");
        		   for( int i=0;i<users.size();i++ ){
        			   Map user = (Map) users.get(i); 
        			   Authenti ym = new Authenti();
        			   ym.setID(user.get("_id").toString());
        			   ym.setUSER_ID(user.get("user_id").toString());
        			   String audit_type = user.get("audit_type").toString();
        			   String []arr= audit_type.split("\\.");
        			   ym.setAUDIT_TYPE(arr[0]);
        			   String audit_state = user.get("audit_state").toString();
        			   String []arr1= audit_state.split("\\.");
        			   ym.setAUDIT_STATE(arr1[0]);
        			   ym.setAUDIT_FAILURE_REASON(user.get("audit_failure_reason").toString());
        			   if(user.get("auditor_id")==null){
        				   ym.setAUDITOR_ID(null);
	    			   }else {
	    				   ym.setAUDITOR_ID(user.get("auditor_id").toString());
	    			   }
        			   if(user.get("audit_date")==null){
        				   ym.setAUDIT_DATE(null);
	    			   }else {
	    				   ym.setAUDIT_DATE(user.get("audit_date").toString());
	    			   }
        			   String final_state = user.get("audit_final_state").toString();
        			   String []arr2= final_state.split("\\.");
        			   ym.setAUDIT_FINAL_STATE(arr2[0]);
        			   ym.setAUDIT_DESCRIBE(user.get("audit_describe").toString());
        			   ym.setISDEL(user.get("is_del").toString());
        			   ymList.add(ym);
	    		   }
        		   Object dat=(Integer) this.authentiService.addsavelists(ymList);
	   			    if(Integer.parseInt(dat.toString())>=1){
	       				getObj.put("statusCode", 200);
	       			}else{
	       				getObj.put("statusCode", 400);
	       			} 
        	   }
           }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return data;
    }
    
    
}
