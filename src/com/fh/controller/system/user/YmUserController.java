package com.fh.controller.system.user;

import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse; 
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fh.controller.UPDATE.getCheck;
import com.fh.controller.base.BaseController;
import com.fh.entity.Page;
import com.fh.entity.system.Ym_user;
import com.fh.service.system.user.TbUserService;
import com.fh.service.system.user.YmUserService;
import com.fh.util.DateUtil;
import com.fh.util.FileUpload;
import com.fh.util.OperMongoData;
import com.fh.util.PageData;
import com.fh.util.ResultUtil;
import com.fh.util.SystemLog;
import com.fh.util.excelexception.Exceldworup;
import com.google.gson.Gson;

@Controller
@RequestMapping({ "/ymUser" })
public class YmUserController extends BaseController{

	String menuUrl = "ymUser.do";
	
	@Resource(name = "ymuserService")
	private YmUserService ymuserService;
	
	@RequestMapping
	public ModelAndView list(Page page) throws Exception {
		ModelAndView mv = getModelAndView();
		mv.setViewName("system/ymuser/members");
		return mv;
	}
	
	@RequestMapping({ "/toAdd" })
	public ModelAndView toAdd() throws Exception {
		ModelAndView mv = getModelAndView();    
	    mv.setViewName("system/ymuser/user_add");  
		return mv;
	}
	
	@RequestMapping({ "/querytbuser" })
	@ResponseBody
	public String querytbuser(Page page)throws Exception{
		PageData pd = new PageData();
		pd = getPageData();

		JSONObject getObj = new JSONObject();
		String sEcho = "0";// 记录操作的次数  每次加1  
		String iDisplayStart = "0";// 起始  
		String iDisplayLength = "10";// size  
		String mDataProp = "mDataProp_";
		String sortName = "";//排序字段
		String iSortCol_0 = "";//排序索引
		String sSortDir_0 = "";//排序方式
		//获取jquery datatable当前配置参数  
		JSONArray jsonArray = JSONArray.fromObject(pd.getString("aoData"));  
		for (int i = 0; i < jsonArray.size(); i++)  
		{  
		    try  
		    {  
		        JSONObject jsonObject = (JSONObject)jsonArray.get(i);  
		        if (jsonObject.get("name").equals("sEcho")){ 
		            sEcho = jsonObject.get("value").toString();  
		        }
		        else if (jsonObject.get("name").equals("iDisplayStart")){
		            iDisplayStart = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iDisplayLength")){ 
		            iDisplayLength = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("sSortDir_0")){
		        	sSortDir_0 = jsonObject.get("value").toString(); 
		        }
		        else if (jsonObject.get("name").equals("iSortCol_0")){
		        	iSortCol_0 = jsonObject.get("value").toString();
		        	mDataProp = mDataProp+""+iSortCol_0;
		        	for (int j = 0; j < jsonArray.size(); j++)  {
		        		JSONObject jsonObject1 = (JSONObject)jsonArray.get(j);  
		        		if(jsonObject1.get("name").equals(mDataProp)){
		        			sortName = jsonObject1.get("value").toString(); 
		        		}
		        	}
		        }
		        
		    }  
		    catch (Exception e) { 
		    	e.printStackTrace();
		        break;  
		    }  
		}   
		 
		String contractName = pd.getString("REAL_NAME"); 
		if ((contractName != null) && (!"".equals(contractName))) { 
			//String param=new String(contractName.getBytes("ISO-8859-1"),"UTF-8").trim();
			pd.put("REAL_NAME",contractName);
		}  
		page.setPd(pd);
		page.setOrderDirection(sSortDir_0);
		page.setOrderField(sortName);
		page.setPageSize(Integer.valueOf(iDisplayLength));
		page.setPageCurrent(Integer.valueOf(iDisplayStart));
		int initEcho = Integer.parseInt(sEcho) + 1;  
		
		List<PageData> data = this.ymuserService.listByParam(page);
		Object  total = this.ymuserService.findCount(page).get("counts");//查询记录的总行数
		getObj.put("sEcho", initEcho);// 不知道这个值有什么用
		getObj.put("iTotalRecords", total);//实际的行数
		getObj.put("iTotalDisplayRecords", total);//显示的行数,这个要和上面写的一样
		for(PageData pro:data){ 
			ResultUtil.resetRes(pro,new String[]{"BIRTHDAY","CREATE_DATE"});
		} 
		getObj.put("aaData", data);//要以JSON格式返回
		return getObj.toString();
	}
	
	
	
	
	@RequestMapping({ "/delete" })
	@SystemLog(methods="会员管理",type="删除")
	@ResponseBody
	public String delete(String ID) {
		PageData pd = new PageData(); 
		JSONObject getObj = new JSONObject();
		Object ob=0;
		try {
			pd = getPageData(); 
			
			//getCheck.mongolDel(ID, "users");
			System.out.println("删除成功");
			ob=this.ymuserService.delById(pd);  
			if(Integer.parseInt(ob.toString())>=1){
				getObj.put("statusCode", 200);
			}else{
				getObj.put("statusCode", 400);
			} 
		} catch (Exception e) {
			this.logger.error(e.toString(), e);
		}
		return getObj.toString(); 
	} 
	 
	
	 @RequestMapping(value={"/save"})
	 @SystemLog(methods="会员管理",type="编辑")
	 @ResponseBody
	 public String save()throws Exception{ 
	     PageData pd = new PageData();
	     JSONObject getObj = new JSONObject();
	     Object ob=0;
	     try {  
	    	 pd=getPageData();   
	    	 String ID=pd.getString("ID"); 
	    	 if(ID==null||"".equals(ID)){//新增  
	    		String password = new SimpleHash("SHA-1",pd.getString("numUser"),pd.getString("password")).toString();
	 			pd.put("password",password);   
	 			pd.put("ID", get32UUID());
	    		 //保存店铺信息
	    		 ob=this.ymuserService.adduser(pd);  
	    	 }else{//修改 
	    		 pd.put("ID",ID); 
	    		 ob=this.ymuserService.edit(pd);  
	    	 }  
		 } catch (Exception e) {
		        this.logger.error(e.toString(), e); 
		 }   
	     if(Integer.parseInt(ob.toString())>=1){
	    	 getObj.put("statusCode", 200);
	     }else{
	    	 getObj.put("statusCode", 400);
	     } 
		 return getObj.toString(); 
	 }  
	 
	 
    @RequestMapping({ "/adduserid" })
	@ResponseBody
	public ModelAndView adduserid()throws Exception{
		ModelAndView mv = getModelAndView();
		PageData data = new PageData();
		data = getPageData();
		PageData udata=this.ymuserService.queryById(data);  
		mv.setViewName("system/ymuser/user_edit");
		mv.addObject("data", udata); 
		return mv;
	}

    
    //同步数据
    @RequestMapping({ "/getUserlist" })
    @ResponseBody
    public Map<String, String> getUserlist() throws Exception{
        Map<String, String> data = new HashMap();
        try {
        	this.ymuserService.tbdelete(null);
        	JSONObject getObj = new JSONObject();
           OperMongoData modata=new OperMongoData();
           List<Map> list=new ArrayList<Map>();
           Map map=new HashMap();
           map.put("key", "interfaceId");
           map.put("value", "getUsers");
           list.add(map);
           Map mongodata = modata.getDatatabe(list);
           if( mongodata != null ){
        	   Map result = (Map) mongodata.get("data");
        	   if( result != null ){  
        		   List<Ym_user> ymList = new ArrayList<Ym_user>();
        		   List<Map> users = (List<Map>) result.get("users");
        		   for( int i=0;i<users.size();i++ ){
        			   Map user = (Map) users.get(i); 
        			   Ym_user ym = new Ym_user();
        			   ym.setID(user.get("_id").toString());
        			   ym.setNICK_NAME(user.get("user_name").toString());
        			   ym.setREAL_NAME(user.get("real_name").toString());
        			   ym.setPASSWORD(user.get("password").toString());
        			   ym.setPHONE(user.get("phone").toString());
        			   ym.setSEX(user.get("gender").toString());
        			   if("".equals(user.get("birthday").toString())){
        				   ym.setBIRTHDAY(null);
        			   }else {
        				   ym.setBIRTHDAY(user.get("birthday").toString());
        			   }
        			   ym.setCREATE_DATE(String.valueOf(user.get("create_date")));
        			   ym.setIMG(user.get("img").toString());
        			   ym.setWORKING_YEARS(user.get("working_years").toString());
        			   ym.setCOMPOSITE_SCORE(user.get("composite_score").toString());
        			   ym.setAUTHENTICATING_STATE(user.get("authenticating_state").toString());
        			   ym.setUSER_TYPE(user.get("user_type").toString());
        			   ym.setHOURLY_WAGE(user.get("hourly_wage").toString());
        			   ym.setDESCRIPTION(user.get("description").toString());
        			   ym.setID_NUMBER(user.get("id_number").toString());
        			   ym.setSCHOOL_NAME(user.get("school_name").toString());
        			   ym.setCERTIFICATE_NAME(user.get("certificate_name").toString());
        			   ym.setCITY(user.get("city").toString());
        			   ym.setEMAIL(user.get("email").toString());
        			   ym.setORDERS_NUMBER(user.get("orders_number").toString());
        			   ym.setWORKING_HOURS((Double) user.get("working_hours"));
        			   ym.setACCOUNT_BALANCE((Double) user.get("account_balance"));
        			   ym.setCUMULATIVE_INCOME((Double) user.get("cumulative_income"));
        			   ym.setWY_TOKEN(user.get("wy_token").toString());
        			   ymList.add(ym);
	    		   }
        		   Object dat=(Integer) this.ymuserService.addsavelist(ymList);
	   			    if(Integer.parseInt(dat.toString())>=1){
	       				getObj.put("statusCode", 200);
	       			}else{
	       				getObj.put("statusCode", 400);
	       			} 
        	   }
           }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return data;
    }
  
   
}
