package com.fh.controller.app.wy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.org.mozilla.javascript.internal.Undefined;

import Decoder.BASE64Decoder;

import com.alibaba.fastjson.JSONArray;
import com.fh.controller.app.AppController;
import com.fh.util.ImageCompressUtil;
import com.fh.util.PageData;
import com.fh.util.ReturnCode;
import com.fh.util.wangyi.WyUtil;
 
@Controller
@RequestMapping({ "/appWy"})
public class AppWyController extends AppController {  
	WyUtil wy = new WyUtil();
	
	//获取网易云信TOKEN
	@RequestMapping(value={"/getToken"}) 
	@ResponseBody
	public String getWyToken() throws Exception {
		String result = "";
		PageData user=new PageData();
		user = getPageData();
		System.out.println("******************网易云信注册******************");
		if( user != null ){
			/**
			 * 返回参数：
			 * 新建网易账号:
			 * {
			 *     "code":200,
			 *     "info":
			 *     {
			 *         "token":"6d3bd0f635024889da1811bdc5933aa9",
			 *         "accid":"cfbd8d6f1eeb47f59e3a5f74c79360a8",
			 *         "name":"何尔萌"
			 *      }
			 * }
			 * */
			String id = user.getString("id");
			String name = user.getString("user_name");
			System.out.println("id:"+id);
			System.out.println("name:"+name);
			if( id!=null && !"".equals(id) && name!=null && !"".equals(name) ){
				result = wy.createWyAccount(id, name);
				logger.info("获取网易云信TOKEN："+result); 
				return result;
			}
		}
		return result;
	}
	
	//更新并获取新token
	@RequestMapping(value={"/refreshToken"}) 
	@ResponseBody
	public String refreshToken() throws Exception {
		String result = "";
		PageData pd=new PageData();
		pd = getPageData();
		System.out.println("******************网易云信更新token******************");
		if( pd != null ){
			String id = pd.getString("id");
			System.out.println("id:"+id);
			if( id!=null && !"".equals(id) ){
				result = wy.refreshToken(id);
				logger.info("更新并获取新token："+result); 
				return result;
			}
		}
		return result;
	}
	
	//创建群组
	@RequestMapping(value={"/createGroup"}) 
	@ResponseBody
	public String createGroup() throws Exception {
		String result = "";
		PageData pd=new PageData();
		pd = getPageData();
		// tname\owner\members
		System.out.println("******************网易云信创建群组******************");
		if( pd != null ){
			result = wy.createGroup(pd);
			logger.info("创建群组："+result); 
			return result;
		}
		return result;
	}
	
	//拉人进群组
	@RequestMapping(value={"/addToGroup"}) 
	@ResponseBody
	public String addToGroup() throws Exception {
		String result = "";
		PageData pd=new PageData();
		pd = getPageData();
		// tid\owner\members\msg
		System.out.println("******************网易云信拉人进群组******************");
		if( pd != null ){
			result = wy.addToGroup(pd);
			logger.info("拉人进群组："+result); 
			return result;
		}
		return result;
	}
	
	//踢人出群组
	@RequestMapping(value={"/kickFromGroup"}) 
	@ResponseBody
	public String kickFromGroup() throws Exception {
		String result = "";
		PageData pd=new PageData();
		pd = getPageData();
		// tid\owner\members
		System.out.println("******************网易云信踢人出群组******************");
		if( pd != null ){
			result = wy.kickFromGroup(pd);
			logger.info("踢人出群组："+result); 
			return result;
		}
		return result;
	}
	
	//解散群组
	@RequestMapping(value={"/removeGroup"}) 
	@ResponseBody
	public String removeGroup() throws Exception {
		String result = "";
		PageData pd=new PageData();
		pd = getPageData();
		// tid\owner
		System.out.println("******************网易云信解散群组******************");
		if( pd != null ){
			result = wy.removeGroup(pd);
			logger.info("踢人出群组："+result); 
			return result;
		}
		return result;
	}
}
