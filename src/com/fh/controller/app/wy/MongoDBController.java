package com.fh.controller.app.wy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fh.controller.app.AppController;
import com.fh.util.PageData;
import com.fh.util.aliyun.MongoUtil;

@Controller
@RequestMapping({ "/appMongo"})
public class MongoDBController extends AppController {  
	MongoUtil mongo=MongoUtil.getMongoUtil();
	public static String DEMO_COLL = "runoob";  //集合
    // 读取数据
   	@RequestMapping(value={"/queryData"}) 
   	@ResponseBody
   	public List<PageData> queryData() throws Exception {
//   		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
//				.getRequestAttributes()).getRequest();
   		System.out.println("*************************************");
   		PageData pd = new PageData();
   		return mongo.queryAll(DEMO_COLL,pd);
   	}
   	
    // 插入一条
   	@RequestMapping(value={"/insertOne"}) 
   	@ResponseBody
   	public String insertOne() throws Exception {
   		System.out.println("*************************************");
   		System.out.println("MongoDBController insetOne: ");
   		PageData pd = new PageData();
   		mongo.insertOne(DEMO_COLL,pd);
   		return null;
   	}
   	
    // 插入一条
   	@RequestMapping(value={"/insertMany"}) 
   	@ResponseBody
   	public String insertMany() throws Exception {
   		System.out.println("*************************************");
   		System.out.println("MongoDBController insertMany: ");
   		List<PageData> list = new ArrayList<PageData>();
   		mongo.insertMany(DEMO_COLL,list);
   		return null;
   	}
   	
    // 修改
   	@RequestMapping(value={"/updateMany"}) 
   	@ResponseBody
   	public String updateMany() throws Exception {
   		System.out.println("*************************************");
   		System.out.println("MongoDBController updateMany: ");
   		PageData pd = new PageData();
   		mongo.updateMany(DEMO_COLL,pd);
   		return null;
   	}
   	
    // 删除一条
   	@RequestMapping(value={"/deleteOne"}) 
   	@ResponseBody
   	public String deleteOne() throws Exception {
   		System.out.println("*************************************");
   		System.out.println("MongoDBController deleteOne: ");
   		PageData pd = new PageData();
   		mongo.deleteOne(DEMO_COLL,pd);
   		return null;
   	}
   	
    // 删除多条
   	@RequestMapping(value={"/deleteMany"}) 
   	@ResponseBody
   	public String deleteMany() throws Exception {
   		System.out.println("*************************************");
   		System.out.println("MongoDBController deleteMany: ");
   		PageData pd = new PageData();
   		mongo.deleteMany(DEMO_COLL,pd);
   		return null;
   	}
   	
}
