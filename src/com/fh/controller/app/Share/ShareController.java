package com.fh.controller.app.Share;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fh.controller.base.BaseController;
import com.fh.entity.system.Ym_user;
import com.fh.util.OperMongoData;
import com.fh.util.PageData;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping({ "/appShare" })
public class ShareController extends BaseController{
  
    //海报
    @RequestMapping({ "/haibao" })
    @ResponseBody
    public ModelAndView haibao() throws Exception{
    	ModelAndView mv = getModelAndView();
    	PageData da=new PageData();
    	da=getPageData();
        Map<String, String> data = new HashMap(); 
        	JSONObject getObj = new JSONObject();
           OperMongoData modata=new OperMongoData();
           List<Map> list=new ArrayList<Map>();
           Map map=new HashMap();
           map.put("key", "interfaceId");
           map.put("value", "getActivityDetails");
           list.add(map);
           Map map2=new HashMap();
           map2.put("key", "id");
           map2.put("value",da.get("id") );
           list.add(map2);
           Map mongodata = modata.getDatatabe(list);
           if(mongodata!=null){
        	   PageData hbdata=new PageData();
        	   Map hmap=(Map) mongodata.get("data"); 
              if(hmap!=null){
            	  mv.addObject("pd", hmap.get("activity"));
              }
           }
           mv.setViewName("app/ym/haibao");
        return mv;
    }
    
    //问答详情
    @RequestMapping({ "/wdxq" })
    @ResponseBody
    public ModelAndView wdxq() throws Exception{
    	ModelAndView mv = getModelAndView();
    	PageData da=new PageData();
    	da=getPageData();
        Map<String, String> data = new HashMap(); 
        	JSONObject getObj = new JSONObject();
           OperMongoData modata=new OperMongoData();
           List<Map> list=new ArrayList<Map>();
           Map map=new HashMap();
           map.put("key", "interfaceId");
           map.put("value", "getPChwDetails");
           list.add(map);
           Map map2=new HashMap();
           map2.put("key", "id");
           map2.put("value",da.get("id") );
           list.add(map2);
           Map mongodata = modata.getDatatabe(list);
           if(mongodata!=null){
        	   PageData hbdata=new PageData();
        	   Map hmap=(Map) mongodata.get("data"); 
              if(hmap!=null){
            	  mv.addObject("pd", hmap.get("chw"));
              }
           }
           mv.setViewName("app/ym/wdxq");
        return mv;
    } 
    
}
