package com.fh.controller.app.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fh.controller.app.AppController;
import com.fh.util.ImageCompressUtil;
import com.fh.util.PageData;
import com.fh.util.ReturnCode;
import com.fh.util.fileConfig;

import Decoder.BASE64Decoder;
 
@Controller
@RequestMapping({ "/appUploadImg"})
public class AppUploadImgController extends AppController {  
	
	//上传文件存放路径
	public static String clientBasePath = fileConfig.getString("clientBasePath");
	
	//上传文件存放路径
	public static String imgBasePath = fileConfig.getString("imgBasePath"); 
	
	 @RequestMapping(value={"/imgBase64"}) 
	 @ResponseBody
	 public PageData imgBase64() throws Exception{  
		long startTime=System.currentTimeMillis();
		PageData _result=new PageData();
		PageData pa=getPageData();
		try{
			String img=pa.getString("img").trim();  
	    	if (img == null){//图像数据为空  
	    		_result.put("code", 400);
				_result.put("msg", "上传图片失败！");	 
				logger.error("错误:图像数据为空,上传图片失败！");
			}else{ 
				long stage=System.currentTimeMillis();
				BASE64Decoder decoder = new BASE64Decoder();  
				//Base64解码  
	            byte[] b = decoder.decodeBuffer(img);  
	            for(int i=0;i<b.length;++i){  
	                if(b[i]<0){//调整异常数据
	                    b[i]+=256;  
	                }  
	            } 
	            //生成png图片   
	            String imgName=get32UUID()+".png"; 
	            // String imgFilePath =clientBasePath+imgName; //新生成的图片 
	            String imgFilePath =clientBasePath+imgName; //新生成的图片 
	            File file = new File(imgFilePath);
	            if (!file.exists()) {
                    // 先得到文件的上级目录，并创建上级目录，在创建文件
                    file.getParentFile().mkdir();
                    file.createNewFile();
                    file.setExecutable(true);//设置可执行权限  
                    file.setReadable(true);//设置可读权限  
                    file.setWritable(true);//设置可写权限  
                }
	            OutputStream out = new FileOutputStream(file);      
	            out.write(b);  
	            out.flush();  
	            out.close();
	            ImageCompressUtil.compressFile(imgFilePath); 
	            _result.put("result",imgName);
	            _result.put("code", ReturnCode.SUCCESS);
				_result.put("msg", "上传图片成功！");	
				logger.info(_result.getString("msg"));
			 } 
		}catch(Exception e){
			e.printStackTrace();
			logger.error("图片上传错误：",e);
			_result.put("code", ReturnCode.SERVICE_FAIL);
			_result.put("msg", "服务器繁忙，请稍后操作");
			return _result;
		}
		long endTime=System.currentTimeMillis();
		long time=endTime-startTime;
		logger.info("imgBase64消耗时间为："+time+"毫秒，共计："+time/1000+"秒！！！！！");   
        return _result;
	}
	 
	 @RequestMapping(value={"/imgBase64_2"}) 
	 @ResponseBody
	 public PageData imgBase64_2() throws Exception{ 
		long startTime=System.currentTimeMillis();
		PageData _result=new PageData();
		PageData pa=getPageData();
		try{
			String img=pa.getString("img");  
	    	if (img == null){//图像数据为空  
	    		_result.put("code", ReturnCode.IMG_FAIL);
				_result.put("msg", "上传图片失败！");	
				logger.error("错误:图像数据为空,上传图片失败！");
			}else{ 
				long stage=System.currentTimeMillis();
				BASE64Decoder decoder = new BASE64Decoder();  
				//Base64解码  
	            byte[] b = decoder.decodeBuffer(img);  
	            for(int i=0;i<b.length;++i){  
	                if(b[i]<0){//调整异常数据
	                    b[i]+=256;  
	                }  
	            } 
	            //生成png图片   
	            String imgName=get32UUID()+".png"; 
	            String imgFilePath =clientBasePath+imgName; //新生成的图片 
	            OutputStream out = new FileOutputStream(imgFilePath);      
	            out.write(b);  
	            out.flush();  
	            out.close();
	            ImageCompressUtil.compressFile(imgFilePath);  
	            _result.put("result",imgName);
	            _result.put("code", ReturnCode.SUCCESS);
				_result.put("msg", "上传图片成功！");
				logger.info(_result.getString("msg"));
			 } 
		}catch(Exception e){
			e.printStackTrace();
			logger.error("图片上传错误：",e);
			_result.put("code", ReturnCode.SERVICE_FAIL);
			_result.put("code", ReturnCode.SERVICE_FAIL);
			_result.put("msg", "服务器繁忙，请稍后操作");
			return _result;
		} 
		long endTime=System.currentTimeMillis();
		long time=endTime-startTime;
		logger.info("imgBase64_2消耗时间为："+time+"毫秒，共计："+time/1000+"秒！！！！！");   
        return _result;
	} 
	 
	 @RequestMapping(value={"/audioBase64"}) 
	 @ResponseBody
	 public PageData audioBase64() throws Exception{ 
		 long startTime=System.currentTimeMillis();
		PageData _result=new PageData();
		PageData pa=getPageData();
		try{
			String img=pa.getString("img");  
	    	if (img == null){//图像数据为空  
	    		_result.put("code", ReturnCode.IMG_FAIL);
				_result.put("msg", "上传图片失败！");	
				logger.error("错误:图像数据为空,上传图片失败！");
			}else{
				long stage=System.currentTimeMillis();
				BASE64Decoder decoder = new BASE64Decoder();  
				//Base64解码  
	            byte[] b = decoder.decodeBuffer(img);  
	            for(int i=0;i<b.length;++i){  
	                if(b[i]<0){//调整异常数据
	                    b[i]+=256;  
	                }  
	            }  
	            //生成png图片   
	            String imgName=get32UUID()+".amr"; 
	            String imgFilePath =clientBasePath+imgName; //新生成的图片 
	            OutputStream out = new FileOutputStream(imgFilePath);      
	            out.write(b);  
	            out.flush();  
	            out.close();
	            ImageCompressUtil.compressFile(imgFilePath);  
	            _result.put("result",imgName);
	            _result.put("code", ReturnCode.SUCCESS);
				_result.put("msg", "上传图片成功！");
				logger.info(_result.getString("msg"));
			 } 
		}catch(Exception e){
			e.printStackTrace();
			logger.error("图片上传错误：",e);
			_result.put("code", ReturnCode.SERVICE_FAIL);
			_result.put("code", ReturnCode.SERVICE_FAIL);
			_result.put("msg", "服务器繁忙，请稍后操作");
			return _result;
		} 
		long endTime=System.currentTimeMillis();
		long time=endTime-startTime;
		logger.info("audioBase64消耗时间为："+time+"毫秒，共计："+time/1000+"秒！！！！！");   
        return _result; 
	}
}
