package com.fh.controller.UPDATE;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fh.util.aliyun.MongoUtil;

public class getCheck {
	
	/**
	 * 会员管理数据
	 * @return
	 */
//	public static List getSUserList(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> users = test.getCollection("users");
//        //查询数据
//        MongoCursor<Document> cursor = users.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号  ID
//             * 用户昵称  NICK_NAME
//             * 手机     PHONE
//             * 个人描述    DESCRIPTION
//             * 工龄      WORKING_YEARS
//             * 学校名称  SCHOOL_NAME
//             * 总工时    WORKING_HOURS
//             * 时薪     HOURLY_WAGE
//             * 真实姓名     REAL_NAME
//             * 性别     SEX
//             */
//            //储存数据
//            map.put("ID",dec.get("_id"));
//            map.put("NICK_NAME",dec.get("user_name"));
//            map.put("PHONE",dec.get("phone"));
//            map.put("DESCRIPTION",dec.get("description"));
//            map.put("WORKING_YEARS",dec.get("working_years"));
//            map.put("SCHOOL_NAME",dec.get("school_name"));
//            map.put("WORKING_HOURS",dec.get("working_hours"));
//            map.put("HOURLY_WAGE",dec.get("hourly_wage"));
//            map.put("REAL_NAME",dec.get("real_name"));
//            map.put("SEX",dec.get("gender"));
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//	
//	/**
//     * 人才管理数据
//     * @return
//     */
//    public static List getcapable(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("resumes");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号     ID
//             *用户姓名  name
//             *性别      sex
//             *出生日期  birthday
//             *毕业院校  graduation_school
//             *设计类型  sjname
//             *最高学历  maximun_educational_level
//             *工作年限  working_year
//             *邮箱        e_mail
//             *城市        city
//             *自我描述      introduction
//             *期望职位      work_hope
//             *期望月薪      price_hope
//             *期望城市      city_hope
//             *补充说明      remark
//             */
//            //储存数据
//            map.put("ID",dec.get("id"));
//            map.put("name",dec.get("real_name"));
//            map.put("sex",dec.get("gender"));
//            map.put("birthday",dec.get("birthday"));
//            map.put("graduation_school",dec.get("school_name"));
//            map.put("sjname",dec.get("type"));
//            map.put("maximun_educational_level",dec.get("education"));
//            //工作年限暂定
//            map.put("working_year","2");
//            map.put("e_mail",dec.get("email"));
//            map.put("city",dec.get("city"));
//            map.put("introduction",dec.get("description"));
//            map.put("work_hope",dec.get("expected_positions"));
//            map.put("price_hope",dec.get("expected_salary"));
//            map.put("city_hope",dec.get("expected_city"));
//            map.put("remark",dec.get("expected_description"));
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//    
//    
//    /**
//     * 获取认证管理数据
//     * @return
//     */
//    public  static List getStatus(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("auditRecords");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             *编号            ID
//             *用户姓名         NICK_NAME
//             *审核类型          AUDIT_TYPE
//             *描述              AUDIT_DESCRIBE
//             *审核状态          AUDIT_STATE
//             */
//            //储存数据
//            map.put("ID",dec.get("_id"));
//            //待定
//            map.put("NICK_NAME",dec.get("user_id"));
//            map.put("AUDIT_TYPE",dec.get("audit_type"));
//            map.put("AUDIT_DESCRIBE",dec.get("audit_describe"));
//            map.put("AUDIT_STATE",dec.get("audit_state"));
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//	
//	
//    /**
//     * 获取雇主管理数据
//     * @return
//     */
//    public static List getEmployer(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("orderList");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号                   ID
//             *标题                    title
//             *类型编号                  sort_id
//             *地区                    area
//             *描述内容                  detail
//             *预算                    budget
//             *抢单截止时间            end_time
//             *设计单位                  design_unit
//             *设计面积                  design_area
//             *个数                    number
//             *设计深度                  design_depth
//             *工时                    working_hours
//             *订单状态                  state
//             */
//            //储存数据
//            map.put("ID",dec.get(dec.get("id")));
//            map.put("title",dec.get("project_title"));
//            map.put("sort_id",dec.get("project_type"));
//            map.put("area",dec.get("project_region"));
//            map.put("detail",dec.get("project_describe"));
//            map.put("budget",dec.get("project_budget"));
//            map.put("end_time",dec.get("project_deadLine"));
//            map.put("design_unit",dec.get("project_unit"));
//            map.put("design_area",dec.get("project_area"));
//            //个数暂定
//            map.put("number","1");
//            map.put("design_depth",dec.get("project_depth"));
//            map.put("working_hours",dec.get("project_workHours"));
//            map.put("state",dec.get("invited_state"));
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//    
//    /**
//     * 获取活动管理数据
//     * @return
//     */
//    public static List getActivitys(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("activitys");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号             ID
//             *活动名称          ACT_NAME
//             *活动类型          TYPE_NAME
//             *活动内容          ACT_CONTENT
//             *活动地点          ACT_ADDR
//             *发布人            CREATE_NAME
//             *是否免费          I_FREE
//             *活动结束时间        START_DATE
//             *活动开始时间        END_DATE
//             *状态                STATUS_NAME
//             *查看图片
//             *查看报名人             PART_COUNT
//             *创建时间              CREATE_DATE
//             *修改时间                  MODIFY_DATE
//             */
//            //储存数据
//            map.put("ID",dec.get("_id"));
//            map.put("ACT_NAME",dec.get("title"));
//            //活动类型
//            map.put("TYPE_NAME",dec.get("description"));
//            //活动内容
//            map.put("ACT_CONTENT",dec.get("title"));
//            //活动地址
//            map.put("ACT_ADDR","上海建联");
//            //创建人姓名
//            map.put("CREATE_NAME","admin");
//            //是否免费
//            map.put("I_FREE","0");
//
//            map.put("START_DATE",dec.get("start_date"));
//            map.put("END_DATE",dec.get("end_date"));
//            map.put("STATUS_NAME",dec.get("participants"));
//            /*map.put("PART_COUNT",dec.get("_id"));*/
//            //创建时间
//            map.put("CREATE_DATE","");
//            //修改时间
//            map.put("MODIFY_DATE","");
//
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//    
//    /**
//     * 获取校友会的数据
//     * @return
//     */
//    public static List getalumniAssociations(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("alumniAssociations");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号           ID
//             * 学校名称         school_name
//             * 地区               area
//             * 学校简介             school_detail
//             * 审核状态             check_status
//             * 管理员编号            admin_id
//             * 加入总人数            add_num
//             * 学校图标                 school_logo
//             */
//            //储存数据
//            map.put("ID",dec.get("id"));
//            map.put("school_name",dec.get("school_name"));
//            map.put("area",dec.get("school_address"));
//            map.put("school_detail",dec.get("school_description"));
//            map.put("check_status",dec.get("status"));
//            map.put("admin_id","");
//            map.put("add_num","");
//            map.put("school_logo",dec.get("school_logo"));
//
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//    
//    /**
//     * 获取喵喵圈的数据
//     */
//    public static List getmeowCircle(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("meowCircle");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号           ID
//             *学堂标题          school_title
//             *内容            detail
//             *发布人           issuer
//             *发布时间          issu_time
//             *点赞数           like_num
//             */
//            //储存数据
//            map.put("ID",dec.get("id"));
//            map.put("school_title",dec.get("title"));
//            map.put("detail",dec.get("title"));
//            //map.put("issuer","");
//            map.put("issu_time",dec.get("create_date"));
//            map.put("like_num",dec.get("like"));
//
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//    
//    
//    /**
//     * 获取系统通知数据
//     */
//    public static List getsystemNotices(){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection("systemNotices");
//        //查询数据
//        MongoCursor<Document> cursor = resumes.find().iterator();
//        //创建插入集合
//        HashMap map = null;
//        //遍历数据
//        for (MongoCursor<Document> it = cursor; it.hasNext(); ) {
//            // 清空集合
//            map = new HashMap();
//
//            //遍历每一个Document
//            Document dec = it.next();
//            /**
//             * 编号       ID
//             *通知内容      CONTENT
//             *通知对象      USER_ID
//             *是否已读      IS_READ
//             *描述            DESCRIPTION
//             *操作人           OBJECT_ID
//             */
//            //储存数据
//            map.put("ID",dec.get("id"));
//            map.put("CONTENT",dec.get("title"));
//            map.put("USER_ID",dec.get("object_id"));
//            map.put("IS_READ",dec.get("state"));
//            map.put("DESCRIPTION",dec.get("content"));
//            map.put("OBJECT_ID",dec.get("user_id"));
//
//            //插入集合
//            list.add(map);
//        }
//        return list;
//    }
//	
//    
//    /**
//     * 根据ID和表名删除数据
//     */
//    public static boolean mongolDel(String id,String tablename){
//        //设置返回的集合
//        ArrayList<Object> list = new ArrayList<Object>();
//        //获取操作数据库的客户端
//        MongoClient mongoDBClient = MongoUtil.createMongoDBClient();
//        //设置数据库名字
//        MongoDatabase test = mongoDBClient.getDatabase("yemao");
//        //设置查询的表
//        MongoCollection<Document> resumes = test.getCollection(tablename);
//
//        Bson filter = Filters.eq("_id", id);
//        
//        resumes.deleteOne(filter);
//
//        return true;
//    }
	
}
