package com.fh.entity.system;

import java.sql.Timestamp;
import java.util.Date;

public class Ym_user {

	private String ID;
	private String REAL_NAME;
	private String NICK_NAME;
	private String PASSWORD;
	private String PHONE;
	private String SEX;
	private String BIRTHDAY;
	private String IMG;
	private String WORKING_YEARS;
	private String COMPOSITE_SCORE;
	private String AUTHENTICATING_STATE;
	private String USER_TYPE;
	private String HOURLY_WAGE;
	private String DESCRIPTION;
	private String ID_NUMBER;
	private String SCHOOL_NAME;
	private String CERTIFICATE_NAME;
	private String CITY;
	private String EMAIL;
	private String ORDERS_NUMBER;
	private double WORKING_HOURS;
	private double ACCOUNT_BALANCE;
	private double CUMULATIVE_INCOME;
	private String WY_TOKEN;
	private String CREATE_DATE;
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getREAL_NAME() {
		return REAL_NAME;
	}
	public void setREAL_NAME(String rEAL_NAME) {
		REAL_NAME = rEAL_NAME;
	}
	public String getNICK_NAME() {
		return NICK_NAME;
	}
	public void setNICK_NAME(String nICK_NAME) {
		NICK_NAME = nICK_NAME;
	}
	public String getPASSWORD() {
		return PASSWORD;
	}
	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}
	public String getPHONE() {
		return PHONE;
	}
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	} 
	public String getBIRTHDAY() {
		return BIRTHDAY;
	}
	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}
	public String getIMG() {
		return IMG;
	}
	public void setIMG(String iMG) {
		IMG = iMG;
	}
	public String getWORKING_YEARS() {
		return WORKING_YEARS;
	}
	public void setWORKING_YEARS(String wORKING_YEARS) {
		WORKING_YEARS = wORKING_YEARS;
	}
	public String getCOMPOSITE_SCORE() {
		return COMPOSITE_SCORE;
	}
	public void setCOMPOSITE_SCORE(String cOMPOSITE_SCORE) {
		COMPOSITE_SCORE = cOMPOSITE_SCORE;
	}
	public String getAUTHENTICATING_STATE() {
		return AUTHENTICATING_STATE;
	}
	public void setAUTHENTICATING_STATE(String aUTHENTICATING_STATE) {
		AUTHENTICATING_STATE = aUTHENTICATING_STATE;
	}
	public String getUSER_TYPE() {
		return USER_TYPE;
	}
	public void setUSER_TYPE(String uSER_TYPE) {
		USER_TYPE = uSER_TYPE;
	}
	public String getHOURLY_WAGE() {
		return HOURLY_WAGE;
	}
	public void setHOURLY_WAGE(String hOURLY_WAGE) {
		HOURLY_WAGE = hOURLY_WAGE;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getID_NUMBER() {
		return ID_NUMBER;
	}
	public void setID_NUMBER(String iD_NUMBER) {
		ID_NUMBER = iD_NUMBER;
	}
	public String getSCHOOL_NAME() {
		return SCHOOL_NAME;
	}
	public void setSCHOOL_NAME(String sCHOOL_NAME) {
		SCHOOL_NAME = sCHOOL_NAME;
	}
	public String getCERTIFICATE_NAME() {
		return CERTIFICATE_NAME;
	}
	public void setCERTIFICATE_NAME(String cERTIFICATE_NAME) {
		CERTIFICATE_NAME = cERTIFICATE_NAME;
	}
	public String getCITY() {
		return CITY;
	}
	public void setCITY(String cITY) {
		CITY = cITY;
	}
	public String getEMAIL() {
		return EMAIL;
	}
	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String sEX) {
		SEX = sEX;
	}
	public double getWORKING_HOURS() {
		return WORKING_HOURS;
	}
	public String getORDERS_NUMBER() {
		return ORDERS_NUMBER;
	}
	public void setORDERS_NUMBER(String oRDERS_NUMBER) {
		ORDERS_NUMBER = oRDERS_NUMBER;
	}
	public void setWORKING_HOURS(double wORKING_HOURS) {
		WORKING_HOURS = wORKING_HOURS;
	}
	public double getACCOUNT_BALANCE() {
		return ACCOUNT_BALANCE;
	}
	public void setACCOUNT_BALANCE(double aCCOUNT_BALANCE) {
		ACCOUNT_BALANCE = aCCOUNT_BALANCE;
	}
	public double getCUMULATIVE_INCOME() {
		return CUMULATIVE_INCOME;
	}
	public void setCUMULATIVE_INCOME(double cUMULATIVE_INCOME) {
		CUMULATIVE_INCOME = cUMULATIVE_INCOME;
	}
	public String getWY_TOKEN() {
		return WY_TOKEN;
	}
	public void setWY_TOKEN(String wY_TOKEN) {
		WY_TOKEN = wY_TOKEN;
	}
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	
	
	

}
