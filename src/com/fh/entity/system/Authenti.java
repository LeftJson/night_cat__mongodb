package com.fh.entity.system;

public class Authenti {

	private String ID;
	private String USER_ID;
	private String AUDIT_OBJECT_ID;
	private String AUDIT_TYPE;
	private String AUDIT_STATE;
	private String AUDIT_FAILURE_REASON;
	private String AUDITOR_ID;
	private String AUDIT_DATE;
	private String CREATE_DATE;
	private String AUDIT_FINAL_STATE;
	private String AUDIT_DESCRIBE;
	private String ISDEL;
	public String getISDEL() {
		return ISDEL;
	}
	public void setISDEL(String iSDEL) {
		ISDEL = iSDEL;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getAUDIT_OBJECT_ID() {
		return AUDIT_OBJECT_ID;
	}
	public void setAUDIT_OBJECT_ID(String aUDIT_OBJECT_ID) {
		AUDIT_OBJECT_ID = aUDIT_OBJECT_ID;
	}
	public String getAUDIT_FAILURE_REASON() {
		return AUDIT_FAILURE_REASON;
	}
	public void setAUDIT_FAILURE_REASON(String aUDIT_FAILURE_REASON) {
		AUDIT_FAILURE_REASON = aUDIT_FAILURE_REASON;
	}
	public String getAUDITOR_ID() {
		return AUDITOR_ID;
	}
	public void setAUDITOR_ID(String aUDITOR_ID) {
		AUDITOR_ID = aUDITOR_ID;
	}
	public String getAUDIT_DATE() {
		return AUDIT_DATE;
	}
	public void setAUDIT_DATE(String aUDIT_DATE) {
		AUDIT_DATE = aUDIT_DATE;
	}
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public String getAUDIT_DESCRIBE() {
		return AUDIT_DESCRIBE;
	}
	public void setAUDIT_DESCRIBE(String aUDIT_DESCRIBE) {
		AUDIT_DESCRIBE = aUDIT_DESCRIBE;
	}
	public String getAUDIT_TYPE() {
		return AUDIT_TYPE;
	}
	public void setAUDIT_TYPE(String aUDIT_TYPE) {
		AUDIT_TYPE = aUDIT_TYPE;
	}
	public String getAUDIT_STATE() {
		return AUDIT_STATE;
	}
	public void setAUDIT_STATE(String aUDIT_STATE) {
		AUDIT_STATE = aUDIT_STATE;
	}
	public String getAUDIT_FINAL_STATE() {
		return AUDIT_FINAL_STATE;
	}
	public void setAUDIT_FINAL_STATE(String aUDIT_FINAL_STATE) {
		AUDIT_FINAL_STATE = aUDIT_FINAL_STATE;
	}
	
}
