package com.fh.service.system.customer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("customerServerService")
public class CustomerServerService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("CustomerServerMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("CustomerServerMapper.findCount", page);
  }
  
  //添加
  public Object adduser(PageData pd) throws Exception{
    return this.dao.save("CustomerServerMapper.save", pd);
  }
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("CustomerServerMapper.delById", pd);
  }
  
  //修改
  public Object edit(PageData pd)throws Exception{
    return this.dao.update("CustomerServerMapper.edit", pd);
  }
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("CustomerServerMapper.queryById", pd);
  }

}
