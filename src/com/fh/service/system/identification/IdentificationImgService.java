package com.fh.service.system.identification;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.util.PageData;

@Service("identificationImgService")
public class IdentificationImgService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;   
  
  //查询列表信息
  public List<PageData> queryByPid(PageData pd)throws Exception{
    return (List<PageData>)dao.findForList("IdentificationImgMapper.queryByPid", pd);
  } 
  
  public PageData queryMaxOrderby(PageData pd)throws Exception{
    return (PageData)dao.findForObject("IdentificationImgMapper.queryMaxOrderby", pd);
  } 

}
