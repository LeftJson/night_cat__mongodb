package com.fh.service.system.user;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.entity.system.Ym_user;
import com.fh.util.PageData;
import com.fh.util.UuidUtil;

@Service("ymuserService")
public class YmUserService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;
	//查询
	public List<PageData> listByParam(Page page)  throws Exception {
	       return (List<PageData>)this.dao.findForList("YmUserMapper.listPageByParam", page);
	}
	public PageData findCount(Page page)  throws Exception {
	       return (PageData)this.dao.findForObject("YmUserMapper.findCount", page);
	}
	
	
	//添加
	public Object adduser(PageData pd) throws Exception{
		return this.dao.save("YmUserMapper.save", pd);
	}
	
	
	//删除
	public Object delById(PageData pd)throws Exception{
		return this.dao.update("YmUserMapper.delById", pd);
	}
	
	//修改
	public Object edit(PageData pd)throws Exception{
		return this.dao.update("YmUserMapper.edit", pd);
	}
	
	
	//通过编号获取数据
	public PageData queryById(PageData pd) throws Exception {
		return  (PageData) this.dao.findForObject("YmUserMapper.queryById", pd);
	}
	
	//批量同步数据
	public Object addsavelist(List<Ym_user> emps) throws Exception{
		return this.dao.save("YmUserMapper.addsavelist",emps);
	}
	
	
	//删除整张表的数据
	public void tbdelete(PageData pd) throws Exception {
		this.dao.delete("YmUserMapper.tbdelete", pd);
	}
		
}
