package com.fh.service.system.employer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("orderJoinPersonService")
public class OrderJoinPersonService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("EmployerOrderJoinPersonMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("EmployerOrderJoinPersonMapper.findCount", page);
  }
  
  
  //添加
  public Object adduser(PageData pd) throws Exception{
    return this.dao.save("EmployerOrderJoinPersonMapper.save", pd);
  }
  
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("EmployerOrderJoinPersonMapper.delById", pd);
  }
  
  //修改
  public Object edit(PageData pd)throws Exception{
    return this.dao.update("EmployerOrderJoinPersonMapper.edit", pd);
  }
  
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("EmployerOrderJoinPersonMapper.queryById", pd);
  }

}
