package com.fh.service.system.employer;

import org.springframework.stereotype.Service;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;
import com.fh.util.UuidUtil;

@Service("employerService")
public class EmployerService{
  
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("EmployerMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("EmployerMapper.findCount", page);
  }
  
  

  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("EmployerMapper.delById", pd);
  }
  

  
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("EmployerMapper.queryById", pd);
  }
  
//查询Excel导出数据
  public List<PageData> doexlelist(PageData pd) throws Exception{
       return (List<PageData>)this.dao.findForList("EmployerMapper.doexlelist", pd);
  }

}
