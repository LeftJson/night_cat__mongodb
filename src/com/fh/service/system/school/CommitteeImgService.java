package com.fh.service.system.school;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.util.PageData;

@Service("committeeImgService")
public class CommitteeImgService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;   
  
  //查询列表信息
  public List<PageData> queryByPid(PageData pd)throws Exception{
    return (List<PageData>)dao.findForList("CaseImgMapper.queryByPid", pd);
  } 
  
  public PageData queryMaxOrderby(PageData pd)throws Exception{
    return (PageData)dao.findForObject("CaseImgMapper.queryMaxOrderby", pd);
  } 

}
