package com.fh.service.system.school;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("schoolCommitteeService")
public class SchoolCommitteeService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("SchoolCommitteeMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("SchoolCommitteeMapper.findCount", page);
  }
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("SchoolCommitteeMapper.delById", pd);
  }
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("SchoolCommitteeMapper.queryById", pd);
  }
  
  //提现状态更改
  public Object edit(PageData pd) throws Exception {
    return this.dao.update("SchoolCommitteeMapper.edit", pd);
  } 
  
  /*
   * 根据编号查找详情
   */
  public PageData findById(PageData pd) throws Exception {
    return (PageData) this.dao.findForObject("SchoolCommitteeMapper.querybyid", pd);
  }

}
