package com.fh.service.system.Yx_Roel;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.util.PageData;

@Service("yxroel")
public class Yx_RoelService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;
	
	//通过编号获取数据
	public PageData queryById(PageData pd) throws Exception {
		return  (PageData) this.dao.findForObject("YmRoelMapper.queryById", pd);
	}
}
