package com.fh.service.system.designer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.util.PageData;

@Service("worksImgService")
public class WorksImgService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;   
  
  //查询列表信息
  public List<PageData> queryByPid(PageData pd)throws Exception{
    return (List<PageData>)dao.findForList("WorksImgMapper.queryByPid", pd);
  } 
  
  public PageData queryMaxOrderby(PageData pd)throws Exception{
    return (PageData)dao.findForObject("WorksImgMapper.queryMaxOrderby", pd);
  } 

}
