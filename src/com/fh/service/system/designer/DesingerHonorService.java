package com.fh.service.system.designer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("designerhonorService")
public class DesingerHonorService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("DesignerHonorMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("DesignerHonorMapper.findCount", page);
  }
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("DesignerHonorMapper.delById", pd);
  }
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("DesignerHonorMapper.queryById", pd);
  }
  
 //查询Excel导出数据
  public List<PageData> doexlelist(PageData pd) throws Exception{
       return (List<PageData>)this.dao.findForList("DesignerHonorMapper.doexlelist", pd);
  }
}
