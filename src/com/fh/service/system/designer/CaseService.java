package com.fh.service.system.designer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("caseService")
public class CaseService{
  
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("CaseMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("CaseMapper.findCount", page);
  }
  
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("CaseMapper.delById", pd);
  }
  
  //修改
  public Object edit(PageData pd)throws Exception{
    return this.dao.update("CaseMapper.edit", pd);
  }
  
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("CaseMapper.queryById", pd);
  }
  
  /*
   * 根据编号查找详情
   */
  public PageData findById(PageData pd) throws Exception {
    return (PageData) this.dao.findForObject("CaseMapper.querybyid", pd);
  }
  
//查询Excel导出数据
  public List<PageData> doexlelist(PageData pd) throws Exception{
       return (List<PageData>)this.dao.findForList("CaseMapper.doexlelist", pd);
  }

}
