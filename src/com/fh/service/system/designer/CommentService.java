package com.fh.service.system.designer;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("commentService")
public class CommentService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("CommentMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("CommentMapper.findCount", page);
  }
  
  
  //添加
  public Object adduser(PageData pd) throws Exception{
    return this.dao.save("CommentMapper.save", pd);
  }
  
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("CommentMapper.delById", pd);
  }
  
  //修改
  public Object edit(PageData pd)throws Exception{
    return this.dao.update("CommentMapper.edit", pd);
  }
  
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("CommentMapper.queryById", pd);
  }

}
