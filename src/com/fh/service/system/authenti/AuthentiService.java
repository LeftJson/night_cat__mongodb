package com.fh.service.system.authenti;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.entity.system.Authenti;
import com.fh.entity.system.Ym_user;
import com.fh.util.PageData;

@Service("authentiService")
public class AuthentiService {

	@Resource(name = "daoSupport")
	private DaoSupport dao;
	//查询
	public List<PageData> listByParam(Page page)  throws Exception {
	       return (List<PageData>)this.dao.findForList("AuthentiMapper.listPageByParam", page);
	}
	public PageData findCount(Page page)  throws Exception {
	       return (PageData)this.dao.findForObject("AuthentiMapper.findCount", page);
	}
	
	
	//添加
	public Object adduser(PageData pd) throws Exception{
		return this.dao.save("AuthentiMapper.save", pd);
	}
	
	
	//删除
	public Object delById(PageData pd)throws Exception{
		return this.dao.update("AuthentiMapper.delById", pd);
	}
	
	//修改
	public Object edit(PageData pd)throws Exception{
		return this.dao.update("AuthentiMapper.edit", pd);
	}
	
	
	//通过编号获取数据
	public PageData queryById(PageData pd) throws Exception {
		return  (PageData) this.dao.findForObject("AuthentiMapper.queryById", pd);
	}
	
	//批量同步数据
	public Object addsavelists(List<Authenti> emps) throws Exception{
		return this.dao.save("AuthentiMapper.addsavelist",emps);
	}
	
	//审核
	public Object upexamine(PageData pd)throws Exception{
		return this.dao.update("AuthentiMapper.upexamine", pd);
	}
	
	//删除整张表的数据
	public void tbdelete(PageData pd) throws Exception {
		this.dao.delete("AuthentiMapper.tbdelete", pd);
	} 
}
