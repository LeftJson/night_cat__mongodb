package com.fh.service.system.pay;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("payService")
public class PayService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("PayMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("PayMapper.findCount", page);
  }
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("PayMapper.delById", pd);
  }
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("PayMapper.queryById", pd);
  }

}
