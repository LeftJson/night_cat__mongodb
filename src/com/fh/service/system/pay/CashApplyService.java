package com.fh.service.system.pay;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.fh.dao.DaoSupport;
import com.fh.entity.Page;
import com.fh.util.PageData;

@Service("cashApplyService")
public class CashApplyService{
  @Resource(name = "daoSupport")
  private DaoSupport dao;
  //查询
  public List<PageData> listByParam(Page page)  throws Exception {
         return (List<PageData>)this.dao.findForList("CashApplyMapper.listPageByParam", page);
  }
  public PageData findCount(Page page)  throws Exception {
         return (PageData)this.dao.findForObject("CashApplyMapper.findCount", page);
  }
  
  //修改
  public Object editBack(PageData pd)throws Exception{
    return this.dao.update("CashApplyMapper.editBack", pd);
  }
  
  
  //删除
  public Object delById(PageData pd)throws Exception{
    return this.dao.update("CashApplyMapper.delById", pd);
  }
  
  //通过编号获取数据
  public PageData queryById(PageData pd) throws Exception {
    return  (PageData) this.dao.findForObject("CashApplyMapper.queryById", pd);
  }
  
  //提现状态更改
  public Object edit(PageData pd) throws Exception {
    return this.dao.update("CashApplyMapper.edit", pd);
  } 

}
