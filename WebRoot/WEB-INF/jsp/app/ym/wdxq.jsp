<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>建联夜猫</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	  <link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/public.css" />
	  <link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/meow/wzxq.css" />
	  <style>
		  .pinlunlist {
			  background: white;
		  }

		  .header {
			  position: static;
		  }

		  .content {
			  margin-top: 0;
			  background: white;
		  }

		  .pinglun {
			  padding-bottom: 0;
			  background: white;
		  }

		  .confirmColor {
			  color: #f65aa6;
			  background: #ffffff !important;
		  }

		  .comment-reply {
			  font-size: 0.28rem;
		  }

		  .tupian {
			  background-size: cover!important;
			  background-position: center center!important;
			  background-repeat: no-repeat!important;
			  width: 7.2rem;
			  height: 2.6rem;
			  background: #CCCCCC;
			  overflow: hidden;
			  margin: 0 auto 0.2rem;
			  display:none;
		  }
		  .btn{
			  height: 1rem;
			  line-height:1rem;
			  width: 100%;
			  color:#fff;
			  background:#fd70bc;
			  font-size:0.28rem;
			  text-align:center;
			  margin-bottom: 0.25rem;
		  }
	  </style>
	  <script>
          var fs = 100
          var w = 750

          function dorem() {
              var sw = window.outerWidth ? window.outerWidth : screen.width
              document.documentElement.style.fontSize = sw / w * fs + 'px'
          }
          dorem()
          window.onresize = dorem
	  </script>
  </head>
  
  <body>
  <div class="templete-body">
      <div id="btn" class="btn">打开建联夜猫APP</div>
	  <!--文章详情-->
	  <div class="content">
		  <div class="piaoti">
			  ${pd.title}
		  </div>
		  <div class="nichengshijian">
			  <!--<div class="ns-left">
				  <span><img id="uimg"/></span><span>${pd.user.user_name}</span>
			  </div>-->
			  <div class="tm-right">
				  <span>发布时间</span> <span id="fbdate"></span>
			  </div>
		  </div>
		  <div class="tupian" id="wdimg">
		  </div>
		  <div class="neirongshijian">
			  ${pd.description}
		  </div>
	  </div>

	  <!--评论详情-->
      <div class="pinglun">
		  <p id="hotComment">热门评论</p>
	  </div>
  </div>
  <a id="abc" style="display:none;" href="jianlianyemao://abc">立即打开<a>
  <input type="hidden" id="bkimg" value="${pd.imgs}">
  <input type="hidden" id="uzimg" value="${pd.user.img}">
  <input type="hidden" id="credate" value="${pd.create_date}">
  </body>
  <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
        var root = 'http://101.132.96.90:8080/upload/img/';
		var browser = {
			versions: function () {
				var u = navigator.userAgent, app = navigator.appVersion;
				return {         //移动终端浏览器版本信息
					trident: u.indexOf('Trident') > -1, //IE内核
					presto: u.indexOf('Presto') > -1, //opera内核
					webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
					gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
					mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
					ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
					android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
					iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
					iPad: u.indexOf('iPad') > -1, //是否iPad
					webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
				};
			}(),
			language: (navigator.browserLanguage || navigator.language).toLowerCase()
		}
        // 原图
        function getRealImgPath (path){
            if( path.indexOf("http") < 0 ) {
                path = root + path;
            }
            return path;
        }
        //3.2、时间处理-时间轴=>年月日格式
        function timeStamp2String (time) {
            //y=年;ymd=年月日;ymdhm年月日时分;ymdhms年月日时分秒
            var datetime = new Date();
            datetime.setTime(time);
            var year = datetime.getFullYear();
            var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
            var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
            var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
            var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
            return year + "-" + month + "-" + date + " " + hour + ":" + minute;
        }
		
		function getCommentsHtml(comments){
			var html = '';
			$.each(comments,function(index,comment){
				if( !comment.user ){
					comment.user = {};
				}
				html += '<div class="pinlunlist">'
					  + '<div class="top-pinlun">'
						  + '<div class="tp-left2">'
							+ '<div class="img" style="background-image: url('+getRealImgPath(comment.user.img)+')"></div>'
							+ '<p class="name">'+comment.user.user_name+'</p>'
						  + '</div>'
						  + '<div class="tp-right">'
							  + '<span>'+comment.like+'</span>'
							  + '<span>'
								  + '<img src="<%=basePath%>/H-ui/static/images/zan.png"/>'
							  + '</span>'
						  + '</div>'
					  + '</div>'
					  + '<div class="neirong">'+comment.content+'</div>'
				  + '</div>';
			})
			return html;
		}
		
		function getComments() {
			var id = '${pd._id}';
			if( id=='' || id==null ){
				console.log("获取文章ID失败！");
				return;
			}
			var params = {interfaceId: "getComments", pageNo: 0, pageSize: 10, where: {comment_id: id}};
			var data = {params:params};
			$.ajax( {
				type : "POST",
				dataType:"json",   //返回格式为json
				url : "http://47.100.34.193:3000/mongoApi",
				data: {params:JSON.stringify(params)}, 
				success : function(result) {
					if( result ){
						var comments = result.data.comments;
						var html = getCommentsHtml(comments);
						$("#hotComment").after(html);
					}else{
						$("#hotComment").after('');
					}	
				},
				error:function(err){
					// alert("err:"+JSON.stringify(err));
					console.log("查询评论失败！");
				}
			});	
	    }

        setTimeout(function(){
            var arrStr = $("#bkimg").val();
            var arr = arrStr.split(",");
            if(arr.length >0 ){
                var mk=arr[0];
                mk = mk.replace(/[\[\]]/g,'');
				$("#wdimg").css("display","none");
                $("#wdimg").css("background","url("+getRealImgPath(mk)+")");
			}

            var uzimg = $("#uzimg").val();
            $("#uimg").attr("src",getRealImgPath(uzimg));

            var credate = $("#credate").val();
            $("#fbdate").html(timeStamp2String(credate));
			
			// 查询评论列表
			getComments();
        },200)
		
		$("#btn").click(function(){
		    if (browser.versions.mobile) {//判断是否是移动设备打开。browser代码在下面
				var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
				if (ua.match(/MicroMessenger/i) == "micromessenger") { // 微信
					alert("请使用其他浏览器打开:");
				}else{
				    console.log("其他浏览器内核:");
					document.getElementById("abc").click();
				}
		    }else{
				alert("请使用移动设备打开:");
			}	
		})
		
		
	</script>

</html>




