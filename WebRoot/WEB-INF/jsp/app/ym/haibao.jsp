<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>建联夜猫</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
  	<link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/public.css" />
  	<link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/meow/haibao.css" />
	<style>
	  .btn{
		  height: 1rem;
		  line-height:1rem;
		  width: 100%;
		  color:#fff;
		  background:#fd70bc;
		  font-size:0.28rem;
		  text-align:center;
		  margin-bottom: 0.25rem;
	  }
	</style>
	  <script>
          var fs = 100
          var w = 750

          function dorem() {
              var sw = window.outerWidth ? window.outerWidth : screen.width
              document.documentElement.style.fontSize = sw / w * fs + 'px'
          }
          dorem()
          window.onresize = dorem
	  </script>
  </head>
  
  <body>
  <div>
	  <div class="content" id="content">
          <div id="btn" class="btn">打开建联夜猫APP</div>
		  <div class="bottom">
			  <p style="padding-top:20px;"><span id="num">${pd.participants}</span>人已报名</p>
		  </div>
	  </div>
	  <a id="abc" style="display:none;" href="jianlianyemao://abc">立即打开<a>
  </div>
  <input id="cover" type="hidden" value="${pd.cover}"/>
  <input id="participants" type="hidden" value="${pd.participants}"/>
  </body>
  <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    var root = 'http://101.132.96.90:8080/upload/img/';
	var browser = {
			versions: function () {
				var u = navigator.userAgent, app = navigator.appVersion;
				return {         //移动终端浏览器版本信息
					trident: u.indexOf('Trident') > -1, //IE内核
					presto: u.indexOf('Presto') > -1, //opera内核
					webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
					gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
					mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
					ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
					android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
					iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
					iPad: u.indexOf('iPad') > -1, //是否iPad
					webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
				};
			}(),
			language: (navigator.browserLanguage || navigator.language).toLowerCase()
		}
    // 原图
    function getRealImgPath (path){
        if( path.indexOf("http") < 0 ) {
            path = root + path;
        }
        return path;
    }
    setTimeout(function(){
        var cover = $("#cover").val();
        $("#content").css("background-image","url("+getRealImgPath(cover)+")");
        var arrStr = $("#participants").val();
        var arr = arrStr.split(",");
        $("#num").text(arr.length);
    },200)
	
	$("#btn").click(function(){
		if (browser.versions.mobile) {//判断是否是移动设备打开。browser代码在下面
			var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
			if (ua.match(/MicroMessenger/i) == "micromessenger") { // 微信
				alert("请使用其他浏览器打开:");
			}else{
				console.log("其他浏览器内核:");
				document.getElementById("abc").click();
			}
		}else{
			alert("请使用移动设备打开:");
		}	
	})
</script>

</html>
