<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>建联夜猫</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	  <link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/public.css" />
	  <link rel="stylesheet" href="<%=basePath%>/H-ui/static/css/meow/wzxq.css" />
	  <script>
          var fs = 100
          var w = 750

          function dorem() {
              var sw = window.outerWidth ? window.outerWidth : screen.width
              document.documentElement.style.fontSize = sw / w * fs + 'px'
          }
          dorem()
          window.onresize = dorem
	  </script>
	  <style>
		  .pinlunlist {
			  background: white;
		  }

		  .header {
			  position: static;
		  }

		  .content {
			  margin-top: 0;
			  background: white;
		  }

		  .pinglun {
			  padding-bottom: 0;
			  background: white;
		  }

		  .confirmColor {
			  color: #f65aa6;
			  background: #ffffff !important;
		  }

		  .comment-reply {
			  font-size: 0.28rem;
		  }

		  .tupian {
			  background-size: cover!important;
			  background-position: center center!important;
			  background-repeat: no-repeat!important;
			  width: 7.2rem;
			  height: 2.6rem;
			  background: #CCCCCC;
			  overflow: hidden;
			  margin: 0 auto 0.2rem;
		  }
	  </style>

  </head>
  
  <body>
  <div class="templete-body">
	  <!--文章详情-->
	  <div class="content">
		  <div class="piaoti">
			  标题
		  </div>
		  <div class="nichengshijian">
			  <div class="ns-left">
				  <span><img src=""/></span><span>张三</span>
			  </div>
			  <div class="tm-right">
				  <span>发布时间</span> <span>2018</span>
			  </div>
		  </div>
		  <div class="tupian">
		  </div>
		  <div class="neirongshijian">
			  描述
		  </div>
	  </div>
  </div>
  </body>
  <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>


</html>
