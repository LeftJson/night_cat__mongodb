<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
        <link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
		<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
		<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
		<title>新增用户信息</title>
	</head>

	<body>
	    <article class="page-container">
			<form class="form form-horizontal" id="form-user-edit">
			    <input type="hidden" name="id" id="id" value="${pd._id }"/>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">昵称：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="text" class="input-text" style="border: none;" name="user_name" id="user_name"  value="${pd.user_name }" readonly="readonly"  maxlength="32" placeholder="昵称"  data-rule-required="true"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">真实姓名：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
						<input type="text" class="input-text" style="border: none;" name="real_name" id="real_name"  value="${pd.real_name }" readonly="readonly" data-rule="IDNO" placeholder="真实姓名"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">城市：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
						<input type="text" class="input-text" style="border: none;" name="city" id="city"  value="${pd.city }" readonly="readonly"  placeholder="城市"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">邮箱：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
						<input type="text" class="input-text" style="border: none;" name="email" id="email"  value="${pd.email }"  readonly="readonly" placeholder="邮箱"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">手机号：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					  <input type="number" style="width:150px;border: none;" class="input-text" name="phone" id="phone"  value="${pd.phone }" readonly="readonly" data-rule="mobile" placeholder="手机号" title="手机号" data-rule-required="true"/>
					</div>
				</div>
				<%--<div class="row cl">--%>
					<%--<label class="form-label col-xs-4 col-sm-2">生日：</label>--%>
					<%--<div class="formControls col-xs-8 col-sm-9">--%>
						<%--<input type="text" style="width:150px;" class="input-text" name="birthday" id="birthday"  value="${pd.birthday }" readonly="readonly" data-rule="mobile" title="生日" data-rule-required="true"/>--%>
						<%--&lt;%&ndash;<input type="text" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" class="input-text Wdate" value="${pd.birthday}" readonly="readonly" id="birthday" name="birthday" data-rule-required="true">&ndash;%&gt;--%>
					<%--</div>--%>
				<%--</div>--%>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">工龄：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="email" class="input-text" style="border: none;" name="working_years" id="working_years"  value="${pd.working_years }" readonly="readonly" data-rule="email" placeholder="工龄" title="工龄"/>
					</div>
				</div> 
				<%--<div class="row cl">--%>
					<%--<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>综合评分：</label>--%>
					<%--<div class="formControls col-xs-8 col-sm-9">--%>
					   <%--<input type="text" class="input-text" name="composite_score" id="composite_score"  value="${pd.composite_score }"  readonly="readonly" placeholder="综合评分" data-rule-required="true"/>--%>
					<%--</div>--%>
				<%--</div> --%>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">时薪：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="number" style="width:150px;border: none;" class="input-text" name="hourly_wage" id="hourly_wage"  value="${pd.hourly_wage }" readonly="readonly"  placeholder="时薪" />
					</div>
				</div> 
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">个人描述：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
						<div class="input-text" style="border: none;">${pd.description}</div>
						<%--<textarea  class="textarea" name="description" style="border: none;" id="description"  readonly="readonly">${pd.description}</textarea>--%>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">身份证号：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="text" class="input-text" style="border: none;" name="id_number" id="id_number"  readonly="readonly" value="${pd.id_number }" data-rule="IDNO" placeholder="身份证号"/>
					</div>
				</div>  
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">学校名称：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="text" class="input-text" style="border: none;" name="school_name" id="school_name" readonly="readonly"  value="${pd.school_name }"  placeholder="学校名称"/>
					</div>
				</div> 
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">职称证书：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
					   <input type="text" class="input-text" style="border: none;" name="certificate_name" id="certificate_name" readonly="readonly"  value="${pd.certificate_name }"  placeholder="职称证书"/>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">签约状态：</label>
					<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
						<c:if test="${pd.authenticating_state == '0' || pd.authenticating_state == '0.0'}">
							<div class="input-text" style="border: none;">未认证</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '1' || pd.authenticating_state == '1.0'}">
							<div class="input-text" style="border: none;">实名认证中</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '2' || pd.authenticating_state == '2.0'}">
							<div class="input-text" style="border: none;">已实名认证</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '3' || pd.authenticating_state == '3.0'}">
							<div class="input-text" style="border: none;">已实名认证>>平台认证中</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '4' || pd.authenticating_state == '4.0'}">
							<div class="input-text" style="border: none;">已实名认证>>已平台认证</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '5' || pd.authenticating_state == '5.0'}">
							<div class="input-text" style="border: none;">已实名认证>>已平台认证>>签约认证中</div>
						</c:if>
						<c:if test="${pd.authenticating_state == '6' || pd.authenticating_state == '6.0'}">
							<div class="input-text" style="border: none;">已实名认证>>已平台认证>>已签约认证</div>
						</c:if>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<%--<a class="btn btn-primary radius" _href="<%=basePath%>yx_case/tocase.do?ID=${pd._id}" onclick="Hui_admin_tab(this)"  href="javascript:;" data-title="查看设计师案例">查看设计师案例</a>--%>
						<a class="btn btn-primary radius" onclick="project_edit('查看设计师案例','<%=basePath%>yx_case/tocase.do?ID=${pd._id}','','300')" data-title="查看设计师案例">查看设计师案例</a>
						<input class="btn btn-primary radius" onclick="clone_palne()" type="button" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
					</div>
				</div>
			</form>
		</article> 
        <!--_footer 作为公共模版分离出去-->
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/icheck/jquery.icheck.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
		<!--/_footer /作为公共模版分离出去-->

		<!--请在下方写此页面业务相关的脚本-->
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.config.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
		<script>
            function clone_palne(){
                    parent.$('.r .Hui-iconfont').click();
                    parent.layer.close(index);
            }

            function project_edit(title,url,w,h){
            	var index=layer.open({
            		type:2,
            		title:title,
            		content:url

            });
            	layer.full(index);
            }

		</script>
	</body> 
</html>  