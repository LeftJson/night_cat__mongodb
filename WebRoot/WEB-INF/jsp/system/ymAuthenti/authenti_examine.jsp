<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
		<title>添加会员</title>
	</head>

	<body class="pos-r"> 
	<div>
<article class="page-container">
	<form class="form form-horizontal" id="form-horizontal">  
	 <input type="hidden" name="ID" id="ID" value="${pd._id }"/>
	 <input type="hidden" value="${pd.user_id }" id="user_id" name="user_id">
	 <input type="hidden" value="${pd.audit_final_state}" id="audit_final_state" name="audit_final_state"/>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">真实姓名：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="${pds.user_name }" readonly="readonly" placeholder="" id="user_name" name="user_name" readonly="readonly">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>描述：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="${pd.audit_describe}" readonly="readonly" placeholder="" readonly="readonly" id="audit_describe" name="audit_describe" data-rule-required="true"  >
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">所需资料名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				身份证正面照，身份证反面照，手持身份证
			</div>
		</div>
		<div class="row cl">
            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>实名认证：</label>
            <div class="formControls col-xs-8 col-sm-9">
				<c:choose>
					<c:when test="${not empty pdlist}">
					<span class="select-box" style="display: -webkit-flex;">
					<c:forEach items="${pdlist}" var="cate">
						<c:if test="${pd.audit_type == '0'}">
							<c:if test="${cate.type == '0' || cate.type == '1' || cate.type == '2'}">
								<c:choose>
									<c:when test="${cate.img !=null && cate.img!='' && cate.img!=undefined}">
										<div style="width: 250px;height: 250px;padding: 5px 5px;"><img src="http://www.jianlianyemao.com/upload/img/${cate.img}" style="width: 100%;height: 100%"  class="img"/> </div>
									</c:when>
									<c:otherwise>
										<c:if test="${cate.type == '0'}">
											<div>暂无手持证件照，</div>
										</c:if>
										<c:if test="${cate.type == '1'}">
											<div>暂无证件正面照，</div>
										</c:if>
										<c:if test="${cate.type == '2'}">
											<div>暂无证件背面照，</div>
										</c:if>
										<c:if test="${cate.type == '3'}">
											<div>暂无学历证书照，</div>
										</c:if>
										<c:if test="${cate.type == '4'}">
											<div>暂无职称证书照</div>
										</c:if>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:if>
					</c:forEach>
					</span>
					</c:when>
				</c:choose>
            </div>
        </div>
		<c:if test="${ pd.audit_final_state == '4' || pd.audit_final_state == '6' }">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">所需资料名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				学历图片、职称图片
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>平台认证：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<c:choose>
					<c:when test="${not empty pdlist}">
                    <span class="select-box" style="display: -webkit-flex;">
                    <c:forEach items="${pdlist}" var="cate">
						<c:if test="${pd.audit_type == '0'}">
							<c:choose>
								<c:when test="${cate.type == '3' || cate.type == '4'}">
									<c:choose>
										<c:when test="${cate.img !=null && cate.img!='' && cate.img!=undefined}">
											<div style="width: 250px;height: 250px;padding: 5px 5px;"><img src="http://www.jianlianyemao.com/upload/img/${cate.img}" style="width: 100%;height: 100%"  class="img"/> </div>
										</c:when>
										<c:otherwise>
											<c:if test="${cate.type == '0'}">
												<div>暂无手持证件照，</div>
											</c:if>
											<c:if test="${cate.type == '1'}">
												<div>暂无证件正面照，</div>
											</c:if>
											<c:if test="${cate.type == '2'}">
												<div>暂无证件背面照，</div>
											</c:if>
											<c:if test="${cate.type == '3'}">
												<div>暂无学历证书照，</div>
											</c:if>
											<c:if test="${cate.type == '4'}">
												<div>暂无职称证书照</div>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:when>
							</c:choose>
						</c:if>
					</c:forEach>
                    </span>
					</c:when>
				</c:choose>
			</div>
		</div>
	</c:if>
		<%--<div class="row cl">--%>
			<%--<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>签约认证：</label>--%>
			<%--<div class="formControls col-xs-8 col-sm-9">--%>
                    <%--<span class="select-box" style="display: -webkit-flex;">--%>
					<%--<c:choose>--%>
						<%--<c:when test="${not empty pdlist}">--%>
							<%--<c:forEach items="${pdlist}" var="cate">--%>
								<%--<c:choose>--%>
								<%--<c:when test="${cate.img !=null || cate.img!='' || cate.img!=undefined}">--%>
								<%--<c:if test="${pd.audit_type == '0'}">--%>
									<%--<c:if test="${cate.type == '5' || cate.type == '6' }">--%>
										<%--<div style="width: 250px;height: 250px;"><img src="http://www.jianlianyemao.com/upload/img/${cate.img}" style="width: 100%;height: 100%"  class="img"/> </div>--%>
									<%--</c:if>--%>
								<%--</c:if>--%>
								<%--</c:when>--%>
									<%--<c:otherwise>--%>
										<%--暂无图片--%>
									<%--</c:otherwise>--%>
								<%--</c:choose>--%>
							<%--</c:forEach>--%>
						<%--</c:when>--%>
						<%--<c:otherwise>--%>
							<%--暂无图片--%>
						<%--</c:otherwise>--%>
					<%--</c:choose>--%>
                    <%--</span>--%>
			<%--</div>--%>
		<%--</div>--%>
		<c:if test="${pd.audit_final_state !='6'}">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>审核：</label>
				<div class="formControls col-xs-8 col-sm-9 skin-minimal" id="audit_state">
					<div class="radio-box">
						<input type="radio" id="flag-1" name="audit_state"  value="2" <c:if test="${pd.audit_state == '2' }">checked="checked"</c:if> checked="checked">
						<label for="flag-1">审核通过</label>
					</div>
					<div class="radio-box">
						<input type="radio" id="flag-2" name="audit_state" value="1" <c:if test="${pd.audit_state == '1' }">checked="checked"</c:if>>
						<label for="flag-2">审核不通过</label>
					</div>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">审核结果：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<textarea class="textarea"  placeholder=""  id="audit_failure_reason" name="audit_failure_reason">${pd.audit_failure_reason}</textarea>
				</div>
			</div>
		</c:if>
		<c:if test="${pd.audit_final_state =='6'}">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">签约证明文件图片：</label>
				<div class="formControls col-xs-8 col-sm-9">
				<span class="btn-upload form-group">
					<input type="file" id="document" name="document" onchange="check()" style="width:150px;" />
				</span>
				</div>
			</div>
		</c:if>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2"> 
				<button class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i>提交</button>
			</div>
		</div>
	</form>
</article> 
</div>
    <!-- 遮罩层-->
    <div id="outerdiv" style="position:fixed;top:0;left:0;background:rgba(0,0,0,0.7);z-index:2;width:100%;height:100%;display:none;">
        <div id="innerdiv" style="position:absolute;">
            <img id="bigimg" style="border:5px solid #fff;" src="" />
        </div>
    </div>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>

<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.config.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>js/uploadImg.js"></script>
<script type="text/javascript">

$(function(){
 var validate = $("#form-horizontal").validate({ 
	   submitHandler: function(form){ 
		   companyAdd(); //执行提交方法
          },
      });    
});

function companyAdd(){ 
	var index = parent.layer.getFrameIndex(window.name);
	debugger
    var url="<%=basePath%>yx_auditRecords/upexamine.do";
    var ID='${pd._id }';
    var user_id='${pd.user_id }';
    var audstate='${pd.audit_final_state}';
    var audit_state = $("input[name='audit_state']:checked").val();
   // var audit_state=$("#audit_state").val();
    var audit_failure_reason=$("#audit_failure_reason").val();
    var ACT_IMG=document.getElementById("document").value;
    if(audstate=='6'){
        audit_state=2;
        if(ACT_IMG==null||ACT_IMG==""||ACT_IMG==undefined){
            alert("请选择要签约证明文件的图片！");
            return
        }
	}
    var map = {
        "user_id":user_id,
        "ID":ID,
        "audstate":audstate,
        "audit_state":audit_state,
        "audit_failure_reason":audit_failure_reason
    };
    $.ajaxFileUpload({
        url : url,
        type:'post',
        secureuri:false,
        data:map,
        fileElementId:['document'],//file标签的id
        dataType: 'json',//返回数据的类型
        success: function (data, status) {
			if(typeof data=='string'){
                data= JSON.parse(data);
			}
            if(data.statusCode == 200){
                parent.$('.breadcrumb .r .Hui-iconfont').click();
                parent.layer.close(index);
            }
        },
        error: function (data, status, e) {
            alert(e);
        }
    });

	<%--$.ajax({--%>
            <%--url : "<%=basePath%>yx_auditRecords/upexamine.do",--%>
            <%--data :$('#form-horizontal').serialize(),--%>
            <%--type : 'post',--%>
            <%--dataType : 'json',--%>
            <%--async : false,--%>
            <%--success : function(result) {--%>
                <%--if(typeof result=='string'){--%>
                    <%--result= JSON.parse(result);--%>
                <%--}--%>
               <%--if(result.statusCode ==200){--%>
					<%--parent.$('.breadcrumb .r .Hui-iconfont').click();--%>
    				<%--parent.layer.close(index); --%>
            	 <%--}--%>
            <%--},--%>
            <%--error : function(msg) {--%>
            <%--}--%>
        <%--});  --%>
    }


$("body").on('click','img',function(){
    var _this = $(this);//将当前的img元素作为_this传入函数
    imgShow("#outerdiv", "#innerdiv", "#bigimg", _this);
});
function imgShow(outerdiv, innerdiv, bigimg, _this){
    var src = _this.attr("src");//获取当前点击的pimg元素中的src属性
    $(bigimg).attr("src", src);//设置#bigimg元素的src属性
    /*获取当前点击图片的真实大小，并显示弹出层及大图*/
    $("<img/>").attr("src", src).load(function(){
        var windowW = $(window).width();//获取当前窗口宽度
        var windowH = $(window).height();//获取当前窗口高度
        var realWidth = this.width;//获取图片真实宽度
        var realHeight = this.height;//获取图片真实高度
        var imgWidth, imgHeight;
        var scale = 0.8;//缩放尺寸，当图片真实宽度和高度大于窗口宽度和高度时进行缩放

        if(realHeight>windowH*scale) {//判断图片高度
            imgHeight = windowH*scale;//如大于窗口高度，图片高度进行缩放
            imgWidth = imgHeight/realHeight*realWidth;//等比例缩放宽度
            if(imgWidth>windowW*scale) {//如宽度扔大于窗口宽度
                imgWidth = windowW*scale;//再对宽度进行缩放
            }
        } else if(realWidth>windowW*scale) {//如图片高度合适，判断图片宽度
            imgWidth = windowW*scale;//如大于窗口宽度，图片宽度进行缩放
            imgHeight = imgWidth/realWidth*realHeight;//等比例缩放高度
        } else {//如果图片真实高度和宽度都符合要求，高宽不变
            imgWidth = realWidth;
            imgHeight = realHeight;
        }
        $(bigimg).css("width",imgWidth);//以最终的宽度对图片缩放

        var w = (windowW-imgWidth)/2;//计算图片与窗口左边距
        var h = (windowH-imgHeight)/2;//计算图片与窗口上边距
        $(innerdiv).css({"top":h, "left":w});//设置#innerdiv的top和left属性
        $(outerdiv).fadeIn("fast");//淡入显示#outerdiv及.pimg
    });
    $(bigimg).click(function(event){
        $(outerdiv).fadeOut();
        event.stopPropagation();//阻止事件冒泡

    });

    $(outerdiv).click(function(){//再次点击淡出消失弹出层
        $(this).fadeOut("fast");
    });
}
</script>
</body>

</html>
