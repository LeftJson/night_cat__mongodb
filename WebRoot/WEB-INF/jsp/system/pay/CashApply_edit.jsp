<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
      + request.getServerName() + ":" + request.getServerPort()
      + path + "/";
%> 
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
    <title>会员管理</title>
  </head>

  <body class="pos-r"> 
  <div>
<article class="page-container">
  <form class="form form-horizontal" id="form-horizontal">   
  <input type="hidden" value="${data.ID}" id="ID" name="ID">
  <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>提现申请状态：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.is_apply}" placeholder="" id="is_apply" name="is_apply" data-rule-required="true" >
      </div>
    </div>    
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>详情：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.detail}" placeholder="" id="detail" name="detail" data-rule-required="true" >
      </div>
    </div>  
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2">提现金额：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.price }" placeholder="" id="price" name="price">
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2">提现时间：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.time }" placeholder="" id="time" name="time">
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>申请任务编号：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.apply_id}" placeholder="" id="apply_id" name="apply_id" data-rule-required="true" >
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>提现记录：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.record_id}" placeholder="" id="record_id" name="record_id" data-rule-required="true" >
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>用户名：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.user_id}" placeholder="" id="user_id" name="user_id" data-rule-required="true" >
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>电话号码：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.phone}" placeholder="" id="phone" name="phone" data-rule-required="true" >
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>总金额：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" disabled="disabled" class="input-text" value="${data.all_money}" placeholder="" id="all_money" name="all_money" data-rule-required="true" >
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>拒绝原因：</label>
      <div class="formControls col-xs-8 col-sm-9">
        <input type="text" class="input-text" value="${data.reason}" placeholder="" id="reason" name="reason" data-rule-required="true" >
      </div>
    </div>     
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2"> 
        <button class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i>提交</button>
      </div>
    </div>
  </form>
</article> 
</div>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>

<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
<!--请在下方写此页面业务相关的脚本-->
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.config.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">

$(function(){
 var validate = $("#form-horizontal").validate({ 
     submitHandler: function(form){ 
       companyAdd(); //执行提交方法
          },
      });    
});

function companyAdd(){ 
  var index = parent.layer.getFrameIndex(window.name);  
  $.ajax({
            url : "<%=basePath%>CashApply/save.do",
            data :$('#form-horizontal').serialize(),
            type : 'post',
            dataType : 'json',
            async : false,
            success : function(result) {
               if(result.statusCode ==200){
          parent.$('.breadcrumb .r .Hui-iconfont').click();
            parent.layer.close(index);
             }
            },
            error : function(msg) {
            }
        });  
    } 
</script>
</body>

</html>
