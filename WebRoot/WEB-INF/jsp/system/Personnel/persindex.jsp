<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
		<title>会员管理</title>
	</head>

	<body class="pos-r"> 
		<div>
			<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>会员管理 <span class="c-gray en">&gt;</span>人才管理
				<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a>
			</nav>
			<div class="page-container">
				<div class="text-c"> 
		             <input type="text" id="REAL_NAME"  name="REAL_NAME"  placeholder="用户名称" style="width:150px" class="input-text"> 
				     <button name="search" id="search" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i>搜索</button>
			    </div>
				<div class="mt-20">
					<form id="form-mem">
						<table class="table table-border table-bordered table-bg table-hover table-sort">
							<thead>
								<tr class="text-c">
									<th style="width: 150px;height: 20px;">真实名称</th>
									<th style="width: 150px;height: 20px;">学校名称</th>
									<th style="width: 150px;height: 20px;">学历</th>
									<th style="width: 170px;height: 20px;">邮箱</th>
									<th style="width: 150px;height: 20px;">城市</th>
									<th style="width: 150px;height: 20px;">期望职位</th>
									<th style="width: 100px;height: 20px;">期望薪资</th>
									<th style="width: 150px;height: 20px;">期望描述</th>
									<%--<th>状态</th>--%>
									<th style="width: 150px;height: 20px;">操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>
		<script type="text/javascript">  
		 var table;
			$(function() {
				//table
				table = $('.table-sort').dataTable({
					"aaSorting": [
						//[1, "asc"]
					], //默认第几个排序
					"aoColumnDefs": [
						//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
						//{"orderable":false,"aTargets":[3]} 
					],
					"bStateSave": true, //状态保存
					"bProcessing": false, // 是否显示取数据时的那个等待提示
	                "bServerSide": true,//这个用来指明是通过服务端来取数据
	                "sAjaxSource": "<%=basePath%>yx_resumes/querytbuser.do",//这个是请求的地址
	                "bLengthChange" : true,// 每行显示记录数  
	                "iDisplayLength" : 10,// 每页显示行数  
	                "bSort" : true,// 排序
	                "bAutoWidth" : false, //是否自适应宽度  
	                "searching":false,//隐藏右侧搜索框 
	                "bPaginate": true, //翻页功能
	                "bLengthChange": true, //改变每页显示数据数量 
                     "aoColumns": [
              		 {  
	                     "sClass":"center", 
	                     "mDataProp" : "real_name",
	                     "aTargets":[0],
	                     "mData":"real_name"
	                 },
	                 {  
                       	"sClass":"center", 
                       	"mDataProp" : "school_name",
                        "aTargets":[1],
                         "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                             return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.school_name+'">'+c.school_name+'</div>';
                         }
                     },
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "education",
	                     "aTargets":[2],
	                     "mData":"education"
	                 },  
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "email",
	                     "aTargets":[3],
                         "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                             return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.email+'">'+c.email+'</div>';
                         }
	                 },
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "city",
	                     "aTargets":[4],
	                     "mData":"city"
	                 },
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "expected_positions",
	                     "aTargets":[5],
                         "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                             return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.expected_positions+'">'+c.expected_positions+'</div>';
                         }
	                 }, 
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "expected_salary",
	                     "aTargets":[6],
	                     "mData":"expected_salary"
	                 }, 
	                 {  
	                     "sClass":"center", 
	                     "mDataProp" : "expected_description",
	                     "aTargets":[7],
                         "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                             return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.expected_description+'">'+c.expected_description+'</div>';
                         }
                     },
	                 {
	                     "sClass":"center",  
	                     "aTargets":[8],
	                     "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象  
	                     	return '<a data-title=\"查看详情\" onclick=\"tiao(\'查看详情\',\'<%=basePath%>yx_resumes/adduserid.do?ID='+c._id+'\',\'\',\'550\')\" href=\"javascript:;\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\">查看详情</a>';
	                    }  
	                 }
			], 
			  
			 "fnServerData" : function(sSource, aoData, fnCallback) {  
				//获取检索参数
			    var REAL_NAME = $("#REAL_NAME").val();  
				 $.ajax({
		                url : sSource,//这个就是请求地址对应sAjaxSource
		                data : {"aoData":JSON.stringify(aoData),"name":REAL_NAME},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
		                type : 'post',
		                dataType : 'json',
		                async : false,
		                success : function(result) {
		                	var data = {};
							if( result.sEcho ){
							  data = result;
							}else{
							  data = JSON.parse(result);
							}
							fnCallback(data);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的 
		    				//表格数据居中
		    				$("td").attr("style","text-align:center");
		                },
		                error : function(msg) {
		                }
		            });
	           }
		});
		
		//搜索
		$('#search').click( function() {
		    table.fnDraw(); 
		});
		
		$(document).keyup(function(event){
		  if(event.keyCode ==13){
		    $("#search").trigger("click");
		  }
		});
	});   
			function quserpany(url){  
				   $.ajax({  
				         type: "POST",
				         url:url,
				         data:$('#form-mem').serialize(), 
				         async: false,
				         error: function(request) {
				             alert("Connection error");
				         },
				         success: function(data) {
				         alert("操作成功！"); 
				            $(".show_iframe").html(data);  
				         }
				     });
				}

				function tiao(title,url,w,h){  
					var index=layer.open({
						type:2,
						title:title,
						content:url

				});
					layer.full(index);
				}

				//审核
				function tiaosh(title,url,w,h){
					    var checklist = document.getElementsByName ("ID");
					   var len=$("input[name='ID']:checked").length; 
					   if(len==0){
					      alert("请选择一个会员！");
					   }else if(len>1){
					      alert("只能选择一个会员！");
					   }else{
					      var data=$("input[name='ID']:checked").val();
					      var URL=url+"?ID="+data;
					      var index=layer.open({
					  		type:2,
					  		title:title,
					  		content:URL

					  });
					  	layer.full(index); 
					   }
				    }

				function piliangdel(url){  
				   $.ajax({  
				        url : url, 
				         type : 'post',
				         dataType : 'json',
				         data:$('#form-mem').serialize(), 
				         async: false,
				        success : function(result) { 
				          		parent.$('.breadcrumb .r .Hui-iconfont').click();
				          		window.location.reload();   
				           },
				           error : function(msg) {
				           }
				     });
				}

				function project_del(url){
								 layer.confirm('确认要删除该数据吗？',function(index){ 
									//此处请求后台程序，下方是成功后的前台处理…… 
									$.ajax({
						                url : url, 
						                type : 'post',
						                dataType : 'json',
						                async : false,
						                success : function(result) {
						                	 /*if(result.status == 1){*/
						                		parent.$('.breadcrumb .r .Hui-iconfont').click();
						                		window.location.reload(); 
						                },
						                error : function(msg) {
						                }
						            });
								 }); 
							}
							
				function delbyid(url){  
				layer.confirm('确认要删除该数据吗？',function(index){ 
				   $.ajax({  
				         type: "POST",
				         url:url,
				         data:$('#form-pany').serialize(), 
				         async: false,
				         success : function(result) { 
				          		parent.$('.breadcrumb .r .Hui-iconfont').click();
				          		window.location.reload();   
				           },
				           error : function(msg) {
				           }
				     });
					});
				}
				//禁止/解禁
				function uppany(url){
					   var checklist = document.getElementsByName ("ID");
					   var len=$("input[name='ID']:checked").length;
					   alert(len);
					   if(len==0){
					      alert("请选择一个需要审核/不审核的用户！");
					   }else if(len>1){
					      alert("只能选择一个需要审核/不审核的用户！");
					   }else{
					   var data=$("input[name='ID']:checked").val();
				       $.ajax({  
					         type: "POST",
					         url:url,
					         data:$('#form-mem').serialize(),  
					         async: false,
					          success : function(result) { 
					       /*    
				             if(result.statusCode == 200){ */
				              		parent.$('.breadcrumb .r .Hui-iconfont').click();
				              		window.location.reload();   
				               },
				               error : function(msg) {
				               }
					    });
				    }
				    }
				   //分配
				function fenpei(title,url,w,h){
					    var checklist = document.getElementsByName ("ID");
					   var len=$("input[name='ID']:checked").length; 
					   if(len==0){
					      alert("请选择一个会员！");
					   }else if(len>1){
					      alert("只能选择一个会员！");
					   }else{
					      var data=$("input[name='ID']:checked").val();
					      var URL=url+"?ID="+data;
					      var index=layer.open({
					  		type:2,
					  		title:title,
					  		content:URL

					  });
					  	layer.full(index); 
					   }
				    }
				    
				  //重置密码
				  function uppw(url){
					  var checklist = document.getElementsByName ("ID");
					   var len=$("input[name='ID']:checked").length;
					   if(len==0){
					      alert("请选择一个需要重置的用户！");
					   }else if(len>1){
					      alert("只能选择一个需要重置的用户！");
					   }else{
					   var data=$("input[name='ID']:checked").val();
				       $.ajax({  
					         type: "POST",
					         url:url,
					         data:$('#form-mem').serialize(),  
					         async: false,
					          success : function(result) {  
				             		alert("密码重置成功！");
				              		parent.$('.breadcrumb .r .Hui-iconfont').click();
				              		window.location.reload();   
				               },
				               error : function(msg) {
				               }
					    });
				    }
				}
				</script>
				</body>

				</html>