<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
		<title>编辑人才信息</title>
	</head>

	<body class="pos-r"> 
	<div>
<article class="page-container">
	<form class="form form-horizontal" id="form-horizontal">   
	<input type="hidden" value="${data._id}" id="_id" name="_id">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">用户姓名：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.real_name}" readonly="readonly"  placeholder="" id="real_name" name="real_name" data-rule-required="true" >
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">出生日期：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.birthday}" readonly="readonly"  placeholder="" id="birthday" name="birthday" data-rule-required="true" >
				<%--<input type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly"  class="input-text Wdate" value="${data.birthday }" placeholder="" id="birthday" name="birthday" data-rule-required="true">--%>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">学校名称：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.school_name }" readonly="readonly"  placeholder="" id="school_name" name="school_name">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">y邮箱：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.email }" readonly="readonly"  placeholder="" id="email" name="email">
			</div>
		</div>   
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">城市：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.city }" readonly="readonly"  placeholder="" id="city" name="city">
			</div>
		</div>  
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">个人描述：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<div class="input-text" style="border: none;">${data.description}</div>
				<%--<textarea  class="textarea" name="description" id="description" readonly="readonly"  >${data.description}</textarea>--%>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">期望职位：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.expected_positions }" readonly="readonly"  placeholder="" id="expected_positions" name="expected_positions">
			</div>
		</div> 
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">期望薪资：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.expected_salary }" readonly="readonly"  placeholder="" id="expected_salary" name="expected_salary">
			</div>
		</div> 
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">期望城市：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<input type="text" class="input-text" style="border: none;" value="${data.expected_city }" readonly="readonly"  placeholder="" id="expected_city" name="expected_city">
			</div>
		</div> 
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2" style="padding-right:0">期望描述：</label>
			<div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
				<div>${data.expected_description}</div>
				<%--<textarea  class="textarea" name="expected_description" id="expected_description"  readonly="readonly" >${data.expected_description}</textarea>--%>
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
				<input class="btn btn-primary radius" onclick="clone_palne()" type="button" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
			</div>
		</div>
	</form>
</article> 
</div>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>

<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
<!--请在下方写此页面业务相关的脚本-->
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.config.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
<script>
    function clone_palne(){
            parent.$('.r .Hui-iconfont').click();
            parent.layer.close(index);
    }
</script>
</body>

</html>
