<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
		<title>活动管理</title>
		<style>
            .wt_img{
               width:200px;
               height:150px;
            }		
		</style>
	</head>

	<body class="pos-r"> 
		<div>
			<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>活动管理<span class="c-gray en">&gt;</span>活动管理
				<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a>
			</nav> 
			<div class="page-container">
				<div class="text-c"> 
				     <input type="text" id="ACT_NAME"  name="ACT_NAME"  placeholder="名称" style="width:250px" class="input-text"> 
				     <button name="search" id="search" class="btn btn-success" type="submit"><i class="Hui-iconfont">&#xe665;</i>搜索</button>
				</div>
				<div class="cl pd-5 bg-1 bk-gray mt-20"> 
				     <span class="l">
					    <a class="btn btn-primary radius" data-title="添加活动" onclick="act_edit('添加活动','<%=basePath%>yx_activity/queryById.do','','550')" href="javascript:;">发布活动</a>
					</span> 
                </div>
				<div class="mt-20">
					<form id="form-activity">
					<table class="table table-border table-bordered table-bg table-hover table-sort">
						<thead>
							<tr class="text-c">
								<th style="width: 150px;height: 20px;">活动名称</th>
								<th style="width: 150px;height: 20px;">活动开始时间</th>
								<th style="width: 150px;height: 20px;">活动结束时间</th>
								<th style="width: 150px;height: 20px;">活动截止时间</th>
								<th style="width: 150px;height: 20px;">活动状态</th>
								<th style="width: 150px;height: 20px;">查看参与人</th>
								<th style="width: 150px;height: 20px;">操作</th>
							</tr>
						</thead>
						<tbody> 
						</tbody>
					</table>
					</form>
				</div>
			</div>  
		</div>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
		<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>
		<script type="text/javascript">
            //3.2、时间处理-时间轴=>年月日格式
            function timeStamp2String (time) {
                //y=年;ymd=年月日;ymdhm年月日时分;ymdhms年月日时分秒
                var datetime = new Date();
                datetime.setTime(time);
                var year = datetime.getFullYear();
                var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
                var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
                var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
                var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
                return year + "-" + month + "-" + date + " " + hour + ":" + minute;
            }
            function contrastTime(start) {
                var evalue = document.getElementById(start).value;
                var dB = new Date(evalue.replace(/-/g, "/"));//获取当前选择日期
                var d = new Date();
                var str = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();//获取当前实际日期
                if (Date.parse(str) > Date.parse(dB)) {//时间戳对比
                    return 1;
                }
                return 0;
            }
            var table;
		$(function() {
			//table
			table = $('.table-sort').dataTable({
				"aaSorting": [
					//[13, "desc"],
					//[12, "desc"]
				], //默认第几个排序
				"aoColumnDefs": [
					//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
					//{"orderable":false,"aTargets":[3]} 
				],
				"bStateSave": true, //状态保存
				"bProcessing": false, // 是否显示取数据时的那个等待提示
                "bServerSide": true,//这个用来指明是通过服务端来取数据
                "sAjaxSource": "<%=basePath%>yx_activity/queryList.do",//这个是请求的地址
                "bLengthChange" : true,// 每行显示记录数  
                "iDisplayLength" : 10,// 每页显示行数  
                "bSort" : true,// 排序
                "bAutoWidth" : false, //是否自适应宽度  
                "searching":false,//隐藏右侧搜索框 
                "bPaginate": true, //翻页功能
                "bLengthChange": true, //改变每页显示数据数量
                "aoColumns": [
          	                 {  
          	                     "sClass":"center", 
          	                     "mDataProp" : "title",
          	                     "aTargets":[0],
                                 "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                                     return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.title+'">'+c.title+'</div>';
                                 }
          	                 },
          	                 {  
          	                     "sClass":"center", 
          	                     "mDataProp" : "start_date",
          	                     "aTargets":[1],
                                 "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                                     var date1= timeStamp2String(c.start_date);
                                     return date1;
                                 }
          	                 },
          	                  {  
          	                     "sClass":"center", 
          	                     "mDataProp" : "end_date",
          	                     "aTargets":[2],
                                  "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                                      var date2= timeStamp2String(c.end_date);
                                      return date2;
                                  }
          	                 }, 
          	                  {  
          	                     "sClass":"center", 
          	                     "mDataProp" : "due_date",
          	                     "aTargets":[3],
                                  "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                                      var date3= timeStamp2String(c.due_date);
                                      return date3;
                                  }
          	                 },
							{
								"sClass":"center",
								"mDataProp" : "actstate",
								"aTargets":[4],
                                "mData":"actstate"
							},
							{
								"sClass":"center",
								"aTargets":[5],
								"mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
									return '<a _href=\"<%=basePath%>yx_activity/toBidder.do?ID='+c._id+'\" data-title=\"查看参与人员\" class=\"btn btn-link\" onclick=\"Hui_admin_tab(this)\" href=\"javascript:;\">查看参与人员</a>';
								}
							},
          	                 {
          	                     "sClass":"center",  
          	                     "aTargets":[6],
          	                     "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
									 if(c.apd >0){
                                         return '<a data-title=\"编辑\" onclick=\"act_edit(\'编辑\',\'<%=basePath%>yx_activity/toEdit.do?ID='+c._id+'\',\'\',\'550\')\" href=\"javascript:;\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6df;</i> 编辑</a>&nbsp<a data-title=\"删除\" href=\"javascript:;\" onclick=\"act_del(\'<%=basePath%>yx_activity/delactivitybyid.do?ID='+c._id+'\')\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6e2;</i> 删除</a>';
									 }else {
                                         return '<a data-title=\"编辑\" onclick=\"act_edit(\'编辑\',\'<%=basePath%>yx_activity/toEdit.do?ID='+c._id+'\',\'\',\'550\')\" href=\"javascript:;\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6df;</i> 编辑</a>';
									 }

          	                    }  
          	                 }
          			], 
				 "fnServerData" : function(sSource, aoData, fnCallback) {  
	                //获取检索参数
					var NAME = $("#ACT_NAME").val();
					 $.ajax({
			                url : sSource,//这个就是请求地址对应sAjaxSource
			                data : {"aoData":JSON.stringify(aoData),"ACT_NAME":NAME},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
			                type : 'post',
			                dataType : 'json',
			                async : false,
			                success : function(result) {
								 var data = {};
								if( result.sEcho ){
								  data = result;
								}else{
								  data = JSON.parse(result);
								}
		                		fnCallback(data);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的 
			                	$(".01").css("color","red"); 
			                	//表格数据居中
			    				$("td").attr("style","text-align:center");
			                },
			                error : function(msg) {
			                }
			            });
		           }  
			});
			
			//检索
			$('#search').click( function() {
			    table.fnDraw();
			});
			$(document).keyup(function(event){
			  if(event.keyCode ==13){
			    $("#search").trigger("click");
			  }
			});
		});
		
			function act_del(url){ 
				 layer.confirm('确认要删除该数据吗？',function(index){ 
					//此处请求后台程序，下方是成功后的前台处理…… 
					$.ajax({
		                url : url, 
		                type : 'post',
		                dataType : 'json',
		                async : false,
		                success : function(result) {
		                	 if(result.statusCode == 200){
		                		 table.fnDraw();
		                		$(".layui-layer-setwin .layui-layer-close1").click(); 
			                } 
		                },
		                error : function(msg) {
		                }
		            });
				 }); 
			}
			 
			/*编辑*/
			function act_edit(title,url,w,h){ 
				var index=layer.open({
					type:2,
					title:title,
					content:url

			});
				layer.full(index);
	         } 
			
			
			//审核
			function act_check(title,url,w,h){
				    var checklist = document.getElementsByName ("ID");
				   var len=$("input[name='ID']:checked").length; 
				   if(len==0){
				      alert("请选择一个活动！");
				   }else if(len>1){
				      alert("只能选择一个活动！");
				   }else{
				      var data=$("input[name='ID']:checked").val();
				      var URL=url+"?ID="+data;
				      //layer_show(title,URL,w,h); 
				      var index=layer.open({
				  		type:2,
				  		title:title,
				  		content:URL

				 		 });
				  	layer.full(index);
				   }
			    }
			
			function img_edit(title,url,w,h){ 
				 var len=$("input[name='ID']:checked").length; 
				 if(len==0){
				     alert("请选择一个活动！");
				 }else if(len>1){
				     alert("只能选择一个活动！");
				 }else{
					 var data=$("input[name='ID']:checked").val();
				     var URL=url+"?ACT_ID="+data;
				     layer_show(title,URL,w,h);
				 } 
			}

			function piliangdel(url){
				   var checklist = document.getElementsByName ("ID");
				   var len=$("input[name='ID']:checked").length;
				   if(len==0){
				      alert("请选择一个或多个需要删除的用户！");
				   }else if(len==1){
				      alert("只能选择两个或两个以上需要删除的用户！");
				   }
				   else{
				   var data=$("input[name='ID']:checked").val();  
			   $.ajax({  
			         type: "POST",
			         url:url,
			         data:$('#form-activity').serialize(), 
			         async: false,
			         success : function(result) { 
			          		parent.$('.breadcrumb .r .Hui-iconfont').click();
			          		window.location.reload();   
			           },
			           error : function(msg) {
			           }
			     });
			}
			}
			
			function exportFeedback(){
			  var checkedElemts = $("#form-activity input:checked");
			  var ids = "";
			  for(var i = 0, il = checkedElemts.length; i < il; i++){
			    ids += "\'" + checkedElemts[i].value + "\',";
			  }
			  ids = ids.substring(0, ids.length - 1);
			  $("#form-activity").append($('<a id="exportFeedback_a" style="display: none" href="<%=basePath%>activity/exportfeedback.do?ids='+ids+'"/>'));
			  $("#exportFeedback_a")[0].click();
			  $("#exportFeedback_a").remove();
			}
</script> 
</body> 
</html>
