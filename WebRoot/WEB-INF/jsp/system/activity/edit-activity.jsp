<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%> 

<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
		<title>添加活动</title>
	</head>

<body class="pos-r">  
<article class="page-container">
	<form class="form form-horizontal" id="form-horizontal">
		<input type="hidden" name="ID" id="ID" value="${pd._id }"/>
		<input type="hidden" name="state" id="state" value="0"/>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>活动名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib" value="${pd.title}" placeholder="" id="title" name="title" data-rule-required="true">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>活动简介：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib" value="${pd.description}" placeholder="" id="description" name="description" data-rule-required="true">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">活动封面图片：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<span class="btn-upload form-group">
                    <c:if test="${pd.cover !=null && pd.cover!='' && pd.cover!=undefined}">
                        <img id="showcover" src="http://www.jianlianyemao.com/upload/img/${pd.cover }" style="width: 31px;height: 31px"  class="img"/>
                    </c:if>
					<c:if test="${pd.cover==null || pd.cover=='' || pd.cover==undefined}">
                        <img id="showcover" src="http://www.jianlianyemao.com/upload/static/yx_404.png" style="width: 31px;height: 31px"  class="img"/>
                    </c:if>
					<input type="file" id="cover" name="cover" onchange="check(this,0)"   style="width:64px;display:inline-block; overflow:hidden;"  value="点击添加图片"/>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">活动海报图片：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<span class="btn-upload form-group">
					<c:if test="${pd.poster_img !=null && pd.poster_img!='' && pd.poster_img!=undefined}">
						<img id="showposter_img" src="http://www.jianlianyemao.com/upload/img/${pd.poster_img }" style="width: 31px;height: 31px"  class="img"/>
					</c:if>
					<c:if test="${pd.poster_img==null || pd.poster_img=='' || pd.poster_img==undefined}">
						<img id="showposter_img" src="http://www.jianlianyemao.com/upload/static/yx_404.png" style="width: 31px;height: 31px"  class="img"/>
					</c:if>
					<input type="file" id="poster_img" name="poster_img" onchange="check(this,1)" style="width:64px;display:inline-block; overflow:hidden;"  value="点击添加图片"/>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2">活动详情图片：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<span class="btn-upload form-group">
					<c:if test="${pd.details_img !=null && pd.details_img!='' && pd.details_img!=undefined}">
						<img id="showdetails_img" src="http://www.jianlianyemao.com/upload/img/${pd.details_img }" style="width: 31px;height: 31px"  class="img"/>
					</c:if>
					<c:if test="${pd.details_img==null || pd.details_img=='' || pd.details_img==undefined}">
						<img id="showdetails_img" src="http://www.jianlianyemao.com/upload/static/yx_404.png" style="width: 31px;height: 31px"  class="img"/>
					</c:if>
					<input type="file" id="details_img" name="details_img" onchange="check(this,2)"  style="width:64px;display:inline-block; overflow:hidden;"  value="点击添加图片"/>
				</span>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>开始时间：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib Wdate" value="${pd.start_date }" placeholder="" id="start_date" name="start_date" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" data-rule-required="true">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>结束时间：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib Wdate" value="${pd.end_date }" placeholder="" id="end_date" name="end_date" data-rule-required="true"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:m'})">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>截止时间：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib Wdate" value="${pd.due_date }" placeholder="" id="due_date" name="due_date" data-rule-required="true"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:m'})">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>报名人数：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text valib" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" value="${pd.enrollment }" placeholder="" id="enrollment" name="enrollment" data-rule-required="true">
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				<input class="btn btn-primary radius" onclick="clone_palne()" type="button" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
			</div>
		</div>
	</form>
</article>
<!-- 遮罩层-->
<div id="outerdiv" style="position:fixed;top:0;left:0;background:rgba(0,0,0,0.7);z-index:2;width:100%;height:100%;display:none;">
	<div id="innerdiv" style="position:absolute;">
		<img id="bigimg" style="border:5px solid #fff;" src="" />
	</div>
</div>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js"></script>
<script type="text/javascript" src="<%=basePath%>js/uploadImg.js"></script>
<script type="text/javascript">

    //3.2、时间处理-时间轴=>年月日格式
    function timeStamp2String (time) {
        //y=年;ymd=年月日;ymdhm年月日时分;ymdhms年月日时分秒
        var datetime = new Date();
        datetime.setTime(time);
        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
        var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
        var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
        var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
        return year + "-" + month + "-" + date + " " + hour + ":" + minute;
    }

    function clone_palne(){
            parent.$('.r .Hui-iconfont').click();
            parent.layer.close(index);
    }

    $(function(){
        var start_date='${pd.start_date }';
        var end_date='${pd.end_date }';
        var due_date='${pd.due_date }';


        var timestamp = new Date(parseInt(start_date));
        var timestamp2 = new Date(parseInt(end_date));
        var timestamp3 = new Date(parseInt(due_date));
        $('#start_date').val(timeStamp2String(timestamp));
        $('#end_date').val(timeStamp2String(timestamp2));
        $('#due_date').val(timeStamp2String(timestamp3));
        $(".city-select").hide();
        var validate = $("#form-horizontal").validate({
            submitHandler: function(form){
                companyAdd(); //执行提交方法
            },
        });
    });

    function companyAdd(){
        var index = parent.layer.getFrameIndex(window.name);
        var title=$('#title').val();
        var start_date=$('#start_date').val();
        var end_date=$('#end_date').val();
        var due_date=$('#due_date').val();

        var stadate=new Date(start_date);
        var enddate=new Date(end_date);
        var duedate=new Date(due_date);
        if(stadate.getTime() > enddate.getTime()){
            alert("开始时间不能大于结束时间！");
            return
        }else if(duedate.getTime() > enddate.getTime()){
            alert("截止时间不能大于结束时间！");
            return
        }
        var enrollment=$('#enrollment').val();
        var description=$('#description').val();
        var ID=$('#ID').val();
        var state=$('#state').val();
        var url="<%=basePath%>yx_activity/editActImg.do";
        var map = {
            "title":title,
            "ID":ID,
            "start_date":stadate.getTime(),
            "end_date":enddate.getTime(),
            "due_date":duedate.getTime(),
            "state":state,
            "description":description,
            "enrollment":enrollment
        };
        $.ajaxFileUpload({
            url : url,
            type:'post',
            secureuri:false,
            data:map,
            fileElementId:['cover','poster_img','details_img'],//file标签的id
            dataType: 'json',//返回数据的类型
            success: function (data, status) {
                if(data.statusCode == 200){
                    parent.$('.breadcrumb .r .Hui-iconfont').click();
                    parent.layer.close(index);
                }else if(data.statusCode == 400){
                    parent.$('.breadcrumb .r .Hui-iconfont').click();
                    parent.layer.close(index);
				}
            },
            error: function (data, status, e) {
                alert(e);
            }
        });
    }

    function check(e,flag){
        var file = e.files[0];
        var id = null;
        var reader = new FileReader();
        if(flag==0){
            id = "#showcover";
        }else if(flag==1){
            id = "#showposter_img";
        }else if(flag==2){
            id = "#showdetails_img";
        }
        if( id!=null ){
            reader.onload=function(f) {
                $(id).attr('src', f.target.result);
            };
            reader.readAsDataURL(file);
        }
        //验证封面图片
        var aa=document.getElementById("cover").value.toLowerCase().split('.');//以“.”分隔上传文件字符串
        var bb=document.getElementById("details_img").value.toLowerCase().split('.');//以“.”分隔上传文件字符串
        var cc=document.getElementById("poster_img").value.toLowerCase().split('.');//以“.”分隔上传文件字符串
        //$("#showcover").val(document.getElementById("cover").files[0].name);

        if(aa[aa.length-1]=='gif'||aa[aa.length-1]=='jpg'||aa[aa.length-1]=='png'||aa[aa.length-1]=='jpeg'){//判断图片格式
            var imagSize =  document.getElementById("cover").files[0].size;
            if(imagSize<930*410){
                return true;
            }else{
                alert("图片为："+imagSize/(930*410)+",请重新上传！");
                document.getElementById("cover").value=null;
                return false;
            }
        }else if( aa[aa.length-1]=='' ){
            return false;
        }else{
            alert('请选择格式为*.jpg、*.gif、*.png、*.jpeg 的图片');//jpg和jpeg格式是一样的只是系统Windows认jpg，Mac OS认jpeg，//二者区别自行百度
            document.getElementById("cover").value=null;
            return false;
        }

        //验证活动详情图片

        if(bb[bb.length-1]=='gif'||bb[bb.length-1]=='jpg'||bb[bb.length-1]=='png'||bb[bb.length-1]=='jpeg'){//判断图片格式
            var imagSize =  document.getElementById("details_img").files[0].size;
            if(imagSize<1400*5208){
                return true;
            }else{
                alert("图片为："+imagSize/(1400*5208)+"M,大于1M,请重新上传！");
                document.getElementById("details_img").value=null;
                return false;
            }
        }else if( bb[bb.length-1]=='' ){
            return false;
        }else{
            alert('请选择格式为*.jpg、*.gif、*.png、*.jpeg 的图片');//jpg和jpeg格式是一样的只是系统Windows认jpg，Mac OS认jpeg，//二者区别自行百度
            document.getElementById("details_img").value=null;
            return false;
        }
        //验证活动海报图片

        if(cc[cc.length-1]=='gif'||cc[cc.length-1]=='jpg'||cc[cc.length-1]=='png'||cc[cc.length-1]=='jpeg'){//判断图片格式
            var imagSize =  document.getElementById("poster_img").files[0].size;
            if(imagSize<1400*2208){
                return true;
            }else{
                alert("图片为："+imagSize/(1400*2208)+"M,大于1M,请重新上传！");
                document.getElementById("poster_img").value=null;
                return false;
            }
        }else if( cc[cc.length-1]=='' ){
            return false;
        }else{
            alert('请选择格式为*.jpg、*.gif、*.png、*.jpeg 的图片');//jpg和jpeg格式是一样的只是系统Windows认jpg，Mac OS认jpeg，//二者区别自行百度
            document.getElementById("poster_img").value=null;
            return false;
        }


    }

    function addImage(){
        var cover = $('#cover');
        debugger;
        cover.click();
        debugger;
        var file = cover.files[0];

        var reader = new FileReader();
        //创建文件读取相关的变量
        var imgFile;

        //为文件读取成功设置事件
        reader.onload=function(e) {
            alert('文件读取完成');
            imgFile = e.target.result;
            console.log(imgFile);
            $('#imgLink').attr('href','aaaaaaa');
            debugger;
        };

        //正式读取文件
        reader.readAsDataURL(file);
    }


    $("body").on('click','img',function(){
        var _this = $(this);//将当前的img元素作为_this传入函数
        imgShow("#outerdiv", "#innerdiv", "#bigimg", _this);
    });
    function imgShow(outerdiv, innerdiv, bigimg, _this){
        var src = _this.attr("src");//获取当前点击的pimg元素中的src属性
        $(bigimg).attr("src", src);//设置#bigimg元素的src属性
        /*获取当前点击图片的真实大小，并显示弹出层及大图*/
        $("<img/>").attr("src", src).load(function(){
            var windowW = $(window).width();//获取当前窗口宽度
            var windowH = $(window).height();//获取当前窗口高度
            var realWidth = this.width;//获取图片真实宽度
            var realHeight = this.height;//获取图片真实高度
            var imgWidth, imgHeight;
            var scale = 0.8;//缩放尺寸，当图片真实宽度和高度大于窗口宽度和高度时进行缩放

            if(realHeight>windowH*scale) {//判断图片高度
                imgHeight = windowH*scale;//如大于窗口高度，图片高度进行缩放
                imgWidth = imgHeight/realHeight*realWidth;//等比例缩放宽度
                if(imgWidth>windowW*scale) {//如宽度扔大于窗口宽度
                    imgWidth = windowW*scale;//再对宽度进行缩放
                }
            } else if(realWidth>windowW*scale) {//如图片高度合适，判断图片宽度
                imgWidth = windowW*scale;//如大于窗口宽度，图片宽度进行缩放
                imgHeight = imgWidth/realWidth*realHeight;//等比例缩放高度
            } else {//如果图片真实高度和宽度都符合要求，高宽不变
                imgWidth = realWidth;
                imgHeight = realHeight;
            }
            $(bigimg).css("width",imgWidth);//以最终的宽度对图片缩放

            var w = (windowW-imgWidth)/2;//计算图片与窗口左边距
            var h = (windowH-imgHeight)/2;//计算图片与窗口上边距
            $(innerdiv).css({"top":h, "left":w});//设置#innerdiv的top和left属性
            $(outerdiv).fadeIn("fast");//淡入显示#outerdiv及.pimg
        });
        $(bigimg).click(function(event){
            $(outerdiv).fadeOut();
            event.stopPropagation();//阻止事件冒泡

        });

        $(outerdiv).click(function(){//再次点击淡出消失弹出层
            $(this).fadeOut("fast");
        });
    }
</script>
</body>

</html>
