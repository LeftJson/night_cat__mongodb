<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
      + request.getServerName() + ":" + request.getServerPort()
      + path + "/";
%> 
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>

<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
    <title>会员管理</title>
  </head>

  <body class="pos-r"> 
  <div>
<article class="page-container">
  <form class="form form-horizontal" id="form-horizontal">   
  <input type="hidden" value="${pd._id}" id="_id" name="_id">
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">标题：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_title}" placeholder="" id="project_title" name="project_title" data-rule-required="true" >
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">项目编号：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_number}" placeholder="" id="project_number" name="project_number" data-rule-required="true" >
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">项目图片：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <span class="select-box" style="display: -webkit-flex;border:none !important;">
            <c:forEach items="${pd.imgs}" var="cate">
                      <div style="width: 250px;height: 250px;padding: 5px 5px;"><img src="http://www.jianlianyemao.com/upload/img/${cate}" style="width: 100%;height: 100%"  class="img"/> </div>
            </c:forEach>
        </span>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">用户姓名：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.user_name}" placeholder="" id="user_name" name="user_name" data-rule-required="true" >
      </div>
    </div>  
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">项目类型：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.type_name }" placeholder="" id="type_name" name="type_name">
      </div>
    </div> 
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">地区：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_region }" placeholder="" id="project_region" name="project_region">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2"style="padding-right:0">设计开始时间：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_startTime }" placeholder="" id="project_startTime" name="project_startTime">
        <%--<input readonly="readonly" type="text" class="input-text valib Wdate" value="${pd.project_startTime }" placeholder="" id="project_startTime" name="project_startTime" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" data-rule-required="true">--%>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">设计结束时间：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_endTime }" placeholder="" id="project_endTime" name="project_endTime">
        <%--<input readonly="readonly" type="text" class="input-text valib Wdate" value="${pd.project_endTime }" placeholder="" id="project_endTime" name="project_endTime" data-rule-required="true"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:m'})">--%>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">抢单截止日期：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_deadLine }" placeholder="" id="project_deadLine" name="project_deadLine">
        <%--<input readonly="readonly" type="text" class="input-text valib Wdate" value="${pd.project_deadLine }" placeholder="" id="project_deadLine" name="project_deadLine" data-rule-required="true"  onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:m'})">--%>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">订单状态：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <c:if test="${pd.project_state == '0'}">
          <div>抢单中</div>
        </c:if>
        <c:if test="${pd.project_state == '1'}">
          <div>已选中</div>
        </c:if>
        <c:if test="${pd.project_state == '2'}">
          <div>确认完善</div>
        </c:if>
        <c:if test="${pd.project_state == '3'}">
          <div>待支付</div>
        </c:if>
        <c:if test="${pd.project_state == '4'}">
          <div>已支付</div>
        </c:if>
        <c:if test="${pd.project_state == '5'}">
          <div>确认交付</div>
        </c:if>
        <c:if test="${pd.project_state == '6'}">
          <div>一键会审</div>
        </c:if>
        <c:if test="${pd.project_state == '7'}">
          <div>已完成</div>
        </c:if>
        <c:if test="${pd.project_state == '8'}">
          <div>已结束</div>
        </c:if>
        <c:if test="${pd.project_state == '9'}">
          <div>已屏蔽</div>
        </c:if>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">描述：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <div>${pd.project_describe}</div>
        <%--<textarea readonly="readonly" class="textarea" style="border:none !important;" placeholder=""  id="Text" name="Text" data-rule-required="true">${pd.project_describe}</textarea>--%>
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">预算：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_budget }" placeholder="" id="project_budget" name="project_budget">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">设计单位：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_unit }" placeholder="" id="project_unit" name="project_unit">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">设计面积：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_area }" placeholder="" id="project_area" name="project_area">
      </div>
    </div>
    <div class="row cl">
      <label class="form-label col-xs-4 col-sm-2" style="padding-right:0">工时：</label>
      <div class="formControls col-xs-8 col-sm-9" style="padding-left:0">
        <input readonly="readonly" type="text" style="border:none !important;" class="input-text" value="${pd.project_workHours }" placeholder="" id="project_workHours" name="project_workHours">
      </div>
    </div>
    <div class="row cl">
      <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
        <input class="btn btn-primary radius" onclick="clone_palne()" type="button" value="&nbsp;&nbsp;返回&nbsp;&nbsp;">
      </div>
    </div>
  </form>
</article> 
</div>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>

<script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/jquery.validation/1.14.0/messages_zh.min.js"></script>
<!--请在下方写此页面业务相关的脚本-->
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/webuploader/0.1.5/webuploader.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.config.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/ueditor.all.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>H-ui/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
    //3.2、时间处理-时间轴=>年月日格式
    function timeStamp2String (time) {
        //y=年;ymd=年月日;ymdhm年月日时分;ymdhms年月日时分秒
        var datetime = new Date();
        datetime.setTime(time);
        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
        var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
        var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
        var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
        return year + "-" + month + "-" + date + " " + hour + ":" + minute;
    }
    function clone_palne(){
        parent.$('.r .Hui-iconfont').click();
        parent.layer.close(index);
    }
$(function(){
    var start_date='${pd.project_startTime }';
    var end_date='${pd.project_endTime }';
    var due_date='${pd.project_deadLine }';
    var timestamp = new Date(parseInt(start_date));
    $('#project_startTime').val(timeStamp2String(timestamp));
    var timestamp2 = new Date(parseInt(end_date));
    $('#project_endTime').val(timeStamp2String(timestamp2));
    var timestamp3 = new Date(parseInt(due_date));
    $('#project_deadLine').val(timeStamp2String(timestamp3));
    var validate = $("#form-horizontal").validate({
     submitHandler: function(form){ 
       companyAdd(); //执行提交方法
          },
      });    
});

function companyAdd(){ 
  var index = parent.layer.getFrameIndex(window.name);  
  $.ajax({
            url : "<%=basePath%>OrderJoin/save.do",
            data :$('#form-horizontal').serialize(),
            type : 'post',
            dataType : 'json',
            async : false,
            success : function(result) {
               if(result.statusCode ==200){
          parent.$('.breadcrumb .r .Hui-iconfont').click();
            parent.layer.close(index);
             }
            },
            error : function(msg) {
            }
        });  
    } 
</script>
</body>

</html>
