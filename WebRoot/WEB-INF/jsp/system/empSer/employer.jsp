<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
  String path = request.getContextPath();
   String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
     + path + "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
 content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico">
<LINK rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/static/h-ui.admin/skin/default/skin.css"
 id="skin" />
<link rel="stylesheet" type="text/css"
 href="<%=basePath%>H-ui/static/h-ui.admin/css/style.css" />
<link rel="stylesheet"
 href="<%=basePath%>H-ui/lib/zTree/v3/css/zTreeStyle/zTreeStyle.css"
 type="text/css">
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>会员管理</title>
</head>

<body class="pos-r">
 <div>
  <nav class="breadcrumb">
   <i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>订单管理
   <span class="c-gray en">&gt;</span>订单管理 <a
    class="btn btn-success radius r"
    style="line-height: 1.6em; margin-top: 3px"
    href="javascript:location.replace(location.href);" title="刷新"><i
    class="Hui-iconfont">&#xe68f;</i></a>
  </nav>
  <div class="page-container">
   <div class="text-c">
    <input type="text" id="REAL_NAME" name="REAL_NAME" placeholder="标题" style="width: 150px" class="input-text">

        <span class="select-box" style="width:150px;">
            <select class="select" name="project_type" id="project_type">
            <option value="">全部</option>
                <c:forEach items="${typelist}" var="gcdl">
                 <option value="${gcdl._id }">${gcdl.type_name}</option>
                </c:forEach>
            </select>
        </span>
    <button name="search" id="search" class="btn btn-success" type="submit">  <i class="Hui-iconfont">&#xe665;</i>搜索 </button>
   </div>
   <%--<div class="cl pd-5 bg-1 bk-gray mt-20">--%>
    <%--<span class="l">--%>
    <%--<button onclick="exportFeedback();" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe600;</i>导出Excel</button> --%>
    <%--</span>--%>
   <%--</div>--%>
   <div class="mt-20">
    <form id="form-mem">
     <table
      class="table table-border table-bordered table-bg table-hover table-sort">
      <thead>
       <tr class="text-c">
        <th style="width: 150px;height: 70px;">标题</th>
        <th style="width: 150px;height: 70px;">发布人</th>
        <th style="width: 150px;height: 70px;">项目类型</th>
        <th style="width: 150px;height: 70px;">地区</th>
        <th style="width: 150px;height: 70px;">预算</th>
        <th style="width: 150px;height: 70px;">抢单截止时间</th>
        <th style="width: 150px;height: 70px;">订单状态</th>
        <th style="width: 150px;height: 70px;">抢单人</th>
        <th style="width: 150px;height: 70px;">操作</th>
       </tr>
      </thead>
      <tbody>
      </tbody>
     </table>
    </form>
   </div>
  </div>
 </div>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/jquery/1.9.1/jquery.min.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/layer/2.1/layer.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/My97DatePicker/WdatePicker.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/static/h-ui/js/H-ui.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/static/h-ui.admin/js/H-ui.admin.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modalmanager.js"></script>
 <script type="text/javascript"
  src="<%=basePath%>H-ui/lib/bootstrap-modal/2.2.4/bootstrap-modal.js"></script>
 <script type="text/javascript">
     //3.2、时间处理-时间轴=>年月日格式
     function timeStamp2String (time) {
         //y=年;ymd=年月日;ymdhm年月日时分;ymdhms年月日时分秒
         var datetime = new Date();
         datetime.setTime(time);
         var year = datetime.getFullYear();
         var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
         var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
         var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
         var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
         return year + "-" + month + "-" + date + " " + hour + ":" + minute;
     }
     var table;
      $(function() {
        //table
        table = $('.table-sort').dataTable({
          aaSorting: [
            //[1, asc"]
          ], //默认第几个排序
          aoColumnDefs: [
            //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
            //{"orderable":false,"aTargets":[3]} 
          ],
          bStateSave: true, //状态保存
          bProcessing: false, // 是否显示取数据时的那个等待提示
          bServerSide: true,//这个用来指明是通过服务端来取数据
          sAjaxSource: "<%=basePath%>yx_employer/querytbemp.do",//这个是请求的地址
          bLengthChange: true,// 每行显示记录数  
          iDisplayLength: 10,// 每页显示行数  
          bSort: true,// 排序
          bAutoWidth: false, //是否自适应宽度  
          searching: false,//隐藏右侧搜索框 
          bPaginat: true, //翻页功能
          bLengthChange: true, //改变每页显示数据数量 
          aoColumns: [
                      {sClass: "center", mDataProp: "project_title", aTargets: [0], "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                              return '<div style=\"overflow: hidden;text-overflow:ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;\" title="'+c.project_title+'">'+c.project_title+'</div>';
                          }
                      },
                      {sClass: "center", mDataProp: "user_name", aTargets: [1], mData: "user_name"},
                      {sClass: "center", mDataProp: "type_name", aTargets: [2], mData: "type_name"},
                      {sClass: "center", mDataProp: "project_region", aTargets: [3], mData: "project_region"},
                      {sClass: "center", mDataProp: "project_budget", aTargets: [4], mData: "project_budget"},
                      {sClass: "center", mDataProp: "project_deadLine", aTargets: [5], "mRender":function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                              if(c.project_deadLine!=''){
                                  var date1= timeStamp2String(c.project_deadLine);
                                  return date1;
                              }else {
                                  return "";
                              }

                          }
                      },
                      {
                          sClass: "center",
                          mDataProp: "project_state",
                          aTargets: [6],
                          mRender:function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                              if(c.project_state == '0'){
                                  return '抢单中';
                              }if(c.project_state == '1'){
                                  return '已选中';
                              }if(c.project_state == '2'){
                                  return '确认完善';
                              }
                              if(c.project_state == '3'){
                                  return '待支付';
                              }
                              if(c.project_state == '4'){
                                  return '已支付';
                              }
                              if(c.project_state == '5'){
                                  return '确认交付';
                              }
                              if(c.project_state == '6'){
                                  return '一键会审';
                              }
                              if(c.project_state == '7'){
                                  return '已完成';
                              }
                              if(c.project_state == '8'){
                                  return '已结束';
                              }
                              if(c.project_state == '9'){
                                  return '已屏蔽';
                              }
                          }
                      },
                      {
                          sClass: "center",
                          aTargets: [7],
                          mRender:function(a,b,c,d){//a表示statCleanRevampId对应的值，c表示当前记录行对象
                              return '<a _href=\"<%=basePath%>yx_employer/toBidder.do?ID='+c._id+'\" data-title=\"查看抢单人\" class=\"btn btn-link\" onclick=\"Hui_admin_tab(this)\" href=\"javascript:;\" >查看抢单人</a>';
                          }
                      },
                      {sClass: "center",aTargets: [8], mRender: function(a,b,c,d){
                              if(c.project_state == '9'){
                                  return '<div style="height:62px;line-height:62px">已屏蔽</div>';
                              }else {
                                  return '<a data-title=\"查看详情\" onclick=\"project_edit(\'查看详情\',\'<%=basePath%>yx_employer/queryemp.do?ID='+c._id+'\',\'\',\'300\')\" href=\"javascript:;\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\">查看详情</a><a data-title=\"屏蔽订单\" href=\"javascript:;\" onclick=\"delbyid(\'<%=basePath%>yx_employer/delete.do?ID='+c._id+'\')\" class=\"btn btn-secondary radius\" style=\"text-decoration:none\">屏蔽订单</a>';
                              }

                      }}],
                     fnServerData: function(sSource, aoData, fnCallback){
                       //获取检索参数
                       var REAL_NAME = $("#REAL_NAME").val();
                       var project_type = $("#project_type").val();
                       $.ajax({
                         url: sSource,//这个就是请求地址对应sAjaxSource
                         data: {
                           aoData: JSON.stringify(aoData),
                           title: REAL_NAME,project_type:project_type
                         },//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
                         type: 'post',
                         dataType: 'json',
                         async: false,
                         success: function(result){
                           var data = {};
                             if( result.sEcho ){
                               data = result;
                             }else{
                               data = JSON.parse(result);
                             }
                             fnCallback(data);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的
                           //表格数据居中
                           $("td").attr("style", "text-align:center");
                         },
                         error: function(msg){
                         }
                       });
                     }
             });

      //搜索
      $('#search').click(function(){
        table.fnDraw();
      });

      $(document).keyup(function(event){
        if(event.keyCode == 13){
          $("#search").trigger("click");
        }
      });
    });
    function quserpany(url){
      $.ajax({
        type: "POST",
        url: url,
        data: $('#form-mem').serialize(),
        async: false,
        error: function(request){
          alert("Connection error");
        },
        success: function(data){
          alert("操作成功！");
          $(".show_iframe").html(data);
        }
      });
    }

     /*编辑*/
     function project_edit(title,url,w,h){
         var index=layer.open({
             type:2,
             title:title,
             content:url
         });
         layer.full(index);
     }

     //审核
    function tiaosh(title, url, w, h){
      var checklist = document.getElementsByName("ID");
      var len = $("input[name='ID']:checked").length;
      if(len == 0){
        alert("请选择一个会员！");
      }else if(len > 1){
        alert("只能选择一个会员！");
      }else{
        var data = $("input[name='ID']:checked").val();
        var URL = url + "?ID=" + data;
        var index = layer.open({
          type: 2,
          title: title,
          content: URL

        });
        layer.full(index);
      }
    }

    function piliangdel(url){
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $('#form-mem').serialize(),
        async: false,
        success: function(result){
          parent.$('.breadcrumb .r .Hui-iconfont').click();
          window.location.reload();
        },
        error: function(msg){
        }
      });
    }

    function project_del(url){
      layer.confirm('确认要删除该数据吗？', function(index){
        //此处请求后台程序，下方是成功后的前台处理…… 
        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          async: false,
          success: function(result){
            /*if(result.status == 1){*/
            parent.$('.breadcrumb .r .Hui-iconfont').click();
            window.location.reload();
          },
          error: function(msg){
          }
        });
      });
    }

    function delbyid(url){
      layer.confirm('确定屏蔽订单?进行此操作将无法恢复', function(index){
        $.ajax({
          type: "POST",
          url: url,
          data: $('#form-pany').serialize(),
          async: false,
          success: function(result){
            parent.$('.breadcrumb .r .Hui-iconfont').click();
            window.location.reload();
          },
          error: function(msg){
          }
        });
      });
    }
    //禁止/解禁
    function uppany(url){
      var checklist = document.getElementsByName("ID");
      var len = $("input[name='ID']:checked").length;
      alert(len);
      if(len == 0){
        alert("请选择一个需要审核/不审核的用户！");
      }else if(len > 1){
        alert("只能选择一个需要审核/不审核的用户！");
      }else{
        var data = $("input[name='ID']:checked").val();
        $.ajax({
          type: "POST",
          url: url,
          data: $('#form-mem').serialize(),
          async: false,
          success: function(result){
            /*    
                if(result.statusCode == 200){ */
            parent.$('.breadcrumb .r .Hui-iconfont').click();
            window.location.reload();
          },
          error: function(msg){
          }
        });
      }
    }
    //分配
    function fenpei(title, url, w, h){
      var checklist = document.getElementsByName("ID");
      var len = $("input[name='ID']:checked").length;
      if(len == 0){
        alert("请选择一个会员！");
      }else if(len > 1){
        alert("只能选择一个会员！");
      }else{
        var data = $("input[name='ID']:checked").val();
        var URL = url + "?ID=" + data;
        var index = layer.open({
          type: 2,
          title: title,
          content: URL

        });
        layer.full(index);
      }
    }
    
    //导出excel
    function exportFeedback(){
      debugger;
      var checkedElemts = $("#form-mem input:checked");
      var ids = "";
      for(var i = 0, il = checkedElemts.length; i < il; i++){
        ids += "\'" + checkedElemts[i].value + "\',";
      }
      ids = ids.substring(0, ids.length - 1);
      $("#form-mem").append($('<a id="exportFeedback_a" style="display: none" href="<%=basePath%>empSer/exportfeedback.do?ids='+ids+'"/>'));
      $("#exportFeedback_a")[0].click();
      $("#exportFeedback_a").remove();
    }

    //重置密码
    function uppw(url){
      var checklist = document.getElementsByName("ID");
      var len = $("input[name='ID']:checked").length;
      if(len == 0){
        alert("请选择一个需要重置的用户！");
      }else if(len > 1){
        alert("只能选择一个需要重置的用户！");
      }else{
        var data = $("input[name='ID']:checked").val();
        $.ajax({
          type: "POST",
          url: url,
          data: $('#form-mem').serialize(),
          async: false,
          success: function(result){
            alert("密码重置成功！");
            parent.$('.breadcrumb .r .Hui-iconfont').click();
            window.location.reload();
          },
          error: function(msg){
          }
        });
      }
    }
  </script>
</body>

</html>